package nl.pelagic.shutdownhook;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import sun.misc.Signal;
import sun.misc.SignalHandler;

@SuppressWarnings({
    "unused", "javadoc", "static-method"
})
public class TestStopHandler extends ShutdownHook implements SignalHandler {
  static final String SIGHUP_NAME = "USR2";
  static final Signal SIGHUP      = new Signal(SIGHUP_NAME);

  @Before
  public void setUp() {
    this.handledSignals.clear();
    this.calls = 0;
  }

  /*
   * SignalHandler
   */

  List<Signal> handledSignals = new LinkedList<>();

  @Override
  public void handle(final Signal sig) {
    this.handledSignals.add(sig);
  }

  /*
   * ShutdownHook
   */

  int calls = 0;

  @Override
  void stopAllShutdownHookParticipants() {
    this.calls++;
  }

  /*
   * Tests
   */

  @Test(timeout = 8000, expected = ExceptionInInitializerError.class)
  public void testConstructThrowSignal() {
    final StopHandler handler = new StopHandler(null, this);
  }

  @Test(timeout = 8000, expected = ExceptionInInitializerError.class)
  public void testConstructThrowHookNull() {
    final StopHandler handler = new StopHandler(SIGHUP_NAME, null);
  }

  @Test(timeout = 8000)
  public void testActivateDeactivate() throws InterruptedException {
    assertThat(Integer.valueOf(this.calls), equalTo(Integer.valueOf(0)));

    final StopHandler handler = new StopHandler(SIGHUP_NAME, this);
    handler.activate();
    handler.activate();

    assertThat(Integer.valueOf(this.calls), equalTo(Integer.valueOf(0)));

    Signal.raise(SIGHUP);

    while (this.calls == 0) {
      Thread.sleep(100);
    }

    assertThat(Integer.valueOf(this.calls), equalTo(Integer.valueOf(1)));

    this.calls = 0;

    Signal.raise(SIGHUP);
    Signal.raise(SIGHUP);

    while (this.calls < 2) {
      Thread.sleep(100);
    }

    assertThat(Integer.valueOf(this.calls), equalTo(Integer.valueOf(2)));

    handler.deactivate();
    handler.deactivate();
  }

  @Test(timeout = 8000)
  public void testHandleSignal() {
    final StopHandler handler = new StopHandler(SIGHUP_NAME, this);
    handler.activate();

    handler.handle(SIGHUP);
    assertThat(Integer.valueOf(this.calls), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(this.handledSignals.size()), equalTo(Integer.valueOf(0)));

    handler.deactivate();
  }

  @Test(timeout = 8000)
  public void testHandleDifferentSignal() {
    final StopHandler handler = new StopHandler(SIGHUP_NAME, this);
    handler.activate();

    handler.handle(new Signal("INT"));
    assertThat(Integer.valueOf(this.calls), equalTo(Integer.valueOf(0)));

    handler.deactivate();
  }

  @Test(timeout = 8000)
  public void testHandleChainToDflt() {
    final StopHandler handler = new StopHandler(SIGHUP_NAME, this);

    final SignalHandler oldHandler = Signal.handle(SIGHUP, SIG_DFL);
    try {
      handler.activate();

      handler.handle(SIGHUP);
      assertThat(Integer.valueOf(this.calls), equalTo(Integer.valueOf(1)));
      assertThat(Integer.valueOf(this.handledSignals.size()), equalTo(Integer.valueOf(0)));

      handler.deactivate();
    }
    finally {
      Signal.handle(SIGHUP, oldHandler);
    }
  }

  @Test(timeout = 8000)
  public void testHandleChainToIgn() {
    final StopHandler handler = new StopHandler(SIGHUP_NAME, this);

    final SignalHandler oldHandler = Signal.handle(SIGHUP, SIG_IGN);
    try {
      handler.activate();

      handler.handle(SIGHUP);
      assertThat(Integer.valueOf(this.calls), equalTo(Integer.valueOf(1)));
      assertThat(Integer.valueOf(this.handledSignals.size()), equalTo(Integer.valueOf(0)));

      handler.deactivate();
    }
    finally {
      Signal.handle(SIGHUP, oldHandler);
    }
  }

  @Test(timeout = 8000)
  public void testHandleChainToThis() {
    final StopHandler handler = new StopHandler(SIGHUP_NAME, this);

    final SignalHandler oldHandler = Signal.handle(SIGHUP, this);
    try {
      handler.activate();

      handler.handle(SIGHUP);
      assertThat(Integer.valueOf(this.calls), equalTo(Integer.valueOf(1)));
      assertThat(Integer.valueOf(this.handledSignals.size()), equalTo(Integer.valueOf(1)));

      handler.deactivate();
    }
    finally {
      Signal.handle(SIGHUP, oldHandler);
    }
  }

  @Test(timeout = 8000)
  public void testHandleChainThrows() {
    final ThrowingSignalHandler throwingSignalHandler = new ThrowingSignalHandler();
    final SignalHandler oldHandler = Signal.handle(SIGHUP, throwingSignalHandler);

    final StopHandler handler = new StopHandler(SIGHUP_NAME, this);
    try {
      handler.activate();

      handler.handle(SIGHUP);
      assertThat(Integer.valueOf(this.calls), equalTo(Integer.valueOf(1)));
      assertThat(Integer.valueOf(this.handledSignals.size()), equalTo(Integer.valueOf(0)));

      handler.deactivate();
    }
    finally {
      Signal.handle(SIGHUP, oldHandler);
    }
  }
}