package nl.pelagic.service.log.syslog;

import java.io.IOException;

interface SysLogLibc {
  void load() throws IOException;

  void openlog(final String ident, final int option, final int facility);

  void syslog(final int priority, final String format, final Object... args);

  void syslog(final int priority, final String s);

  void closelog();
}
