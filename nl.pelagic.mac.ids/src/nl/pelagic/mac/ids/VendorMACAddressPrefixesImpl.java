package nl.pelagic.mac.ids;

import java.io.File;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicReference;

import javax.xml.parsers.DocumentBuilderFactory;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;
import org.osgi.service.log.LogService;
import org.osgi.service.metatype.annotations.Designate;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import nl.pelagic.mac.ids.api.VendorMACAddressPrefixes;
import nl.pelagic.mac.ids.api.VendorMACPrefix;

/**
 * Vendor MAC address prefixes service
 */
@Component
@Designate(ocd = Config.class)
public class VendorMACAddressPrefixesImpl implements VendorMACAddressPrefixes {
  /*
   * Services
   */

  private final AtomicReference<LogService> logger = new AtomicReference<>(null);

  @Reference(cardinality = ReferenceCardinality.OPTIONAL, policy = ReferencePolicy.DYNAMIC,
      policyOption = ReferencePolicyOption.GREEDY)
  void setLogger(final LogService logger) {
    this.logger.set(logger);
  }

  void unsetLogger(final LogService logger) {
    this.logger.compareAndSet(logger, null);
  }

  /*
   * Lifecycle
   */

  Config config = null;

  @Activate
  void activate(final Config config) {
    this.config = config;
    this.read(config.vendorMACsFile());
  }

  @Deactivate
  void deactivate() {
    synchronized (this.lock) {
      this.vendorToPrefix.clear();
      this.prefixToVendorMac.clear();
    }
  }

  @Modified
  void modified(final Config config) {
    final Config preConfig = this.config;
    this.config = config;

    if (!preConfig.vendorMACsFile().equals(config.vendorMACsFile())) {
      this.read(config.vendorMACsFile());
    }
  }

  /*
   * Helpers
   */

  boolean checkFile(final File f) {
    if (!f.exists() //
        || f.isDirectory() //
        || !f.canRead()) {
      final LogService logger = this.logger.get();
      if (logger != null) {
        logger.log(LogService.LOG_ERROR,
            "MAC address prefix file '" + f.getAbsolutePath() + "' is a directory or can't be read, ignored");
      }
      return false;
    }

    return true;
  }

  static final Locale            LOCALE               = Locale.US;

  static final String            XML_VENDOR_MAPPING   = "VendorMapping";
  static final String            XML_ATTR_MAC_PREFIX  = "mac_prefix";
  static final String            XML_ATTR_VENDOR_NAME = "vendor_name";

  Object                         lock                 = new Object();
  Map<String, Set<Long>>         vendorToPrefix       = new TreeMap<>();
  Map<Long, VendorMACPrefixImpl> prefixToVendorMac    = new TreeMap<>();

  void read(final String filename) {
    final File f = new File(filename);
    if (!this.checkFile(f)) {
      return;
    }

    Document document = null;
    Throwable exception = null;
    try {
      document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(f);
    }
    catch (final Throwable e) {
      document = null;
      exception = e;
    }

    final LogService logger = this.logger.get();

    if (document == null) {
      if (logger != null) {
        logger.log(LogService.LOG_ERROR, "Error while reading '" + f.getAbsolutePath() + "', ignored", exception);
      }
      return;
    }

    final Map<String, Set<Long>> vendorToPrefix = new TreeMap<>();
    final Map<Long, VendorMACPrefixImpl> prefixToVendorMac = new TreeMap<>();

    final NodeList vendorMappings = document.getElementsByTagName(XML_VENDOR_MAPPING);
    for (int i = 0; i < vendorMappings.getLength(); i++) {
      /* can't be null */
      final Node vendorMapping = vendorMappings.item(i);

      /* can't be null */
      final NamedNodeMap attributes = vendorMapping.getAttributes();
      if (attributes.getLength() == 0) {
        continue;
      }

      final Node macPrefix = attributes.getNamedItem(XML_ATTR_MAC_PREFIX);
      final Node vendorName = attributes.getNamedItem(XML_ATTR_VENDOR_NAME);
      if ((macPrefix == null) //
          || (vendorName == null)) {
        continue;
      }

      final String macPrefixText = macPrefix.getTextContent().trim();
      final String vendorNameText = vendorName.getTextContent().trim();

      if ((macPrefixText.isEmpty()) //
          || (macPrefixText.length() != 8) //
          || vendorNameText.isEmpty()) {
        if (logger != null) {
          logger.log(LogService.LOG_ERROR,
              "Invalid prefix/vendor combination '" + macPrefixText + "/" + vendorNameText + "', ignored");
        }
        continue;
      }

      final long macPrefixNumber = MACUtils.getMacAddressNumber(macPrefixText);
      if (macPrefixNumber == -1) {
        if (logger != null) {
          logger.log(LogService.LOG_ERROR,
              "Invalid prefix '" + macPrefixText + "' for vendor '" + vendorNameText + "', ignored");
        }
        continue;
      }

      final VendorMACPrefixImpl vendorMac = new VendorMACPrefixImpl(vendorNameText, macPrefixNumber);

      final String vendorNameLower = vendorNameText.toLowerCase(LOCALE);
      final Long macPrefixNumberLong = Long.valueOf(macPrefixNumber);

      Set<Long> vendorIds = vendorToPrefix.get(vendorNameLower);
      if (vendorIds == null) {
        vendorIds = new TreeSet<>();
        vendorToPrefix.put(vendorNameLower, vendorIds);
      }
      vendorIds.add(macPrefixNumberLong);

      final VendorMACPrefixImpl prevVendorMac = prefixToVendorMac.put(macPrefixNumberLong, vendorMac);
      if (prevVendorMac != null) {
        if (logger != null) {
          logger.log(LogService.LOG_ERROR, "Detected duplicate prefix " + macPrefixText);
        }
      }
    }

    synchronized (this.lock) {
      this.vendorToPrefix = vendorToPrefix;
      this.prefixToVendorMac = prefixToVendorMac;
    }
  }

  /*
   * VendorMACAddressPrefixes
   */

  @Override
  public VendorMACPrefix getVendorMacPrefix(final long prefix) throws IllegalArgumentException {
    synchronized (this.lock) {
      return this.prefixToVendorMac.get(Long.valueOf(prefix));
    }
  }

  @Override
  public Set<Long> getVendorPrefixes(final String vendor) {
    if (vendor == null) {
      return null;
    }

    final String ntl = vendor.trim().toLowerCase(LOCALE);
    Set<Long> r;
    synchronized (this.lock) {
      r = this.vendorToPrefix.get(ntl);
    }

    return r;
  }
}