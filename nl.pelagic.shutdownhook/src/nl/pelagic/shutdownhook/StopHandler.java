package nl.pelagic.shutdownhook;

import sun.misc.Signal;
import sun.misc.SignalHandler;

/**
 * Handler for signals received from the OS
 */
public class StopHandler implements SignalHandler {
  /** The signal to handle */
  private Signal        signal       = null;

  /** The shutdown hook service to invoke when the signal is received */
  private ShutdownHook  shutdownHook = null;

  /** True when the component is activated */
  private boolean       activated    = false;

  /** The original signal handler */
  private SignalHandler oldHandler   = null;

  /**
   * Constructor
   *
   * @param signal The signal to handle
   * @param shutdownHook The shutdown hook service to invoke when the signal is received
   * @throws ExceptionInInitializerError When signal is null, when shutdownHook is null, or when both are null
   * @throws IllegalArgumentException When the specified signal is unknown
   */
  public StopHandler(final String signal, final ShutdownHook shutdownHook)
      throws ExceptionInInitializerError, IllegalArgumentException {
    if ((signal == null) //
        || (shutdownHook == null)) {
      throw new ExceptionInInitializerError(String.format( //
          "Null parameter(s):%s%s", //
          (signal == null) ? " signal" : "", //
          (shutdownHook == null) ? " shutdownHook" : ""));
    }

    this.signal = new Signal(signal);
    this.shutdownHook = shutdownHook;
  }

  /** activator */
  public void activate() {
    if (this.activated) {
      return;
    }

    this.oldHandler = Signal.handle(this.signal, this);
    this.activated = true;
  }

  /** deactivator */
  public void deactivate() {
    if (!this.activated) {
      return;
    }

    Signal.handle(this.signal, this.oldHandler);
    this.oldHandler = null;
    this.activated = false;
  }

  @Override
  public void handle(final Signal signal) {
    if (signal.equals(this.signal)) {
      this.shutdownHook.stopAllShutdownHookParticipants();
    }

    /* chain back to previous handler, if one exists */
    if ((this.oldHandler != SIG_DFL) //
        && (this.oldHandler != SIG_IGN)) {
      try {
        this.oldHandler.handle(signal);
      }
      catch (final Throwable e) {
        /* swallow */
      }
    }
  }
}