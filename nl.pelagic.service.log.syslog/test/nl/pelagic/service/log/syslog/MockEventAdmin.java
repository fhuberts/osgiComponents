package nl.pelagic.service.log.syslog;

import java.util.LinkedList;
import java.util.List;

import org.junit.Ignore;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

@SuppressWarnings("javadoc")
@Ignore
public class MockEventAdmin implements EventAdmin {
  public List<Event> postEvents = new LinkedList<>();

  @Override
  public void postEvent(final Event event) {
    this.postEvents.add(event);
  }

  public List<Event> sendEvents = new LinkedList<>();

  @Override
  public void sendEvent(final Event event) {
    this.sendEvents.add(event);
  }

  public void reset() {
    this.postEvents.clear();
    this.sendEvents.clear();
  }
}