package nl.pelagic.inotify;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.Ignore;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

import nl.pelagic.inotify.api.INotifyClient;
import nl.pelagic.inotify.api.INotifyEvent;
import nl.pelagic.inotify.api.INotifyMask;

@Ignore
@SuppressWarnings("javadoc")
public class DemoPlain implements LogService, INotifyClient {

  static final int SLEEP_MSEC = 100;

  public static void main(final String[] args) throws IOException {
    final DemoPlain instance = new DemoPlain();
    instance.run(args);
  }

  static void flush() {
    System.out.flush();
    System.err.flush();
  }

  void run(final String[] args) throws IOException {
    final String dir = new File((args.length == 0) ? "." : args[0]).getCanonicalFile().getAbsolutePath();

    final INotifyImpl impl = new INotifyImpl();
    impl.setLogger(this);

    final File curDir = new File(".");

    System.out.println("\n* Activating INotify");

    try {
      impl.activate();
    }
    catch (final Throwable e) {
      e.printStackTrace();
      return;
    }

    int wd = -1;

    try {
      System.out.println("\n* Adding a watch on " + dir);
      wd = impl.addWatch(this, dir, INotifyMask.IN_ALL_EVENT);

      Thread.sleep(SLEEP_MSEC);

      if (wd < 0) {
        System.out.println("\n* Could not add watch");
        return;
      }

      System.out.println("\n  wd " + wd);

      System.out.println("\n* Creating a temporary file");
      final File tmpFile1 = File.createTempFile("demo-1-", null, curDir);

      Thread.sleep(SLEEP_MSEC);

      System.out.println("\n* Creating another temporary file");
      final File tmpFile2 = File.createTempFile("demo-2-", null, curDir);

      Thread.sleep(SLEEP_MSEC);

      System.out.println("\n* Writing to " + tmpFile1.getName());
      try (final FileWriter fw = new FileWriter(tmpFile1)) {
        fw.write("Writing to the temporary file");
      }

      Thread.sleep(SLEEP_MSEC);

      System.out.println("\n* Renaming  " + tmpFile1.getName() + " to " + tmpFile2.getName());
      if (!tmpFile1.renameTo(tmpFile2)) {
        System.out.println("\n* Rename failed");
      }

      Thread.sleep(SLEEP_MSEC);

      System.out.println("\n* Deleting " + tmpFile2.getName());
      if (!tmpFile2.delete()) {
        System.out.println("\n* Delete failed");
      }

      Thread.sleep(SLEEP_MSEC);
    }
    catch (final InterruptedException e) {
      System.out.println("\n* Interrupted");
      e.printStackTrace();
    }
    catch (final IOException e) {
      System.out.println("\n* IO Exception");
      e.printStackTrace();
    }
    finally {
      System.out.println("\n* Deactivating INotify");
      impl.deactivate();
    }

    System.out.println("\n* End");
    flush();
  }

  /*
   * LogService
   */

  @Override
  public void log(final int level, final String message) {
    this.log(null, level, message, null);
  }

  @Override
  public void log(final int level, final String message, final Throwable exception) {
    this.log(null, level, message, exception);
  }

  @Override
  public void log(final ServiceReference sr, final int level, final String message) {
    this.log(null, level, message, null);
  }

  @Override
  public void log(final ServiceReference sr, final int level, final String message, final Throwable exception) {
    flush();

    String levelString;
    PrintStream out = System.out;
    switch (level) {
      case LogService.LOG_DEBUG:
        levelString = "DEBUG  ";
        break;

      case LogService.LOG_ERROR:
        levelString = "ERROR  ";
        out = System.err;
        break;

      case LogService.LOG_INFO:
        levelString = "INFO   ";
        break;

      case LogService.LOG_WARNING:
        levelString = "WARNING";
        break;

      default:
        throw new IllegalArgumentException("Unknown level " + level);
    }

    out.println(String.format("%s: %s\n", levelString, message));
    if (exception != null) {
      exception.printStackTrace(out);
    }

    flush();
  }

  /*
   * INotifyClient
   */

  @Override
  public void notify(final INotifyEvent event) {
    flush();
    System.out.println(event);
    flush();
  }
}