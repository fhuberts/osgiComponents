package nl.pelagic.inotify.api;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

import org.junit.Test;

@SuppressWarnings({
    "javadoc", "static-method"
})
public class TestINotifyEvent {
  @Test(timeout = 8000)
  public void testINotifyEvent() {
    INotifyEvent ev;

    ev = new INotifyEvent(null, 42, 43, 44);
    assertThat(ev, notNullValue());
    assertThat(ev.getName(), notNullValue());
    assertThat(ev.getName(), equalTo(""));
    assertThat(Integer.valueOf(ev.getWd()), equalTo(Integer.valueOf(42)));
    assertThat(Long.valueOf(ev.getMask()), equalTo(Long.valueOf(43)));
    assertThat(Long.valueOf(ev.getCookie()), equalTo(Long.valueOf(0)));

    ev = new INotifyEvent("name", 42, 43 | INotifyMask.IN_MOVE, 44);
    assertThat(ev, notNullValue());
    assertThat(ev.getName(), notNullValue());
    assertThat(ev.getName(), equalTo("name"));
    assertThat(Integer.valueOf(ev.getWd()), equalTo(Integer.valueOf(42)));
    assertThat(Long.valueOf(ev.getMask()), equalTo(Long.valueOf(43 | INotifyMask.IN_MOVE)));
    assertThat(Long.valueOf(ev.getCookie()), equalTo(Long.valueOf(44)));
  }

  @Test(timeout = 8000)
  public void testToString() {
    INotifyEvent ev;

    ev = new INotifyEvent("name", 42, 43, 44);
    assertThat(ev, notNullValue());
    assertThat(ev.toString(),
        equalTo("WatchEvent [wd=42, name=name, mask=IN_ACCESS | IN_MODIFY | IN_CLOSE_WRITE | IN_OPEN, cookie=0]"));
  }
}