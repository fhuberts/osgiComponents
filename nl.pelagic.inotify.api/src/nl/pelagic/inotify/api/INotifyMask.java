package nl.pelagic.inotify.api;

import java.util.LinkedList;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * <p>
 * INotify Event Mask Values.
 * </p>
 * <p>
 * Copied from <code>'/usr/include/linux/inotify'</code> and <code>'man inotify'</code>.
 * </p>
 * <p>
 * Note: When monitoring a directory
 * </p>
 * <ul>
 * <li>The events marked with an asterisk (*) can occur both for the directory itself and for objects inside the
 * directory.<br>
 * </li>
 * <li>The events marked with a plus sign (+) occur only for objects inside the directory (not for the directory
 * itself).</li>
 * </ul>
 */
@ProviderType
public class INotifyMask {

  /*
   * The following are legal, implemented events that user-space can watch for
   */

  /**
   * (+) File was accessed (e.g., read(2), execve(2)).
   */
  public final static int IN_ACCESS        = 0x00000001;

  /**
   * (+) File was modified (e.g., write(2), truncate(2)).
   */
  public final static int IN_MODIFY        = 0x00000002;

  /**
   * <p>
   * (*) Metadata changed.
   * </p>
   * <p>
   * For example:
   * </p>
   * <ul>
   * <li>permissions (e.g., chmod(2))</li>
   * <li>timestamps (e.g., utimensat(2))</li>
   * <li>extended attributes (setxattr(2))</li>
   * <li>link count (since Linux 2.6.25; e.g., for the target of link(2) and for unlink(2))</li>
   * <li>user/group ID (e.g., chown(2)).</li>
   * </ul>
   */
  public final static int IN_ATTRIB        = 0x00000004;

  /**
   * (+) File opened for writing was closed.
   */
  public final static int IN_CLOSE_WRITE   = 0x00000008;

  /**
   * (*) File or directory not opened for writing was closed.
   */
  public final static int IN_CLOSE_NOWRITE = 0x00000010;

  /**
   * (*) File or directory was opened.
   */
  public final static int IN_OPEN          = 0x00000020;

  /**
   * (+) Generated for the directory containing the old filename when a file is renamed.
   */
  public final static int IN_MOVED_FROM    = 0x00000040;

  /**
   * (+) Generated for the directory containing the new filename when a file is renamed.
   */
  public final static int IN_MOVED_TO      = 0x00000080;

  /**
   * (+) File/directory created in watched directory (e.g., open(2) O_CREAT, mkdir(2), link(2), symlink(2), bind(2) on a
   * UNIX domain socket).
   */
  public final static int IN_CREATE        = 0x00000100;

  /**
   * (+) File/directory deleted from watched directory.
   */
  public final static int IN_DELETE        = 0x00000200;

  /**
   * Watched file/directory was itself deleted. (This event also occurs if an object is moved to another filesystem,
   * since mv(1) in effect copies the file to the other filesystem and then deletes it from the original filesystem.) In
   * addition, an IN_IGNORED event will subsequently be generated for the watch descriptor.
   */
  public final static int IN_DELETE_SELF   = 0x00000400;

  /**
   * Watched file/directory was itself moved.
   */
  public final static int IN_MOVE_SELF     = 0x00000800;

  /*
   * The following are legal events. They are sent as needed to any watch
   */

  /**
   * Filesystem containing watched object was unmounted. In addition, an IN_IGNORED event will subsequently be generated
   * for the watch descriptor.
   */
  public final static int IN_UNMOUNT       = 0x00002000;

  /**
   * Event queue overflowed (wd is -1 for this event).
   */
  public final static int IN_Q_OVERFLOW    = 0x00004000;

  /**
   * Watch was removed explicitly (inotify_rm_watch(2)) or automatically (file was deleted, or filesystem was
   * unmounted). See also BUGS.
   *
   */
  public final static int IN_IGNORED       = 0x00008000;

  /*
   * Helper events
   */

  /**
   * Equates to IN_CLOSE_WRITE | IN_CLOSE_NOWRITE.
   */
  public final static int IN_CLOSE         = (IN_CLOSE_WRITE | IN_CLOSE_NOWRITE);

  /**
   * Equates to IN_MOVED_FROM | IN_MOVED_TO.
   */
  public final static int IN_MOVE          = (IN_MOVED_FROM | IN_MOVED_TO);

  /*
   * Special flags
   */

  /**
   * (since Linux 2.6.15) Watch pathname only if it is a directory. Using this flag provides an application with a
   * race-free way of ensuring that the monitored object is a directory.
   */
  public final static int IN_ONLYDIR       = 0x01000000;

  /**
   * (since Linux 2.6.15) Don't dereference pathname if it is a symbolic link.
   */
  public final static int IN_DONT_FOLLOW   = 0x02000000;

  /**
   * (since Linux 2.6.36) By default, when watching events on the children of a directory, events are generated for
   * children even after they have been unlinked from the directory. This can result in large numbers of uninteresting
   * events for some applications (e.g., if watching /tmp, in which many applications create temporary files whose names
   * are immediately unlinked). Specifying IN_EXCL_UNLINK changes the default behavior, so that events are not generated
   * for children after they have been unlinked from the watched directory.
   */
  public final static int IN_EXCL_UNLINK   = 0x04000000;

  /**
   * If a watch instance already exists for the filesystem object corresponding to pathname, add (OR) the events in mask
   * to the watch mask (instead of replacing the mask).
   */
  public final static int IN_MASK_ADD      = 0x20000000;

  /**
   * Subject of this event is a directory.
   */
  public final static int IN_ISDIR         = 0x40000000;

  /**
   * Monitor the filesystem object corresponding to pathname for one event, then remove from watch list.
   */
  public final static int IN_ONESHOT       = 0x80000000;

  /**
   * <p>
   * All of the events
   * </p>
   * <p>
   * We build the list by hand so that we can add flags in the future and not break backward compatibility. Apps will
   * get only the events that they originally wanted. Be sure to add new events here!
   * </p>
   */
  public final static int IN_ALL_EVENT     = (IN_ACCESS | IN_MODIFY | IN_ATTRIB | IN_CLOSE_WRITE | IN_CLOSE_NOWRITE
      | IN_OPEN | IN_MOVED_FROM | IN_MOVED_TO | IN_DELETE | IN_CREATE | IN_DELETE_SELF | IN_MOVE_SELF);

  /**
   * Convert a Linux event mask into a readable string of event names
   *
   * @param mask The mask
   * @return The string representation
   */
  static public String maskToString(final long mask) {
    long msk = mask;
    final List<String> s = new LinkedList<>() {
      private static final long serialVersionUID = -7596297657949138236L;

      @Override
      public String toString() {
        if (this.size() == 0) {
          return "";
        }

        final StringBuilder sb = new StringBuilder();
        sb.append(this.get(0));

        int i = 1;

        while (i < this.size()) {
          sb.append(" | ");
          sb.append(this.get(i));
          i++;
        }

        return sb.toString();
      }
    };

    /*
     * All of the events
     */

    if ((msk & IN_ALL_EVENT) == IN_ALL_EVENT) {
      s.add("IN_ALL_EVENT");
      msk &= ~IN_ALL_EVENT;
    }

    /*
     * Helper events
     */

    if ((msk & IN_CLOSE) == IN_CLOSE) {
      s.add("IN_CLOSE");
      msk &= ~IN_CLOSE;
    }

    if ((msk & IN_MOVE) == IN_MOVE) {
      s.add("IN_MOVE");
      msk &= ~IN_MOVE;
    }

    /*
     * The following are legal, implemented events that user-space can watch for
     */

    if ((msk & IN_ACCESS) == IN_ACCESS) {
      s.add("IN_ACCESS");
    }
    if ((msk & IN_MODIFY) == IN_MODIFY) {
      s.add("IN_MODIFY");
    }
    if ((msk & IN_ATTRIB) == IN_ATTRIB) {
      s.add("IN_ATTRIB");
    }
    if ((msk & IN_CLOSE_WRITE) == IN_CLOSE_WRITE) {
      s.add("IN_CLOSE_WRITE");
    }
    if ((msk & IN_CLOSE_NOWRITE) == IN_CLOSE_NOWRITE) {
      s.add("IN_CLOSE_NOWRITE");
    }
    if ((msk & IN_OPEN) == IN_OPEN) {
      s.add("IN_OPEN");
    }
    if ((msk & IN_MOVED_FROM) == IN_MOVED_FROM) {
      s.add("IN_MOVED_FROM");
    }
    if ((msk & IN_MOVED_TO) == IN_MOVED_TO) {
      s.add("IN_MOVED_TO");
    }
    if ((msk & IN_CREATE) == IN_CREATE) {
      s.add("IN_CREATE");
    }
    if ((msk & IN_DELETE) == IN_DELETE) {
      s.add("IN_DELETE");
    }
    if ((msk & IN_DELETE_SELF) == IN_DELETE_SELF) {
      s.add("IN_DELETE_SELF");
    }
    if ((msk & IN_MOVE_SELF) == IN_MOVE_SELF) {
      s.add("IN_MOVE_SELF");
    }

    /*
     * The following are legal events. They are sent as needed to any watch
     */

    if ((msk & IN_UNMOUNT) == IN_UNMOUNT) {
      s.add("IN_UNMOUNT");
    }
    if ((msk & IN_Q_OVERFLOW) == IN_Q_OVERFLOW) {
      s.add("IN_Q_OVERFLOW");
    }
    if ((msk & IN_IGNORED) == IN_IGNORED) {
      s.add("IN_IGNORED");
    }
    if ((msk & IN_ISDIR) == IN_ISDIR) {
      s.add("IN_ISDIR");
    }

    /*
     * Special flags
     */

    if ((msk & IN_ONLYDIR) == IN_ONLYDIR) {
      s.add("IN_ONLYDIR");
    }
    if ((msk & IN_DONT_FOLLOW) == IN_DONT_FOLLOW) {
      s.add("IN_DONT_FOLLOW");
    }
    if ((msk & IN_EXCL_UNLINK) == IN_EXCL_UNLINK) {
      s.add("IN_EXCL_UNLINK");
    }
    if ((msk & IN_MASK_ADD) == IN_MASK_ADD) {
      s.add("IN_MASK_ADD");
    }
    if ((msk & IN_ONESHOT) == IN_ONESHOT) {
      s.add("IN_ONESHOT");
    }

    return s.toString();
  }
}