This component needs access to the ```sun.misc``` package.

According to the OSGi specification such packages must be listed in
a comma-delimited list under the following framework property:

```
org.osgi.framework.system.packages.extra

Framework launching property identifying extra packages which the system
bundle must export from the current execution environment.

This property is useful for configuring extra system packages in addition
to the system packages calculated by the framework.

The value of this property may be retrieved by calling the
BundleContext.getProperty method.
```

The Felix framework defines the property as follows:

```
org.osgi.framework.system.packages.extra

Specifies a comma-delimited list of packages that should be exported via
the System Bundle from the framework class loader in addition to the
packages in org.osgi.framework.system.packages.

The default value is empty.

If a value is specified, it is appended to the list of default or specified
packages in org.osgi.framework.system.packages.
```

For ```bndtools``` workspaces this can be achieved by adding the snippet
below to a ```bndrun``` file:

```
-runsystempackages: sun.misc
```