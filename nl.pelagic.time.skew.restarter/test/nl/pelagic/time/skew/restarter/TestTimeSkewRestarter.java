package nl.pelagic.time.skew.restarter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.googlecode.miyamoto.AnnotationProxyBuilder;

import nl.pelagic.testmocks.MockLogService;

@SuppressWarnings({
    "static-method",
    "javadoc"
})
public class TestTimeSkewRestarter {
  MockLogService    logService;

  TimeSkewRestarter timeSkewRestarter;
  MockBundleContext ctx;
  MockBundle        frameworkBundle;

  void fillProps(final AnnotationProxyBuilder<Config> config, final long check, final long max) {
    config.setProperty("checkIntervalMilliSeconds", Long.valueOf(check));
    config.setProperty("maximumTimeSkewMilliSeconds", Long.valueOf(max));
  }

  @Before
  public void setUp() {
    this.logService = new MockLogService();

    this.frameworkBundle = new MockBundle();
    this.ctx = new MockBundleContext(this.frameworkBundle);

    this.timeSkewRestarter = new TimeSkewRestarter();
    this.timeSkewRestarter.setLogger(this.logService);

    final AnnotationProxyBuilder<Config> config = AnnotationProxyBuilder.newBuilder(Config.class);
    this.fillProps(config, 200000, 400000);

    this.timeSkewRestarter.activate(this.ctx, config.getProxedAnnotation());
    this.timeSkewRestarter.modified(config.getProxedAnnotation());
  }

  @After
  public void tearDown() {
    this.timeSkewRestarter.deactivate();
    this.timeSkewRestarter.deactivate();
    this.timeSkewRestarter = null;
  }

  @Test(timeout = 8000)
  public void testInitial() {
    assertThat(Integer.valueOf(this.logService.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(this.frameworkBundle.updateCount), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testNoRestartFramework() throws InterruptedException {
    while (!this.timeSkewRestarter.isAlive()) {
      Thread.sleep(1);
    }

    this.timeSkewRestarter.testMode = true;

    final AnnotationProxyBuilder<Config> config = AnnotationProxyBuilder.newBuilder(Config.class);
    this.fillProps(config, 1000, 2000);

    this.timeSkewRestarter.modified(config.getProxedAnnotation());

    Thread.sleep(1500);

    assertThat(Integer.valueOf(this.logService.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(this.frameworkBundle.updateCount), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testRestartFramework() throws InterruptedException {
    while (!this.timeSkewRestarter.isAlive()) {
      Thread.sleep(1);
    }

    this.timeSkewRestarter.testMode = true;

    AnnotationProxyBuilder<Config> config = AnnotationProxyBuilder.newBuilder(Config.class);
    this.fillProps(config, 1000, 100);

    this.timeSkewRestarter.modified(config.getProxedAnnotation());

    Thread.sleep(500);

    config = AnnotationProxyBuilder.newBuilder(Config.class);
    this.fillProps(config, 1000, 0);

    this.timeSkewRestarter.modified(config.getProxedAnnotation());

    Thread.sleep(1500);

    assertThat(Integer.valueOf(this.logService.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(this.frameworkBundle.updateCount), equalTo(Integer.valueOf(1)));
  }

  @Test(timeout = 8000)
  public void testRestartFrameworkUpdateException() throws InterruptedException {
    while (!this.timeSkewRestarter.isAlive()) {
      Thread.sleep(1);
    }

    this.frameworkBundle.updateThrow = true;

    this.timeSkewRestarter.testMode = true;

    AnnotationProxyBuilder<Config> config = AnnotationProxyBuilder.newBuilder(Config.class);
    this.fillProps(config, 1000, 100);

    this.timeSkewRestarter.modified(config.getProxedAnnotation());

    Thread.sleep(500);

    config = AnnotationProxyBuilder.newBuilder(Config.class);
    this.fillProps(config, 1000, 0);

    this.timeSkewRestarter.modified(config.getProxedAnnotation());

    Thread.sleep(1500);

    assertThat(Integer.valueOf(this.logService.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(this.frameworkBundle.updateCount), equalTo(Integer.valueOf(1)));
  }

  @Test(timeout = 8000)
  public void testDeactivate() throws InterruptedException {
    while (!this.timeSkewRestarter.isAlive()) {
      Thread.sleep(1);
    }

    this.timeSkewRestarter.deactivate();

    assertThat(Integer.valueOf(this.logService.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(this.frameworkBundle.updateCount), equalTo(Integer.valueOf(0)));
  }
}