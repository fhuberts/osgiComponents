package nl.pelagic.service.log.syslog;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.EventObject;
import java.util.Hashtable;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.FrameworkListener;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.SynchronousBundleListener;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogListener;
import org.osgi.service.log.LogReaderService;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The syslog service implementation class
 */
public class SysLog extends Thread implements //
    BundleActivator, //
    SynchronousBundleListener, FrameworkListener, ServiceListener, //
    ManagedService, //
    LogReaderService, //
    SysLogLogger {

  /*
   * Config
   */

  static final String ENV_LOG_SERVICE_NAME                          = "log.syslog.logservice.name";
  static final String ENV_LOG_SERVICE_NAME_DEFAULT                  = "";

  static final String ENV_LOG_SERVICE_LOWEST_LEVEL                  = "log.syslog.logservice.lowest.level";
  static final int    ENV_LOG_SERVICE_LOWEST_LEVEL_DEFAULT          = LogService.LOG_INFO;

  static final String ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC         = "log.syslog.logservice.queue.drain.wait.msec";
  static final int    ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC_DEFAULT = 1000;

  static final String ENV_LOG_READER_DEPTH                          = "log.syslog.logreader.depth";
  static final int    ENV_LOG_READER_DEPTH_DEFAULT                  = 1024;

  String              logServiceName                                = ENV_LOG_SERVICE_NAME_DEFAULT;
  int                 logServiceLowestLevel                         = ENV_LOG_SERVICE_LOWEST_LEVEL_DEFAULT;
  int                 logServiceQueueDrainWaitMsec                  = ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC_DEFAULT;
  int                 logReaderDepth                                = ENV_LOG_READER_DEPTH_DEFAULT;

  void processNameFromEnvironment() {
    final String newValue = System.getProperty(ENV_LOG_SERVICE_NAME);

    this.logServiceName = (newValue == null) //
        ? this.logServiceName //
        : newValue.trim();

    /* write back to environment */
    System.setProperty(ENV_LOG_SERVICE_NAME, this.logServiceName);
  }

  void processLowestLevelFromEnvironment() {
    final String newValue = System.getProperty(ENV_LOG_SERVICE_LOWEST_LEVEL);

    this.logServiceLowestLevel = LogUtils.parseLevel(newValue, this.logServiceLowestLevel);

    /* write back to environment */
    System.setProperty(ENV_LOG_SERVICE_LOWEST_LEVEL, LogUtils.levelToTopicLevel(this.logServiceLowestLevel));
  }

  void processQueueDrainWaitMsecFromEnvironment() {
    final String newValue = System.getProperty(ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC);

    this.logServiceQueueDrainWaitMsec = LogUtils.readUintFromObject(newValue, this.logServiceQueueDrainWaitMsec);

    /* write back to environment */
    System.setProperty(ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC, Integer.toString(this.logServiceQueueDrainWaitMsec));
  }

  void processDepthFromEnvironment() {
    final String newValue = System.getProperty(ENV_LOG_READER_DEPTH);

    this.logReaderDepth = LogUtils.readUintFromObject(newValue, this.logReaderDepth);
    this.logStore.setDepth(this.logReaderDepth);

    /* write back to environment */
    System.setProperty(ENV_LOG_READER_DEPTH, Integer.toString(this.logReaderDepth));
  }

  /*
   * BundleActivator
   */

  Bundle                                          bundle                 = null;

  volatile ServiceTracker<EventAdmin, EventAdmin> eventAdminTracker      = null;

  ServiceRegistration<?>                          serviceRegistration    = null;
  ServiceReference<?>                             serviceReference       = null;

  LogServiceFactory                               logServiceFactory      = null;
  ServiceRegistration<?>                          serviceRegistrationLSF = null;

  SysLogLibc                                      libc                   = new SysLogLibcImpl();

  @Override
  public void start(final BundleContext ctx) throws IOException {
    this.libc.load();

    ctx.addBundleListener(this);
    ctx.addFrameworkListener(this);
    ctx.addServiceListener(this);

    this.setName(this.getClass().getSimpleName());
    this.setDaemon(true);

    this.processNameFromEnvironment();
    this.processLowestLevelFromEnvironment();
    this.processQueueDrainWaitMsecFromEnvironment();
    this.processDepthFromEnvironment();

    this.libc.openlog(this.logServiceName, LinuxLogConstants.LOG_PID | LinuxLogConstants.LOG_CONS,
        LinuxLogConstants.LOG_USER);

    this.run.set(true);
    this.forceStop.set(false);
    this.signalEmpty.set(false);
    this.start();

    this.eventAdminTracker = new ServiceTracker<>(ctx, EventAdmin.class, null);
    this.eventAdminTracker.open();

    final String[] clazzes = {
        ManagedService.class.getName(), //
        LogReaderService.class.getName()
    };
    Dictionary<String, Object> props = new Hashtable<>();
    props.put(Constants.SERVICE_PID, this.getClass().getName());
    this.serviceRegistration = ctx.registerService(clazzes, this, props);
    this.serviceReference = this.serviceRegistration.getReference();

    this.logServiceFactory = new LogServiceFactory(this);
    props = new Hashtable<>();
    props.put(Constants.SERVICE_PID, this.logServiceFactory.getClass().getName());
    this.serviceRegistrationLSF = ctx.registerService(LogService.class.getName(), this.logServiceFactory, props);

    this.bundle = ctx.getBundle();

    this.log(this.bundle, this.serviceReference, LogService.LOG_DEBUG, "LogService started", null, null);
  }

  @Override
  public void stop(final BundleContext ctx) {
    this.log(this.bundle, this.serviceReference, LogService.LOG_DEBUG, "LogService stopped", null, null);

    this.serviceRegistrationLSF.unregister();
    this.serviceRegistrationLSF = null;
    this.logServiceFactory = null;

    this.serviceRegistration.unregister();
    this.serviceRegistration = null;

    this.eventAdminTracker.close();

    ctx.removeServiceListener(this);
    ctx.removeFrameworkListener(this);
    ctx.removeBundleListener(this);

    this.signalEmpty.compareAndSet(false, true);
    this.run.set(false);
    this.interrupt();

    this.waitForQueueToDrain();

    this.forceStop.set(true);
    this.interrupt();

    this.libc.closelog();
    this.libc = null;

    this.logStore.logs.clear();

    this.bundle = null;
    this.serviceReference = null;
    this.serviceRegistration = null;

    this.eventAdminTracker = null;
  }

  /*
   * ManagedService
   */

  @Override
  public void updated(final Dictionary<String, ?> properties) {
    if ((properties == null) //
        || properties.isEmpty()) {
      return;
    }

    /* Name */

    Object value = properties.get(ENV_LOG_SERVICE_NAME);
    if ((value != null) //
        && (value instanceof String)) {
      final String oldValue = this.logServiceName;
      if (!oldValue.equals(value)) {
        /* name changed */
        System.setProperty(ENV_LOG_SERVICE_NAME, (String) value);
        this.processNameFromEnvironment();

        this.signalEmpty.compareAndSet(false, true);

        this.log(this.bundle, this.serviceReference, LogService.LOG_INFO,
            String.format("LogService name changed from '%s' to '%s'", oldValue, this.logServiceName), null, null);

        this.waitForQueueToDrain();

        this.libc.closelog();
        this.libc.openlog(this.logServiceName, LinuxLogConstants.LOG_PID | LinuxLogConstants.LOG_CONS,
            LinuxLogConstants.LOG_USER);
      }
    }

    /* Lowest Level */

    value = properties.get(ENV_LOG_SERVICE_LOWEST_LEVEL);
    if (value != null) {
      final int oldValue = this.logServiceLowestLevel;
      final int newValue = LogUtils.parseLevel(value, oldValue);
      if (newValue != oldValue) {
        /* lowest level changed */
        final String newLevel = LogUtils.levelToTopicLevel(newValue);
        System.setProperty(ENV_LOG_SERVICE_LOWEST_LEVEL, newLevel);
        this.processLowestLevelFromEnvironment();

        this.log(this.bundle, this.serviceReference, LogService.LOG_INFO,
            String.format("LogService lowest log level changed from '%s' to '%s'", LogUtils.levelToTopicLevel(oldValue),
                newLevel),
            null, null);
      }
    }

    /* Queue Drain Wait msec */

    value = properties.get(ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC);
    if (value != null) {
      final int oldValue = this.logServiceQueueDrainWaitMsec;
      final int newValue = LogUtils.readUintFromObject(value, oldValue);
      if (newValue != oldValue) {
        /* queue drain wait msec changed */
        System.setProperty(ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC, Integer.toString(newValue));
        this.processQueueDrainWaitMsecFromEnvironment();

        this.log(this.bundle, this.serviceReference, LogService.LOG_INFO,
            String.format("LogService queue drain wait msec changed from '%d' to '%d'", Integer.valueOf(oldValue),
                Integer.valueOf(this.logServiceQueueDrainWaitMsec)),
            null, null);
      }
    }

    /* Depth */

    value = properties.get(ENV_LOG_READER_DEPTH);
    if (value != null) {
      final int oldValue = this.logReaderDepth;
      final int newValue = LogUtils.readUintFromObject(value, oldValue);
      if (newValue != oldValue) {
        /* depth changed */
        System.setProperty(ENV_LOG_READER_DEPTH, Integer.toString(newValue));
        this.processDepthFromEnvironment();

        this.log(this.bundle, this.serviceReference, LogService.LOG_INFO,
            String.format("LogReader depth changed from '%d' to '%d'", Integer.valueOf(oldValue),
                Integer.valueOf(this.logReaderDepth)),
            null, null);
      }
    }
  }

  /*
   * BundleListener
   */

  @Override
  public void bundleChanged(final BundleEvent event) {
    this.log(event.getBundle(), null, LogService.LOG_INFO, LogUtils.bundleEventToString(event), null, event);
  }

  /*
   * FrameworkListener
   */

  @Override
  public void frameworkEvent(final FrameworkEvent event) {
    final boolean error = (event.getType() == FrameworkEvent.ERROR);
    final int level = error //
        ? LogService.LOG_ERROR //
        : LogService.LOG_INFO;
    final Throwable exception = error //
        ? event.getThrowable() //
        : null;

    this.log(event.getBundle(), null, level, LogUtils.frameworkEventToString(event), exception, event);
  }

  /*
   * ServiceListener
   */

  @Override
  public void serviceChanged(final ServiceEvent event) {
    final ServiceReference<?> sr = event.getServiceReference();
    final int level = (event.getType() == ServiceEvent.MODIFIED) //
        ? LogService.LOG_DEBUG //
        : LogService.LOG_INFO;
    this.log(sr.getBundle(), sr, level, LogUtils.serviceEventToString(event), null, event);
  }

  /*
   * SysLogLogger
   */

  @Override
  public void log(final Bundle bundle, final ServiceReference<?> sr, final int level, final String message,
      final Throwable exception, final EventObject event) {
    @SuppressWarnings("unused")
    final boolean r =
        this.queue.offer(new LogEntryImpl(System.currentTimeMillis(), bundle, sr, level, message, exception, event));

    /*
     * No need to catch exceptions on the offer invocation since the only exception that it can throw is a
     * NullPointerException when logEntry is null, which it never is
     */
  }

  /*
   * Log Store
   */

  LogStore         logStore  = new LogStore(this.logReaderDepth);

  /*
   * LogReaderService
   */

  Set<LogListener> listeners = new CopyOnWriteArraySet<>();

  @Override
  public void addLogListener(final LogListener listener) {
    this.listeners.add(listener);
  }

  @Override
  public void removeLogListener(final LogListener listener) {
    this.listeners.remove(listener);
  }

  @Override
  public Enumeration<LogEntry> getLog() {
    return this.logStore.getLogs();
  }

  /*
   * Notify all log listeners and send events to EventAdmin (asynchronously)
   */

  AtomicBoolean               run         = new AtomicBoolean(true);
  AtomicBoolean               forceStop   = new AtomicBoolean(false);
  AtomicBoolean               signalEmpty = new AtomicBoolean(false);

  BlockingQueue<LogEntryImpl> queue       = new LinkedBlockingQueue<>();

  void waitForQueueToDrain() {
    final long end = System.currentTimeMillis() + this.logServiceQueueDrainWaitMsec;
    while (this.signalEmpty.get() //
        && this.run.get() //
        && (System.currentTimeMillis() <= end)) {
      /* busy-wait for the queue to drain */
    }
  }

  @Override
  public void run() {
    while ((!this.queue.isEmpty() //
        || this.run.get()) //
        && !this.forceStop.get()) {
      LogEntryImpl entry = null;
      try {
        entry = this.queue.take();
      }
      catch (final InterruptedException e) {
        /* swallow */
      }

      if (entry != null) {
        final int lvl = entry.getLevel();

        if (lvl < LogService.LOG_DEBUG) {
          this.logStore.add(entry);
        }

        if (lvl <= this.logServiceLowestLevel) {
          this.libc.syslog(LogUtils.osgiPriorityToOSPriority(lvl), entry.toLogMessage());
        }

        for (final LogListener listener : this.listeners) {
          listener.logged(entry);
        }

        final ServiceTracker<EventAdmin, EventAdmin> eventAdminTracker = this.eventAdminTracker;
        final EventAdmin ea = (eventAdminTracker == null) //
            ? null //
            : eventAdminTracker.getService();
        if (ea != null) {
          ea.postEvent(entry.toEventAdminEvent());
        }
      }

      this.signalEmpty.compareAndSet(this.queue.isEmpty(), false);
    }

    this.queue.clear();
    this.signalEmpty.compareAndSet(this.queue.isEmpty(), false);
    this.logStore.logs.clear();
  }
}