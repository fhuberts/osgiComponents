package nl.pelagic.testmocks;

import java.util.LinkedList;
import java.util.List;

import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

@SuppressWarnings("javadoc")
public class MockLogService implements LogService {
  public List<String> logs     = new LinkedList<>();

  /** set to the level you want to minimally log */
  public int          logLevel = LogService.LOG_INFO;

  public static String levelToString(final int level) {
    switch (level) {
      case LogService.LOG_DEBUG:
        return "DEBUG";

      case LogService.LOG_ERROR:
        return "ERROR";

      case LogService.LOG_INFO:
        return "INFO";

      case LogService.LOG_WARNING:
        return "WARNING";

      default:
        return "UNKNOWN";
    }
  }

  public static int levelToInt(final String level) {
    if (level == null) {
      return -1;
    }

    switch (level) {
      case "DEBUG":
        return LogService.LOG_DEBUG;

      case "ERROR":
        return LogService.LOG_ERROR;

      case "INFO":
        return LogService.LOG_INFO;

      case "WARNING":
        return LogService.LOG_WARNING;

      default:
        return -1;
    }
  }

  public static String constructLogLine(final int level, @SuppressWarnings("rawtypes") final ServiceReference sr,
      final String message, final Throwable exception) {
    return String.format("%s %s%s%s", levelToString(level), sr != null ? sr.getBundle().getSymbolicName() + " " : "",
        message, (exception == null) ? "" : " (" + exception.getMessage() + ")");
  }

  @Override
  public void log(final int level, final String message) {
    this.log(level, message, null);
  }

  @Override
  public void log(final int level, final String message, final Throwable exception) {
    if (level > this.logLevel) {
      return;
    }
    this.logs.add(constructLogLine(level, null, message, exception));
  }

  @Override
  public void log(final ServiceReference sr, final int level, final String message) {
    this.log(sr, level, message, null);
  }

  @Override
  public void log(final ServiceReference sr, final int level, final String message, final Throwable exception) {
    if (level > this.logLevel) {
      return;
    }
    this.logs.add(constructLogLine(level, sr, message, exception));
  }
}