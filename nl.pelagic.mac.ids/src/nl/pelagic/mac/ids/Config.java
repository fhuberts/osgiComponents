package nl.pelagic.mac.ids;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Vendor MAC address prefixes",
    description = "Configuration for the Vendor MAC address prefixes service")
@interface Config {
  static public final String VENDOR_MACS_FILE_DEFAULT = "lib/vendorMacs.xml";

  @AttributeDefinition(required = false,
      description = "The file with the vendor MAC address prefixes (default = " + VENDOR_MACS_FILE_DEFAULT + ")")
  String vendorMACsFile() default VENDOR_MACS_FILE_DEFAULT;
}