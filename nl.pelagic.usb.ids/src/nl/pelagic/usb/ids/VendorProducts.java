package nl.pelagic.usb.ids;

import java.util.Map;
import java.util.TreeMap;

/**
 * Container for all known products of a specific vendor
 */
public class VendorProducts {
  String               vendorDescription;
  Map<Integer, String> products = new TreeMap<>();

  VendorProducts(final String vendorDescription) {
    this.vendorDescription = vendorDescription;
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("VendorProducts [vendorDescription=");
    builder.append(this.vendorDescription);
    builder.append(", products=");
    builder.append(this.products.size());
    builder.append("]");
    return builder.toString();
  }
}