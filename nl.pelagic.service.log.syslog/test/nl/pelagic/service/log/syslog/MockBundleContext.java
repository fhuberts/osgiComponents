package nl.pelagic.service.log.syslog;

import java.io.File;
import java.io.InputStream;
import java.util.Collection;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Ignore;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkListener;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceObjects;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

@SuppressWarnings("javadoc")
@Ignore
public class MockBundleContext implements BundleContext {
  @Override
  public String getProperty(final String key) {
    throw new IllegalStateException();
  }

  Bundle bundle = null;

  @Override
  public Bundle getBundle() {
    return this.bundle;
  }

  @Override
  public Bundle installBundle(final String location, final InputStream input) throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public Bundle installBundle(final String location) throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public Bundle getBundle(final long id) {
    throw new IllegalStateException();
  }

  @Override
  public Bundle[] getBundles() {
    throw new IllegalStateException();
  }

  public List<ServiceListener> serviceListeners = new LinkedList<>();

  @Override
  public void addServiceListener(final ServiceListener listener, final String filter) throws InvalidSyntaxException {
    this.serviceListeners.add(listener);
  }

  @Override
  public void addServiceListener(final ServiceListener listener) {
    this.serviceListeners.add(listener);
  }

  @Override
  public void removeServiceListener(final ServiceListener listener) {
    this.serviceListeners.remove(listener);
  }

  public Set<BundleListener> bundleListeners = new HashSet<>();

  @Override
  public void addBundleListener(final BundleListener listener) {
    this.bundleListeners.add(listener);
  }

  @Override
  public void removeBundleListener(final BundleListener listener) {
    this.bundleListeners.remove(listener);
  }

  public Set<FrameworkListener> frameworkListeners = new HashSet<>();

  @Override
  public void addFrameworkListener(final FrameworkListener listener) {
    this.frameworkListeners.add(listener);
  }

  @Override
  public void removeFrameworkListener(final FrameworkListener listener) {
    this.frameworkListeners.remove(listener);
  }

  List<SVCRegistration>  serviceRegistrations       = new LinkedList<>();

  ServiceRegistration<?> registerServiceReturnMulti = null;

  @Override
  public ServiceRegistration<?> registerService(final String[] clazzes, final Object service,
      final Dictionary<String, ?> properties) {
    this.serviceRegistrations.add(new SVCRegistration(clazzes, service, properties));
    return this.registerServiceReturnMulti;
  }

  ServiceRegistration<?> registerServiceReturnSingle = null;

  @Override
  public ServiceRegistration<?> registerService(final String clazz, final Object service,
      final Dictionary<String, ?> properties) {
    final String[] clazzes = {
        clazz
    };
    this.serviceRegistrations.add(new SVCRegistration(clazzes, service, properties));
    return this.registerServiceReturnSingle;
  }

  @Override
  public <S> ServiceRegistration<S> registerService(final Class<S> clazz, final S service,
      final Dictionary<String, ?> properties) {
    throw new IllegalStateException();
  }

  @Override
  public <S> ServiceRegistration<S> registerService(final Class<S> clazz, final ServiceFactory<S> factory,
      final Dictionary<String, ?> properties) {
    throw new IllegalStateException();
  }

  Map<String, ServiceReference<?>[]> srefs = new HashMap<>();

  @Override
  public ServiceReference<?>[] getServiceReferences(final String clazz, final String filter)
      throws InvalidSyntaxException {
    return this.srefs.get(clazz);
  }

  @Override
  public ServiceReference<?>[] getAllServiceReferences(final String clazz, final String filter)
      throws InvalidSyntaxException {
    throw new IllegalStateException();
  }

  @Override
  public ServiceReference<?> getServiceReference(final String clazz) {
    throw new IllegalStateException();
  }

  @Override
  public <S> ServiceReference<S> getServiceReference(final Class<S> clazz) {
    throw new IllegalStateException();
  }

  @Override
  public <S> Collection<ServiceReference<S>> getServiceReferences(final Class<S> clazz, final String filter)
      throws InvalidSyntaxException {
    throw new IllegalStateException();
  }

  @Override
  public <S> S getService(final ServiceReference<S> reference) {
    throw new IllegalStateException();
  }

  @Override
  public boolean ungetService(final ServiceReference<?> reference) {
    throw new IllegalStateException();
  }

  @Override
  public <S> ServiceObjects<S> getServiceObjects(final ServiceReference<S> reference) {
    throw new IllegalStateException();
  }

  @Override
  public File getDataFile(final String filename) {
    throw new IllegalStateException();
  }

  @Override
  public Filter createFilter(final String filter) throws InvalidSyntaxException {
    return FilterImpl.newInstance(filter);
  }

  @Override
  public Bundle getBundle(final String location) {
    throw new IllegalStateException();
  }
}