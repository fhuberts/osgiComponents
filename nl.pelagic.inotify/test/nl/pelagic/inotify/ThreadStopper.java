package nl.pelagic.inotify;

import org.junit.Ignore;

@SuppressWarnings("javadoc")
@Ignore
public class ThreadStopper extends Thread {
  public INotifyImpl impl         = null;
  public Thread      threadToStop = null;
  public long        sleepMsec    = 1000;

  public ThreadStopper(final INotifyImpl impl, final Thread threadToStop, final long sleepMsec) {
    super();
    this.impl = impl;
    this.threadToStop = threadToStop;
    this.sleepMsec = sleepMsec;
  }

  @Override
  public void run() {
    try {
      Thread.sleep(this.sleepMsec);
    }
    catch (final InterruptedException e) {
      e.printStackTrace();
    }
    this.threadToStop.interrupt();

    if (this.impl != null) {
      try {
        Thread.sleep(this.sleepMsec);
      }
      catch (final InterruptedException e) {
        e.printStackTrace();
      }
      this.impl.clientMap.clear();
    }
  }
}
