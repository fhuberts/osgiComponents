package nl.pelagic.inotify.api;

import org.osgi.annotation.versioning.ProviderType;

/**
 * INotify Service Interface
 */
@ProviderType
public interface INotify {

  /*
   * Watches
   */

  /**
   * Add a watch.
   *
   * @param client The client to send events to.
   * @param path The path to add a watch on.
   * @param mask The event mask to use (see {@link INotifyMask}).
   * @return The non-negative watch descriptor.
   * @throws INotifyException When the watch could not be added, with one of the following error numbers (see
   *           {@link INotifyLinuxErrno}):
   *           <ul>
   *           <li>EACCES When read access to the given path is not permitted.</li>
   *           <li>EINVAL When the given event mask contains no valid events or when client is null.</li>
   *           <li>ENAMETOOLONG When the path is too long.</li>
   *           <li>ENOENT When a directory component in pathname does not exist or is a dangling symbolic link.</li>
   *           <li>ENOMEM When the memory allocation for the path string conversion failed or When there is insufficient
   *           kernel memory available.</li>
   *           <li>ENOSPC When the user limit on the total number of inotify watches was reached, or when the kernel
   *           failed to allocate a needed resource.</li>
   *           <li>EPERM When the native code is not in the RUNNING state.</li>
   *           </ul>
   *           Note: All errors are logged.
   */
  int addWatch(final INotifyClient client, final String path, final long mask) throws INotifyException;

  /**
   * Remove a watch
   *
   * @param wd The watch descriptor of the watch to remove
   * @param immediate
   *          <ul>
   *          <li><b>true</b> : No more events will be delivered on the watch.</li>
   *          <li><b>false</b>: Any pending events will still be delivered: the caller must wait for the
   *          {@link INotifyMask#IN_IGNORED} event to be delivered on the watch, which indicates that all pending events
   *          have been delivered and that no more events will be delivered on the watch.<br>
   *          <b>Note</b>: an {@link INotifyMask#IN_IGNORED} event will <b>always</b> be delivered to a client that
   *          hasn't removed it's watch when this service is stopped.</li>
   *          </ul>
   * @throws INotifyException When the watch could not be removed, with one of these error numbers (see
   *           {@link INotifyLinuxErrno}):
   *           <ul>
   *           <li>EINVAL When the watch descriptor wd is not valid.</li>
   *           <li>EPERM When the native code is not in the RUNNING state.</li>
   *           </ul>
   *           Note: All errors are logged.
   */
  void removeWatch(final int wd, final boolean immediate) throws INotifyException;

  /*
   * Utilities
   */

  /**
   * Convert an error number into the corresponding error text (by invoking libc's 'strerror(errno)').
   *
   * @param errno The error number.
   * @return The string representation of errno or "Unknown error &lt;errno&gt;" when errno is unknown to libc.
   */
  String getStrError(final int errno);
}