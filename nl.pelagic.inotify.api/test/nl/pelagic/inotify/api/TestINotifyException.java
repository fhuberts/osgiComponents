package nl.pelagic.inotify.api;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

import org.junit.Test;

@SuppressWarnings({
    "javadoc", "static-method"
})
public class TestINotifyException {
  @Test(timeout = 8000)
  public void testINotifyException() {
    final INotifyException ex = new INotifyException();

    assertThat(ex, notNullValue());
    assertThat(Integer.valueOf(ex.getErrno()), equalTo(Integer.valueOf(Integer.MIN_VALUE)));
    assertThat(ex.getMessage(), nullValue());
    assertThat(ex.getCause(), nullValue());
  }

  @Test(timeout = 8000)
  public void testINotifyExceptionStringThrowable() {
    final String s = "message";
    final Throwable th = new Throwable("bla bla");
    final INotifyException ex = new INotifyException(s, th);

    assertThat(ex, notNullValue());
    assertThat(Integer.valueOf(ex.getErrno()), equalTo(Integer.valueOf(Integer.MIN_VALUE)));
    assertThat(ex.getMessage(), equalTo(s));
    assertThat(ex.getCause(), equalTo(th));
  }

  @Test(timeout = 8000)
  public void testINotifyExceptionStringInt() {
    final String s = "message";
    final int errno = 42;
    final INotifyException ex = new INotifyException(s, errno);

    assertThat(ex, notNullValue());
    assertThat(Integer.valueOf(ex.getErrno()), equalTo(Integer.valueOf(errno)));
    assertThat(ex.getMessage(), equalTo(s));
    assertThat(ex.getCause(), nullValue());
  }

  @Test(timeout = 8000)
  public void testToString() {
    INotifyException ex;

    ex = new INotifyException();
    assertThat(ex.toString(), equalTo(INotifyException.class.getName()));

    ex = new INotifyException(null, 42);
    assertThat(ex.toString(), equalTo(INotifyException.class.getName() + ", errno = 42"));

    ex = new INotifyException("message", 42);
    assertThat(ex.toString(), equalTo(INotifyException.class.getName() + ": message, errno = 42"));
  }
}