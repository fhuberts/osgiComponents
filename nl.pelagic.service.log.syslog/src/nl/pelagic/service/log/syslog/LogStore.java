package nl.pelagic.service.log.syslog;

import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import org.osgi.service.log.LogEntry;

class LogStore {
  int depth;

  LogStore(final int depth) {
    super();
    this.setDepth(depth);
  }

  List<LogEntry> logs = new LinkedList<>();

  private void trimLogs(final boolean beforeAddingAnEntry) {
    if (this.depth <= 0) {
      this.logs.clear();
      return;
    }

    final int max = this.depth + (beforeAddingAnEntry ? 0 : 1);

    while (this.logs.size() >= max) {
      this.logs.remove(this.logs.size() - 1);
    }
  }

  synchronized void setDepth(final int depth) {
    this.depth = Math.max(0, depth);
    this.trimLogs(false);
  }

  synchronized void add(final LogEntry entry) {
    if (this.depth <= 0) {
      return;
    }

    this.trimLogs(true);
    this.logs.add(0, entry);
  }

  synchronized Enumeration<LogEntry> getLogs() {
    return Collections.enumeration(this.logs);
  }
}