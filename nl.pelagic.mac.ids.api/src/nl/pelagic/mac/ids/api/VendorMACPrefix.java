package nl.pelagic.mac.ids.api;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Container for a vendor MAC address prefix
 */
@ProviderType
public interface VendorMACPrefix {
  /**
   * @return The MAC address prefix
   */
  long getPrefix();

  /**
   * @return The MAC address prefix vendor
   */
  String getVendor();
}