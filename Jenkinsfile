/* https://www.jenkins.io/doc/book/pipeline */
pipeline {

  /* https://www.jenkins.io/doc/book/pipeline/syntax/#agent */
  agent {
    label 'linux'
  }

  /* https://www.jenkins.io/doc/book/pipeline/syntax/#triggers */
  triggers { 
    pollSCM('H/5 * * * *')
  }

  /* https://www.jenkins.io/doc/book/pipeline/syntax/#options */
  options {
    buildDiscarder(
      logRotator(
        numToKeepStr: '10'
      )
    )

    timeout(
      time: 1,
      unit: 'HOURS'
    )
  } /* options */

  /* https://www.jenkins.io/doc/book/pipeline/syntax/#stages */
  stages {
    stage('Setup') {
      steps {
        buildName '#' + env.BUILD_NUMBER + ' - ' + env.GIT_BRANCH + ' - ' + env.GIT_COMMIT.take(7)
      }
    }

    stage('Build') {
      steps {
        sh './gradlew --no-daemon -PCI build export release jacocoTestReport spotbugs javadoc'
      }
    }
  } /* stages */

  /* https://www.jenkins.io/doc/book/pipeline/syntax/#post */
  post {
    always {
      /* https://www.jenkins.io/doc/pipeline/steps/chucknorris */
      chuckNorris()

      /* https://www.jenkins.io/doc/pipeline/steps/core */
      archiveArtifacts(
        artifacts: '**/generated/**',
        defaultExcludes: true,
        caseSensitive: true
        /* 'follow symbolic links' can't be set yet */
      )

      /* https://www.jenkins.io/doc/pipeline/steps/junit */
      junit(
        testResults: '**/generated/test-results/test/*.xml'
      )

      /* https://www.jenkins.io/doc/pipeline/steps/warnings-ng */
      recordIssues(
        tools: [spotBugs(pattern: '**/generated/reports/spotbugs/*.xml'), javaDoc(), java()],
        ignoreFailedBuilds: true
      )

      /* https://www.jenkins.io/doc/pipeline/steps/jacoco */
      jacoco(
        execPattern: '**/generated/jacoco/**.exec',
        exclusionPattern: '**/Test*.class, **/Mock*.class, **/AnnotationProperty.class, **/AnnotationProxyBuilder.class',
        classPattern: '**/bin',
        sourcePattern: '**/src',
        sourceInclusionPattern: '**/*.java'
      )

      /* https://www.jenkins.io/doc/pipeline/steps/mailer */
      step(
        [
          $class: 'Mailer',
          recipients: env.BUILD_NOTIFIER_EMAIL_ADDRESS,
          notifyEveryUnstableBuild: true
        ]
      )
    } /* always */
  } /* post */

} /* pipeline */

