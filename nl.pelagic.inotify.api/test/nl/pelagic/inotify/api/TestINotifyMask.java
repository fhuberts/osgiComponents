package nl.pelagic.inotify.api;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import org.junit.Test;

@SuppressWarnings({
    "static-method", "javadoc", "unused"
})
public class TestINotifyMask {
  @Test(timeout = 8000)
  public void testMaskToString() {
    final INotifyMask m = new INotifyMask();

    final int actual[] = {
        INotifyMask.IN_ACCESS,
        INotifyMask.IN_MODIFY,
        INotifyMask.IN_ATTRIB,
        INotifyMask.IN_CLOSE_WRITE,
        INotifyMask.IN_CLOSE_NOWRITE,
        INotifyMask.IN_OPEN,
        INotifyMask.IN_MOVED_FROM,
        INotifyMask.IN_MOVED_TO,
        INotifyMask.IN_CREATE,
        INotifyMask.IN_DELETE,
        INotifyMask.IN_DELETE_SELF,
        INotifyMask.IN_MOVE_SELF,
        INotifyMask.IN_UNMOUNT,
        INotifyMask.IN_Q_OVERFLOW,
        INotifyMask.IN_IGNORED,
        INotifyMask.IN_ISDIR,
        INotifyMask.IN_CLOSE,
        INotifyMask.IN_MOVE,
        INotifyMask.IN_ONLYDIR,
        INotifyMask.IN_DONT_FOLLOW,
        INotifyMask.IN_EXCL_UNLINK,
        INotifyMask.IN_MASK_ADD,
        INotifyMask.IN_ONESHOT,
        INotifyMask.IN_ALL_EVENT,
        //
        INotifyMask.IN_CLOSE | INotifyMask.IN_ONESHOT,
        INotifyMask.IN_MOVE | INotifyMask.IN_ONESHOT,
        -1,
        0,
        0x10000000 // unused
    };

    final String expected[] = {
        "IN_ACCESS",
        "IN_MODIFY",
        "IN_ATTRIB",
        "IN_CLOSE_WRITE",
        "IN_CLOSE_NOWRITE",
        "IN_OPEN",
        "IN_MOVED_FROM",
        "IN_MOVED_TO",
        "IN_CREATE",
        "IN_DELETE",
        "IN_DELETE_SELF",
        "IN_MOVE_SELF",
        "IN_UNMOUNT",
        "IN_Q_OVERFLOW",
        "IN_IGNORED",
        "IN_ISDIR",
        "IN_CLOSE",
        "IN_MOVE",
        "IN_ONLYDIR",
        "IN_DONT_FOLLOW",
        "IN_EXCL_UNLINK",
        "IN_MASK_ADD",
        "IN_ONESHOT",
        "IN_ALL_EVENT",
        //
        "IN_CLOSE | IN_ONESHOT",
        "IN_MOVE | IN_ONESHOT",
        "IN_ALL_EVENT | IN_UNMOUNT | IN_Q_OVERFLOW | IN_IGNORED | IN_ISDIR | IN_ONLYDIR | IN_DONT_FOLLOW | IN_EXCL_UNLINK | IN_MASK_ADD | IN_ONESHOT",
        "",
        ""
    };

    for (int i = 0; i < actual.length; i++) {
      assertThat("Index " + i, INotifyMask.maskToString(actual[i]), equalTo(expected[i]));
    }
  }
}