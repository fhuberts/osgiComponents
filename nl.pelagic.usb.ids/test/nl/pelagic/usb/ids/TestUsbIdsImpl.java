package nl.pelagic.usb.ids;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.googlecode.miyamoto.AnnotationProxyBuilder;

import nl.pelagic.testmocks.MockLogService;
import nl.pelagic.usb.ids.api.UsbId;

@SuppressWarnings({
    "static-method", //
    "javadoc"
})
public class TestUsbIdsImpl {
  MockLogService logger;
  UsbIdsImpl     impl;

  void fillProps(final AnnotationProxyBuilder<Config> config) {
    config.setProperty("usbIdsFile", "testresources/usb.ids.small");
  }

  @Before
  public void setUp() {
    this.logger = new MockLogService();

    this.impl = new UsbIdsImpl();
    this.impl.setLogger(this.logger);

    final AnnotationProxyBuilder<Config> config = AnnotationProxyBuilder.newBuilder(Config.class);
    this.fillProps(config);

    this.impl.activate(config.getProxedAnnotation());
  }

  @After
  public void tearDown() {
    this.impl.deactivate();
    this.impl.setLogger(null);
    this.impl = null;
    this.logger = null;
  }

  @Test(timeout = 8000)
  public void testInitial() {
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(this.impl.usbIds.toString(), Integer.valueOf(this.impl.usbIds.size()), equalTo(Integer.valueOf(19)));
  }

  @Test(timeout = 8000)
  public void testCheckFile() throws IOException {
    final File dir = new File("testresources/dir");

    boolean r;

    /* non-existing file */

    this.logger.logs.clear();

    File f = new File("testresources/usb.ids.does.not.exist");
    r = this.impl.checkFile(f);
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));

    /* dir */

    this.logger.logs.clear();

    r = this.impl.checkFile(dir);
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));

    /* unreadable file */

    this.logger.logs.clear();

    f = new File("testresources/usb.ids.small");
    final Path p = f.toPath();
    final Set<PosixFilePermission> orgPerms = Files.getPosixFilePermissions(p);

    try {
      Files.setPosixFilePermissions(p, new HashSet<PosixFilePermission>());
      r = this.impl.checkFile(f);
    }
    finally {
      Files.setPosixFilePermissions(p, orgPerms);
    }
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));

    /* non-existing file without logger */

    this.logger.logs.clear();

    this.impl.unsetLogger(this.logger);
    f = new File("testresources/usb.ids.does.not.exist");
    r = this.impl.checkFile(f);
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));
  }

  @Test(timeout = 8000)
  public void testReadDir() {
    this.impl.usbIds.clear();

    final File f = new File("testresources/dir");
    this.impl.read(f);

    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
  }

  @Test(timeout = 8000)
  public void testReadOk() {
    this.impl.usbIds.clear();

    final File f = new File("testresources/usb.ids");
    this.impl.read(f);

    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(this.impl.usbIds.toString(), Integer.valueOf(this.impl.usbIds.size()), equalTo(Integer.valueOf(3413)));
  }

  @Test(timeout = 8000)
  public void testReadLoopFail() {
    this.impl.usbIds.clear();

    final File f = new File("testres\u0000ources/usb.ids");

    this.impl.readLoop(f);
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(this.impl.usbIds.toString(), Integer.valueOf(this.impl.usbIds.size()), equalTo(Integer.valueOf(0)));

    this.impl.setLogger(null);
    this.impl.readLoop(f);

    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(this.impl.usbIds.toString(), Integer.valueOf(this.impl.usbIds.size()), equalTo(Integer.valueOf(0)));

    this.impl.setLogger(this.logger);
    this.impl.readLoop(f);

    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(2)));
    assertThat(this.impl.usbIds.toString(), Integer.valueOf(this.impl.usbIds.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testGetUsbIdNotPresent() {
    UsbId r;

    r = this.impl.getUsbId(65535, 65534);
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(2)));
    assertThat(r, notNullValue());
    assertThat(Integer.valueOf(r.getVendorId()), equalTo(Integer.valueOf(65535)));
    assertThat(r.getVendorDescription(), equalTo(String.format("Unknown vendor 0x%04x", Integer.valueOf(65535))));
    assertThat(Integer.valueOf(r.getProductId()), equalTo(Integer.valueOf(65534)));
    assertThat(r.getProductDescription(), equalTo(String.format("Unknown product 0x%04x", Integer.valueOf(65534))));

    this.logger.logs.clear();

    r = this.impl.getUsbId(65535, 65534);
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(r, notNullValue());
    assertThat(Integer.valueOf(r.getVendorId()), equalTo(Integer.valueOf(65535)));
    assertThat(r.getVendorDescription(), equalTo(String.format("Unknown vendor 0x%04x", Integer.valueOf(65535))));
    assertThat(Integer.valueOf(r.getProductId()), equalTo(Integer.valueOf(65534)));
    assertThat(r.getProductDescription(), equalTo(String.format("Unknown product 0x%04x", Integer.valueOf(65534))));

    /* without logger */

    this.logger.logs.clear();

    this.impl.unsetLogger(this.logger);

    r = this.impl.getUsbId(65534, 65534);
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(r, notNullValue());
    assertThat(Integer.valueOf(r.getVendorId()), equalTo(Integer.valueOf(65534)));
    assertThat(r.getVendorDescription(), equalTo(String.format("Unknown vendor 0x%04x", Integer.valueOf(65534))));
    assertThat(Integer.valueOf(r.getProductId()), equalTo(Integer.valueOf(65534)));
    assertThat(r.getProductDescription(), equalTo(String.format("Unknown product 0x%04x", Integer.valueOf(65534))));

    this.logger.logs.clear();

    r = this.impl.getUsbId(65534, 65534);
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(r, notNullValue());
    assertThat(Integer.valueOf(r.getVendorId()), equalTo(Integer.valueOf(65534)));
    assertThat(r.getVendorDescription(), equalTo(String.format("Unknown vendor 0x%04x", Integer.valueOf(65534))));
    assertThat(Integer.valueOf(r.getProductId()), equalTo(Integer.valueOf(65534)));
    assertThat(r.getProductDescription(), equalTo(String.format("Unknown product 0x%04x", Integer.valueOf(65534))));
  }

  @Test(timeout = 8000)
  public void testGetUsbIdPresent() {
    final UsbId r = this.impl.getUsbId(1, 0x7778);
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(r, notNullValue());
  }

  @Test(timeout = 8000)
  public void testtModified() {
    /* no change */

    AnnotationProxyBuilder<Config> config = AnnotationProxyBuilder.newBuilder(Config.class);
    this.fillProps(config);

    Map<Integer, VendorProducts> pre = this.impl.usbIds;

    this.impl.modified(config.getProxedAnnotation());
    assertThat(Boolean.valueOf(pre == this.impl.usbIds), equalTo(Boolean.TRUE));

    /* change (non-existing file) */

    config = AnnotationProxyBuilder.newBuilder(Config.class);
    this.fillProps(config);
    config.setProperty("usbIdsFile", "testresources/usb.ids.does.not.exist");

    pre = this.impl.usbIds;
    this.impl.modified(config.getProxedAnnotation());
    assertThat(Boolean.valueOf(pre == this.impl.usbIds), equalTo(Boolean.TRUE));

    /* change (other file) */

    config = AnnotationProxyBuilder.newBuilder(Config.class);
    this.fillProps(config);
    config.setProperty("usbIdsFile", "testresources/usb.ids");

    pre = this.impl.usbIds;
    this.impl.modified(config.getProxedAnnotation());
    assertThat(Boolean.valueOf(pre == this.impl.usbIds), equalTo(Boolean.FALSE));
  }
}
