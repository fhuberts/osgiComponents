package nl.pelagic.shutdownhook.api;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * All components implementing this service interface will be invoked when the VM shuts down, either normally or in
 * response to caught signals.
 */
@ConsumerType
public interface ShutdownHookParticipant {
  /**
   * <p>
   * Invoked when the VM shuts down.
   * </p>
   * <p>
   * <b>Note:</b> the invocation is performed from the thread of the VM shutdown hook. The VM is in a fragile state at
   * that time so be careful with the work that is performed in the call: keep the work to an absolute minimum.
   * </p>
   */
  void shutdownHook();
}