package nl.pelagic.service.log.syslog;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

import org.junit.Test;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

@SuppressWarnings({
    "static-method",
    "javadoc"
})
public class TestLogUtils {
  @Test(timeout = 8000)
  public void testInstance() {
    assertThat(new LogUtils().toString(), notNullValue());
  }

  @Test(timeout = 8000)
  public void testClipLevel() {
    assertThat(Integer.valueOf(LogUtils.clipLevel(LogService.LOG_ERROR - 1)),
        equalTo(Integer.valueOf(LogService.LOG_ERROR)));
    assertThat(Integer.valueOf(LogUtils.clipLevel(LogService.LOG_ERROR)),
        equalTo(Integer.valueOf(LogService.LOG_ERROR)));
    assertThat(Integer.valueOf(LogUtils.clipLevel(LogService.LOG_WARNING)),
        equalTo(Integer.valueOf(LogService.LOG_WARNING)));
    assertThat(Integer.valueOf(LogUtils.clipLevel(LogService.LOG_INFO)), equalTo(Integer.valueOf(LogService.LOG_INFO)));
    assertThat(Integer.valueOf(LogUtils.clipLevel(LogService.LOG_DEBUG)),
        equalTo(Integer.valueOf(LogService.LOG_DEBUG)));
    assertThat(Integer.valueOf(LogUtils.clipLevel(LogService.LOG_DEBUG + 1)),
        equalTo(Integer.valueOf(LogService.LOG_DEBUG)));
  }

  @Test(timeout = 8000)
  public void testParseLevel() {
    /* null */

    assertThat(Integer.valueOf(LogUtils.parseLevel(null, LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_INFO)));

    /* Number */

    assertThat(Integer.valueOf(LogUtils.parseLevel(Long.valueOf(-1), LogService.LOG_DEBUG)),
        equalTo(Integer.valueOf(LogService.LOG_ERROR)));
    assertThat(Integer.valueOf(LogUtils.parseLevel(Long.valueOf(0), LogService.LOG_DEBUG)),
        equalTo(Integer.valueOf(LogService.LOG_ERROR)));
    assertThat(Integer.valueOf(LogUtils.parseLevel(Long.valueOf(1), LogService.LOG_DEBUG)),
        equalTo(Integer.valueOf(LogService.LOG_ERROR)));
    assertThat(Integer.valueOf(LogUtils.parseLevel(Long.valueOf(2), LogService.LOG_DEBUG)),
        equalTo(Integer.valueOf(LogService.LOG_WARNING)));
    assertThat(Integer.valueOf(LogUtils.parseLevel(Long.valueOf(3), LogService.LOG_DEBUG)),
        equalTo(Integer.valueOf(LogService.LOG_INFO)));
    assertThat(Integer.valueOf(LogUtils.parseLevel(Long.valueOf(4), LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_DEBUG)));
    assertThat(Integer.valueOf(LogUtils.parseLevel(Long.valueOf(5), LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_DEBUG)));

    /* Wrong class */

    assertThat(Integer.valueOf(LogUtils.parseLevel(new Object(), LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_INFO)));

    /* String empty */

    assertThat(Integer.valueOf(LogUtils.parseLevel("", LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_INFO)));
    assertThat(Integer.valueOf(LogUtils.parseLevel("  ", LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_INFO)));

    /* String number */

    assertThat(Integer.valueOf(LogUtils.parseLevel(" 0 ", LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_ERROR)));
    assertThat(Integer.valueOf(LogUtils.parseLevel(" 12 ", LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_DEBUG)));
    assertThat(Integer.valueOf(LogUtils.parseLevel(" 999999999999999999999999999999999999 ", LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_INFO)));

    /* String valid string */

    assertThat(Integer.valueOf(LogUtils.parseLevel(" log_error ", LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_ERROR)));
    assertThat(Integer.valueOf(LogUtils.parseLevel(" error ", LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_ERROR)));

    assertThat(Integer.valueOf(LogUtils.parseLevel(" log_warning ", LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_WARNING)));
    assertThat(Integer.valueOf(LogUtils.parseLevel(" warning ", LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_WARNING)));

    assertThat(Integer.valueOf(LogUtils.parseLevel(" log_info ", LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_INFO)));
    assertThat(Integer.valueOf(LogUtils.parseLevel(" info ", LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_INFO)));

    assertThat(Integer.valueOf(LogUtils.parseLevel(" log_debug ", LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_DEBUG)));
    assertThat(Integer.valueOf(LogUtils.parseLevel(" debug ", LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_DEBUG)));

    /* String invalid */

    assertThat(Integer.valueOf(LogUtils.parseLevel(" not a level or number ", LogService.LOG_INFO)),
        equalTo(Integer.valueOf(LogService.LOG_INFO)));
  }

  @Test(timeout = 8000)
  public void testLevelToTopicLevel() {
    assertThat(LogUtils.levelToTopicLevel(LogService.LOG_ERROR - 1), equalTo("LOG_OTHER"));
    assertThat(LogUtils.levelToTopicLevel(LogService.LOG_ERROR), equalTo("LOG_ERROR"));
    assertThat(LogUtils.levelToTopicLevel(LogService.LOG_WARNING), equalTo("LOG_WARNING"));
    assertThat(LogUtils.levelToTopicLevel(LogService.LOG_INFO), equalTo("LOG_INFO"));
    assertThat(LogUtils.levelToTopicLevel(LogService.LOG_DEBUG), equalTo("LOG_DEBUG"));
    assertThat(LogUtils.levelToTopicLevel(LogService.LOG_DEBUG + 1), equalTo("LOG_OTHER"));
  }

  @Test(timeout = 8000)
  public void testLevelToSyslogString() {
    assertThat(LogUtils.levelToSyslogString(LogService.LOG_ERROR - 1), equalTo("*" + (LogService.LOG_ERROR - 1) + "*"));
    assertThat(LogUtils.levelToSyslogString(LogService.LOG_ERROR), equalTo("*ERROR*"));
    assertThat(LogUtils.levelToSyslogString(LogService.LOG_WARNING), equalTo("*WARN*"));
    assertThat(LogUtils.levelToSyslogString(LogService.LOG_INFO), equalTo("*INFO*"));
    assertThat(LogUtils.levelToSyslogString(LogService.LOG_DEBUG), equalTo("*DEBUG*"));
    assertThat(LogUtils.levelToSyslogString(LogService.LOG_DEBUG + 1), equalTo("*" + (LogService.LOG_DEBUG + 1) + "*"));
  }

  @Test(timeout = 8000)
  public void testBundleEventToString() {
    final MockBundle bundle = new MockBundle();

    assertThat(LogUtils.bundleEventToString(null), equalTo("BundleEvent <null>"));

    assertThat(LogUtils.bundleEventToString(new BundleEvent(BundleEvent.INSTALLED, bundle)),
        equalTo("BundleEvent INSTALLED"));
    assertThat(LogUtils.bundleEventToString(new BundleEvent(BundleEvent.STARTED, bundle)),
        equalTo("BundleEvent STARTED"));
    assertThat(LogUtils.bundleEventToString(new BundleEvent(BundleEvent.STOPPED, bundle)),
        equalTo("BundleEvent STOPPED"));
    assertThat(LogUtils.bundleEventToString(new BundleEvent(BundleEvent.UPDATED, bundle)),
        equalTo("BundleEvent UPDATED"));
    assertThat(LogUtils.bundleEventToString(new BundleEvent(BundleEvent.UNINSTALLED, bundle)),
        equalTo("BundleEvent UNINSTALLED"));
    assertThat(LogUtils.bundleEventToString(new BundleEvent(BundleEvent.RESOLVED, bundle)),
        equalTo("BundleEvent RESOLVED"));
    assertThat(LogUtils.bundleEventToString(new BundleEvent(BundleEvent.UNRESOLVED, bundle)),
        equalTo("BundleEvent UNRESOLVED"));

    assertThat(LogUtils.bundleEventToString(new BundleEvent(BundleEvent.STARTING, bundle)),
        equalTo("BundleEvent STARTING"));
    assertThat(LogUtils.bundleEventToString(new BundleEvent(BundleEvent.STOPPING, bundle)),
        equalTo("BundleEvent STOPPING"));
    assertThat(LogUtils.bundleEventToString(new BundleEvent(BundleEvent.LAZY_ACTIVATION, bundle)),
        equalTo("BundleEvent LAZY_ACTIVATION"));

    assertThat(LogUtils.bundleEventToString(new BundleEvent(BundleEvent.LAZY_ACTIVATION + 1, bundle)),
        equalTo("BundleEvent " + (BundleEvent.LAZY_ACTIVATION + 1)));
  }

  @Test(timeout = 8000)
  public void testFrameworkEventToString() {
    final MockBundle bundle = new MockBundle();

    assertThat(LogUtils.frameworkEventToString(null), equalTo("FrameworkEvent <null>"));

    assertThat(LogUtils.frameworkEventToString(new FrameworkEvent(FrameworkEvent.STARTED, bundle, null)),
        equalTo("FrameworkEvent STARTED"));
    assertThat(LogUtils.frameworkEventToString(new FrameworkEvent(FrameworkEvent.ERROR, bundle, null)),
        equalTo("FrameworkEvent ERROR"));
    assertThat(LogUtils.frameworkEventToString(new FrameworkEvent(FrameworkEvent.PACKAGES_REFRESHED, bundle, null)),
        equalTo("FrameworkEvent PACKAGES_REFRESHED"));
    assertThat(LogUtils.frameworkEventToString(new FrameworkEvent(FrameworkEvent.STARTLEVEL_CHANGED, bundle, null)),
        equalTo("FrameworkEvent STARTLEVEL_CHANGED"));
    assertThat(LogUtils.frameworkEventToString(new FrameworkEvent(FrameworkEvent.WARNING, bundle, null)),
        equalTo("FrameworkEvent WARNING"));
    assertThat(LogUtils.frameworkEventToString(new FrameworkEvent(FrameworkEvent.INFO, bundle, null)),
        equalTo("FrameworkEvent INFO"));

    assertThat(LogUtils.frameworkEventToString(new FrameworkEvent(FrameworkEvent.STOPPED, bundle, null)),
        equalTo("FrameworkEvent STOPPED"));
    assertThat(LogUtils.frameworkEventToString(new FrameworkEvent(FrameworkEvent.STOPPED_UPDATE, bundle, null)),
        equalTo("FrameworkEvent STOPPED_UPDATE"));
    assertThat(
        LogUtils
            .frameworkEventToString(new FrameworkEvent(FrameworkEvent.STOPPED_BOOTCLASSPATH_MODIFIED, bundle, null)),
        equalTo("FrameworkEvent STOPPED_BOOTCLASSPATH_MODIFIED"));
    assertThat(LogUtils.frameworkEventToString(new FrameworkEvent(FrameworkEvent.WAIT_TIMEDOUT, bundle, null)),
        equalTo("FrameworkEvent WAIT_TIMEDOUT"));

    assertThat(LogUtils.frameworkEventToString(new FrameworkEvent(FrameworkEvent.WAIT_TIMEDOUT + 1, bundle, null)),
        equalTo("FrameworkEvent " + (FrameworkEvent.WAIT_TIMEDOUT + 1)));
  }

  @Test(timeout = 8000)
  public void testServiceEventToString() {
    final ServiceReference<?> reference = new MockServiceReference<>();

    assertThat(LogUtils.serviceEventToString(null), equalTo("ServiceEvent <null>"));

    assertThat(LogUtils.serviceEventToString(new ServiceEvent(ServiceEvent.REGISTERED, reference)),
        equalTo("ServiceEvent REGISTERED"));
    assertThat(LogUtils.serviceEventToString(new ServiceEvent(ServiceEvent.MODIFIED, reference)),
        equalTo("ServiceEvent MODIFIED"));
    assertThat(LogUtils.serviceEventToString(new ServiceEvent(ServiceEvent.UNREGISTERING, reference)),
        equalTo("ServiceEvent UNREGISTERING"));

    assertThat(LogUtils.serviceEventToString(new ServiceEvent(ServiceEvent.MODIFIED_ENDMATCH, reference)),
        equalTo("ServiceEvent MODIFIED_ENDMATCH"));

    assertThat(LogUtils.serviceEventToString(new ServiceEvent(ServiceEvent.MODIFIED_ENDMATCH + 1, reference)),
        equalTo("ServiceEvent " + (ServiceEvent.MODIFIED_ENDMATCH + 1)));
  }

  @Test(timeout = 8000)
  public void testReadUinttFromObject() {
    /* null */

    assertThat(Integer.valueOf(LogUtils.readUintFromObject(null, -1)), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(LogUtils.readUintFromObject(null, 0)), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(LogUtils.readUintFromObject(null, 42)), equalTo(Integer.valueOf(42)));

    /* Wrong object */

    assertThat(Integer.valueOf(LogUtils.readUintFromObject(new Object(), -1)), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(LogUtils.readUintFromObject(new Object(), 0)), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(LogUtils.readUintFromObject(new Object(), 42)), equalTo(Integer.valueOf(42)));

    /* Number */

    assertThat(Integer.valueOf(LogUtils.readUintFromObject(Integer.valueOf(-1), 31)), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(LogUtils.readUintFromObject(Integer.valueOf(0), 32)), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(LogUtils.readUintFromObject(Long.valueOf(42), 33)), equalTo(Integer.valueOf(42)));

    /* String */

    assertThat(Integer.valueOf(LogUtils.readUintFromObject(" not a number ", -1)), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(LogUtils.readUintFromObject(" not a number ", 42)), equalTo(Integer.valueOf(42)));

    assertThat(Integer.valueOf(LogUtils.readUintFromObject(" -1 ", 51)), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(LogUtils.readUintFromObject(" 0 ", 52)), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(LogUtils.readUintFromObject(" 42 ", 53)), equalTo(Integer.valueOf(42)));

    assertThat(Integer.valueOf(LogUtils.readUintFromObject("", 54)), equalTo(Integer.valueOf(54)));
  }

  @Test(timeout = 8000)
  public void testOsgiPriorityToOSPriorityLinux() {
    int r;

    r = LogUtils.osgiPriorityToOSPriority(LogService.LOG_ERROR);
    assertThat(Integer.valueOf(r), equalTo(Integer.valueOf(LinuxLogConstants.LOG_ERR)));

    r = LogUtils.osgiPriorityToOSPriority(LogService.LOG_WARNING);
    assertThat(Integer.valueOf(r), equalTo(Integer.valueOf(LinuxLogConstants.LOG_WARNING)));

    r = LogUtils.osgiPriorityToOSPriority(LogService.LOG_INFO);
    assertThat(Integer.valueOf(r), equalTo(Integer.valueOf(LinuxLogConstants.LOG_INFO)));

    r = LogUtils.osgiPriorityToOSPriority(LogService.LOG_DEBUG);
    assertThat(Integer.valueOf(r), equalTo(Integer.valueOf(LinuxLogConstants.LOG_DEBUG)));

    r = LogUtils.osgiPriorityToOSPriority(Integer.MAX_VALUE);
    assertThat(Integer.valueOf(r), equalTo(Integer.valueOf(LinuxLogConstants.LOG_DEBUG)));
  }
}