package nl.pelagic.shutdownhook;

import org.junit.Ignore;

import nl.pelagic.shutdownhook.api.ShutdownHookParticipant;

@Ignore
@SuppressWarnings("javadoc")
public class DummyShutdownHookParticipant implements ShutdownHookParticipant {

  public boolean throwException = false;
  public int     callCount      = 0;

  @Override
  public void shutdownHook() {
    this.callCount++;
    if (this.throwException) {
      throw new RuntimeException();
    }
  }
}