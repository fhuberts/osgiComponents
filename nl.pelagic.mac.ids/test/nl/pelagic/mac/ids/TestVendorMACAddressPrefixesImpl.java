package nl.pelagic.mac.ids;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.googlecode.miyamoto.AnnotationProxyBuilder;

import nl.pelagic.mac.ids.api.VendorMACPrefix;
import nl.pelagic.testmocks.MockLogService;

@SuppressWarnings({
    "static-method",
    "javadoc"
})
public class TestVendorMACAddressPrefixesImpl {
  MockLogService               logger;
  VendorMACAddressPrefixesImpl impl;

  void fillProps(final AnnotationProxyBuilder<Config> config) {
    config.setProperty("vendorMACsFile", "testresources/vendorMacs.small.xml");
  }

  @Before
  public void setUp() {
    this.logger = new MockLogService();

    this.impl = new VendorMACAddressPrefixesImpl();
    this.impl.setLogger(this.logger);

    final AnnotationProxyBuilder<Config> config = AnnotationProxyBuilder.newBuilder(Config.class);
    this.fillProps(config);

    this.impl.activate(config.getProxedAnnotation());
  }

  @After
  public void tearDown() {
    this.impl.deactivate();
    this.impl.setLogger(null);
    this.impl = null;
    this.logger = null;
  }

  @Test(timeout = 8000)
  public void testInitial() {
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(this.impl.vendorToPrefix.toString(), Integer.valueOf(this.impl.vendorToPrefix.size()),
        equalTo(Integer.valueOf(8)));
    assertThat(this.impl.prefixToVendorMac.toString(), Integer.valueOf(this.impl.prefixToVendorMac.size()),
        equalTo(Integer.valueOf(17)));
  }

  @Test(timeout = 8000)
  public void testCheckFile() throws IOException {
    final File dir = new File("testresources/dir");

    boolean r;

    /* non-existing file */

    this.logger.logs.clear();

    File f = new File("testresources/mac.ids.does.not.exist");
    r = this.impl.checkFile(f);
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));

    /* dir */

    this.logger.logs.clear();

    r = this.impl.checkFile(dir);
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));

    /* unreadable file */

    this.logger.logs.clear();

    f = new File("testresources/vendorMacs.small.xml");
    final Path p = f.toPath();
    final Set<PosixFilePermission> orgPerms = Files.getPosixFilePermissions(p);

    try {
      Files.setPosixFilePermissions(p, new HashSet<PosixFilePermission>());
      r = this.impl.checkFile(f);
    }
    finally {
      Files.setPosixFilePermissions(p, orgPerms);
    }
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));

    /* non-existing file without logger */

    this.logger.logs.clear();

    this.impl.unsetLogger(this.logger);
    f = new File("testresources/mac.ids.does.not.exist");
    r = this.impl.checkFile(f);
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Boolean.valueOf(r), equalTo(Boolean.FALSE));
  }

  @Test(timeout = 8000)
  public void testReadDir() {
    final Map<String, Set<Long>> pre1 = this.impl.vendorToPrefix;
    final Map<Long, VendorMACPrefixImpl> pre2 = this.impl.prefixToVendorMac;

    this.impl.read("testresources/dir");

    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(pre1 == this.impl.vendorToPrefix), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(pre2 == this.impl.prefixToVendorMac), equalTo(Boolean.TRUE));
  }

  @Test(timeout = 8000)
  public void testReadInvalid() {
    final Map<String, Set<Long>> pre1 = this.impl.vendorToPrefix;
    final Map<Long, VendorMACPrefixImpl> pre2 = this.impl.prefixToVendorMac;

    this.impl.read("testresources/vendorMacs.invalid.xml");
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(pre1 == this.impl.vendorToPrefix), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(pre2 == this.impl.prefixToVendorMac), equalTo(Boolean.TRUE));

    /* no logger */

    this.logger.logs.clear();
    this.impl.unsetLogger(this.logger);

    this.impl.read("testresources/vendorMacs.invalid.xml");
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Boolean.valueOf(pre1 == this.impl.vendorToPrefix), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(pre2 == this.impl.prefixToVendorMac), equalTo(Boolean.TRUE));
  }

  @Test(timeout = 8000)
  public void testReadEmpty() {
    final Map<String, Set<Long>> pre1 = this.impl.vendorToPrefix;
    final Map<Long, VendorMACPrefixImpl> pre2 = this.impl.prefixToVendorMac;

    this.impl.read("testresources/vendorMacs.empty.xml");

    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Boolean.valueOf(pre1 == this.impl.vendorToPrefix), equalTo(Boolean.FALSE));
    assertThat(Boolean.valueOf(pre2 == this.impl.prefixToVendorMac), equalTo(Boolean.FALSE));
    assertThat(this.impl.vendorToPrefix.toString(), Integer.valueOf(this.impl.vendorToPrefix.size()),
        equalTo(Integer.valueOf(0)));
    assertThat(this.impl.prefixToVendorMac.toString(), Integer.valueOf(this.impl.prefixToVendorMac.size()),
        equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testReadWarnings() {
    Map<String, Set<Long>> pre1 = this.impl.vendorToPrefix;
    Map<Long, VendorMACPrefixImpl> pre2 = this.impl.prefixToVendorMac;

    this.impl.read("testresources/vendorMacs.warnings.xml");

    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(5)));
    assertThat(Boolean.valueOf(pre1 == this.impl.vendorToPrefix), equalTo(Boolean.FALSE));
    assertThat(Boolean.valueOf(pre2 == this.impl.prefixToVendorMac), equalTo(Boolean.FALSE));
    assertThat(this.impl.vendorToPrefix.toString(), Integer.valueOf(this.impl.vendorToPrefix.size()),
        equalTo(Integer.valueOf(8)));
    assertThat(this.impl.prefixToVendorMac.toString(), Integer.valueOf(this.impl.prefixToVendorMac.size()),
        equalTo(Integer.valueOf(17)));

    /* no logger */

    this.logger.logs.clear();
    this.impl.unsetLogger(this.logger);

    pre1 = this.impl.vendorToPrefix;
    pre2 = this.impl.prefixToVendorMac;

    this.impl.read("testresources/vendorMacs.warnings.xml");

    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Boolean.valueOf(pre1 == this.impl.vendorToPrefix), equalTo(Boolean.FALSE));
    assertThat(Boolean.valueOf(pre2 == this.impl.prefixToVendorMac), equalTo(Boolean.FALSE));
    assertThat(this.impl.vendorToPrefix.toString(), Integer.valueOf(this.impl.vendorToPrefix.size()),
        equalTo(Integer.valueOf(8)));
    assertThat(this.impl.prefixToVendorMac.toString(), Integer.valueOf(this.impl.prefixToVendorMac.size()),
        equalTo(Integer.valueOf(17)));

  }

  @Test(timeout = 8000)
  public void testReadOk() {
    final Map<String, Set<Long>> pre1 = this.impl.vendorToPrefix;
    final Map<Long, VendorMACPrefixImpl> pre2 = this.impl.prefixToVendorMac;

    this.impl.read("testresources/vendorMacs.xml");

    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Boolean.valueOf(pre1 == this.impl.vendorToPrefix), equalTo(Boolean.FALSE));
    assertThat(Boolean.valueOf(pre2 == this.impl.prefixToVendorMac), equalTo(Boolean.FALSE));
    assertThat(this.impl.vendorToPrefix.toString(), Integer.valueOf(this.impl.vendorToPrefix.size()),
        equalTo(Integer.valueOf(15316)));
    assertThat(this.impl.prefixToVendorMac.toString(), Integer.valueOf(this.impl.prefixToVendorMac.size()),
        equalTo(Integer.valueOf(19747)));
  }

  @Test(timeout = 8000)
  public void testGetVendorMacPrefix() {
    VendorMACPrefix r;

    r = this.impl.getVendorMacPrefix(-1);
    assertThat(r, nullValue());

    r = this.impl.getVendorMacPrefix(0);
    assertThat(r, notNullValue());
    assertThat(Long.valueOf(r.getPrefix()), equalTo(Long.valueOf(0)));
    assertThat(r.getVendor(), equalTo("Xerox"));

    r = this.impl.getVendorMacPrefix(0x0fL);
    assertThat(r, notNullValue());
    assertThat(Long.valueOf(r.getPrefix()), equalTo(Long.valueOf(0x0fL)));
    assertThat(r.getVendor(), equalTo("Next"));
  }

  @Test(timeout = 8000)
  public void testVendorPrefixes() {
    Set<Long> r;

    r = this.impl.getVendorPrefixes(null);
    assertThat(r, nullValue());

    r = this.impl.getVendorPrefixes(" ");
    assertThat(r, nullValue());

    r = this.impl.getVendorPrefixes(" some wildly unknown vendor really!!!!");
    assertThat(r, nullValue());

    r = this.impl.getVendorPrefixes("Xerox");
    assertThat(r, notNullValue());
    assertThat(Integer.valueOf(r.size()), equalTo(Integer.valueOf(10)));
  }

  @Test(timeout = 8000)
  public void testtModified() {
    final Map<String, Set<Long>> pre1 = this.impl.vendorToPrefix;
    final Map<Long, VendorMACPrefixImpl> pre2 = this.impl.prefixToVendorMac;

    /* no change */

    AnnotationProxyBuilder<Config> config = AnnotationProxyBuilder.newBuilder(Config.class);
    this.fillProps(config);

    this.impl.modified(config.getProxedAnnotation());
    assertThat(Boolean.valueOf(pre1 == this.impl.vendorToPrefix), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(pre2 == this.impl.prefixToVendorMac), equalTo(Boolean.TRUE));

    /* change (non-existing file) */

    config = AnnotationProxyBuilder.newBuilder(Config.class);
    this.fillProps(config);
    config.setProperty("vendorMACsFile", "testresources/mac.ids.does.not.exist");

    this.impl.modified(config.getProxedAnnotation());
    assertThat(Boolean.valueOf(pre1 == this.impl.vendorToPrefix), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(pre2 == this.impl.prefixToVendorMac), equalTo(Boolean.TRUE));

    /* change (other file) */

    config = AnnotationProxyBuilder.newBuilder(Config.class);
    this.fillProps(config);
    config.setProperty("vendorMACsFile", "testresources/vendorMacs.xml");

    this.impl.modified(config.getProxedAnnotation());
    assertThat(Boolean.valueOf(pre1 == this.impl.vendorToPrefix), equalTo(Boolean.FALSE));
    assertThat(Boolean.valueOf(pre2 == this.impl.prefixToVendorMac), equalTo(Boolean.FALSE));
  }
}