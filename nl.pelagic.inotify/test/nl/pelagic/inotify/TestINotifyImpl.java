package nl.pelagic.inotify;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.osgi.service.log.LogService;

import nl.pelagic.inotify.api.INotifyClient;
import nl.pelagic.inotify.api.INotifyEvent;
import nl.pelagic.inotify.api.INotifyException;
import nl.pelagic.inotify.api.INotifyLinuxErrno;
import nl.pelagic.inotify.api.INotifyMask;
import nl.pelagic.testmocks.MockLogService;

@SuppressWarnings({
    "javadoc", "static-method"
})
public class TestINotifyImpl implements INotifyClient {
  final String       testDir       = "testresources";
  final File         testDirFile   = new File(this.testDir);

  /*
   * INotifyClient
   */

  List<INotifyEvent> events        = new LinkedList<>();

  boolean            throwOnNotify = false;

  int                sleepOnNotify = 0;

  @Override
  public void notify(final INotifyEvent event) throws Exception {
    this.events.add(event);
    if (this.sleepOnNotify != 0) {
      Thread.sleep(this.sleepOnNotify);
    }
    if (this.throwOnNotify) {
      throw new Exception("throwOnNotify");
    }
  }

  /*
   * Setup
   */

  @BeforeClass
  public static void setUpBeforeClass() {
    final String jlp = System.getProperty("java.library.path");
    assertThat(jlp, notNullValue());
    final String[] split = jlp.trim().split("\\s*:\\s*");
    assertThat(jlp, Integer.valueOf(split.length), equalTo(Integer.valueOf(1)));
    assertThat(split[0],
        Boolean.valueOf(
            split[0].endsWith("/testresources/lib/release/64") || split[0].endsWith("/testresources/lib/release/32")),
        equalTo(Boolean.TRUE));
  }

  MockLogService logger;
  INotifyImpl    impl;

  void reset() {
    this.logger.logs.clear();
    this.events.clear();
    this.throwOnNotify = false;
    this.sleepOnNotify = 0;
  }

  @Before
  public void setUp() {
    this.logger = new MockLogService();
    this.impl = new INotifyImpl();
    this.impl.setLogger(this.logger);

    assertThat(this.impl.logger, notNullValue());
    assertThat(this.impl.logger.get(), equalTo((LogService) this.logger));
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));

    this.checkState(CheckState.INITIAL);

    INotifyException ex = null;

    try {
      this.impl.activate();
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(ex, nullValue());
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));

    this.checkState(CheckState.ACTIVATED);

    /* reset tests */
    this.reset();
  }

  @After
  public void tearDown() {
    this.events.clear();
    this.logger.logs.clear();

    this.impl.deactivate();
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    this.checkState(CheckState.DEACTIVATED);

    this.impl.unsetLogger(this.logger);
    this.impl = null;
    this.logger = null;
  }

  /*
   * Checks
   */

  public enum CheckState {
    INITIAL, ACTIVATED, DEACTIVATED
  }

  int commonStateErrno = 0;

  void checkCommonState() {
    assertThat(Integer.valueOf(this.impl.inotifyEventLoopThreadResult),
        equalTo(Integer.valueOf(this.commonStateErrno)));

    assertThat(this.impl.eventQueue, notNullValue());
    assertThat(Integer.valueOf(this.impl.eventQueue.size()), equalTo(Integer.valueOf(0)));

    assertThat(this.impl.clientMap, notNullValue());
    assertThat(Integer.valueOf(this.impl.clientMap.size()), equalTo(Integer.valueOf(0)));
  }

  void checkState(final CheckState state) {
    this.checkCommonState();

    switch (state) {
      case INITIAL:
        assertThat(this.impl.inotifyEventLoopThread, nullValue());
        assertThat(Boolean.valueOf(this.impl.nativeIsRunning.get()), equalTo(Boolean.FALSE));
        assertThat(Boolean.valueOf(this.impl.run.get()), equalTo(Boolean.TRUE));
        break;

      case ACTIVATED:
        assertThat(this.impl.inotifyEventLoopThread, notNullValue());
        assertThat(Boolean.valueOf(this.impl.nativeIsRunning.get()), equalTo(Boolean.TRUE));
        assertThat(Boolean.valueOf(this.impl.run.get()), equalTo(Boolean.TRUE));
        break;

      case DEACTIVATED:
        assertThat(this.impl.inotifyEventLoopThread, nullValue());
        assertThat(Boolean.valueOf(this.impl.nativeIsRunning.get()), equalTo(Boolean.FALSE));
        assertThat(Boolean.valueOf(this.impl.run.get()), equalTo(Boolean.FALSE));
        break;

      default:
        assert (false);
        break;
    }
  }

  /*
   * Tests
   */

  @Test(timeout = 8000)
  public void testSetLogger() {
    assertThat(this.impl.logger, notNullValue());
    assertThat(this.impl.logger.get(), equalTo((LogService) this.logger));

    final MockLogService logger2 = new MockLogService();
    this.impl.unsetLogger(logger2);
    assertThat(this.impl.logger.get(), equalTo((LogService) this.logger));

    this.impl.unsetLogger(this.logger);
    assertThat(this.impl.logger.get(), nullValue());

    this.impl.setLogger(this.logger);
    assertThat(this.impl.logger.get(), equalTo((LogService) this.logger));
  }

  @Test(timeout = 8000)
  public void testGetStrError() {
    assertThat(this.impl.getStrError(-1), equalTo("Unknown error -1"));
    assertThat(this.impl.getStrError(INotifyLinuxErrno.SUCCESS), equalTo("Success"));
    assertThat(this.impl.getStrError(INotifyLinuxErrno.EPERM), equalTo("Operation not permitted"));
  }

  @SuppressWarnings("null")
  @Test(timeout = 8000)
  public void testAddWatchAndRemoveWatch() throws InterruptedException {
    int errno;
    INotifyException ex;
    int wd = -1;

    /*
     * add null client
     */
    wd = -1;
    ex = null;
    errno = 0;
    try {
      wd = this.impl.addWatch(null, "testresources", INotifyMask.IN_CREATE);
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(wd), equalTo(Integer.valueOf(-1)));
    assertThat(ex, notNullValue());
    errno = ex.getErrno();
    assertThat(Integer.valueOf(errno), equalTo(Integer.valueOf(INotifyLinuxErrno.EINVAL)));
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(0)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();

    /*
     * add 1st: invalid state & EPERM
     */
    this.impl.callbackTestMap = new TreeMap<>();

    this.impl.callbackTestMap.clear();
    this.impl.callbackTestMap.put("addWatch", Integer.valueOf(1));

    wd = -1;
    ex = null;
    errno = 0;
    try {
      wd = this.impl.addWatch(this, "testresources", INotifyMask.IN_CREATE);
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(wd), equalTo(Integer.valueOf(-1)));
    assertThat(ex, notNullValue());
    errno = ex.getErrno();
    assertThat(Integer.valueOf(errno), equalTo(Integer.valueOf(INotifyLinuxErrno.EPERM)));
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(0)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();

    /*
     * add 2nd: path string failure & ENOMEM
     */

    this.impl.callbackTestMap.clear();
    this.impl.callbackTestMap.put("addWatch", Integer.valueOf(2));

    wd = -1;
    ex = null;
    errno = 0;
    try {
      wd = this.impl.addWatch(this, "testresources", INotifyMask.IN_CREATE);
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(wd), equalTo(Integer.valueOf(-1)));
    assertThat(ex, notNullValue());
    errno = ex.getErrno();
    assertThat(Integer.valueOf(errno), equalTo(Integer.valueOf(INotifyLinuxErrno.ENOMEM)));
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(0)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();

    /*
     * add 3rd: path does not exist & ENOENT
     */

    this.impl.callbackTestMap.clear();

    wd = -1;
    ex = null;
    errno = 0;
    try {
      wd = this.impl.addWatch(this, "testresources/this/path/does/not/exist/really!", INotifyMask.IN_CREATE);
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(wd), equalTo(Integer.valueOf(-1)));
    assertThat(ex, notNullValue());
    errno = ex.getErrno();
    assertThat(Integer.valueOf(errno), equalTo(Integer.valueOf(INotifyLinuxErrno.ENOENT)));
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(0)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();

    /*
     * add 4th: OK
     */

    this.impl.callbackTestMap.clear();

    wd = -1;
    ex = null;
    errno = 0;
    try {
      wd = this.impl.addWatch(this, "testresources", INotifyMask.IN_CREATE);
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(wd), not(equalTo(Integer.valueOf(-1))));
    assertThat(ex, nullValue());
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(1)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();

    /*
     * remove 1st: invalid state & EPERM
     */

    this.impl.callbackTestMap.clear();
    this.impl.callbackTestMap.put("removeWatch", Integer.valueOf(1));

    /* wd from OK addWatch */
    ex = null;
    errno = 0;
    try {
      this.impl.removeWatch(wd, true);
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(wd), not(equalTo(Integer.valueOf(-1))));
    assertThat(ex, notNullValue());
    errno = ex.getErrno();
    assertThat(Integer.valueOf(errno), equalTo(Integer.valueOf(INotifyLinuxErrno.EPERM)));
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(1)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();

    /*
     * remove 2nd: invalid watch & EINVAL
     */

    this.impl.callbackTestMap.clear();

    /* wd from OK addWatch */
    ex = null;
    errno = 0;
    try {
      this.impl.removeWatch(-1, true);
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(wd), not(equalTo(Integer.valueOf(-1))));
    assertThat(ex, notNullValue());
    errno = ex.getErrno();
    assertThat(Integer.valueOf(errno), equalTo(Integer.valueOf(INotifyLinuxErrno.EINVAL)));
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(1)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();

    /*
     * remove 3rd: OK
     */

    this.impl.callbackTestMap.clear();

    /* wd from OK addWatch */
    ex = null;
    errno = 0;
    try {
      this.impl.removeWatch(wd, false);
    }
    catch (final INotifyException e) {
      ex = e;
    }

    while (this.impl.clientMap.size() != 0) {
      Thread.sleep(10);
    }

    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(wd), not(equalTo(Integer.valueOf(-1))));
    assertThat(ex, nullValue());
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(0)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(1)));
    final INotifyEvent event = this.events.get(0);
    assertThat(event.toString(), Integer.valueOf(event.getWd()), equalTo(Integer.valueOf(wd)));
    assertThat(event.toString(), event.getName(), equalTo(""));
    assertThat(event.toString(), Long.valueOf(event.getMask()), equalTo(Long.valueOf(INotifyMask.IN_IGNORED)));
    assertThat(event.toString(), Long.valueOf(event.getCookie()), equalTo(Long.valueOf(0)));
    this.logger.logs.clear();
    this.events.clear();

    /*
     * add 5th: OK
     */

    this.impl.callbackTestMap.clear();

    wd = -1;
    ex = null;
    errno = 0;
    try {
      wd = this.impl.addWatch(this, "testresources", INotifyMask.IN_CREATE);
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(wd), not(equalTo(Integer.valueOf(-1))));
    assertThat(ex, nullValue());
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(1)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();

    /*
     * remove 4th: OK
     */

    this.impl.callbackTestMap.clear();

    /* wd from OK addWatch */
    ex = null;
    errno = 0;
    try {
      this.impl.removeWatch(wd, true);
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(wd), not(equalTo(Integer.valueOf(-1))));
    assertThat(ex, nullValue());
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(0)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();

    this.impl.callbackTestMap.clear();
  }

  @Test(timeout = 8000)
  public void testCallbackLog() {

    /* with logger */
    this.impl.callbackLog(LinuxLogLevels.LOG_CRIT, "test callbackLog 1");
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(this.logger.logs.get(0), equalTo("ERROR test callbackLog 1"));
    this.logger.logs.clear();

    this.impl.unsetLogger(this.logger);

    this.impl.callbackLog(LinuxLogLevels.LOG_CRIT, "test callbackLog 2");
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();
  }

  @Test(timeout = 8000)
  public void testRunEvents() throws IOException, InterruptedException {
    int wd;
    Exception ex;

    /* add watch */
    wd = -1;
    ex = null;
    try {
      wd = this.impl.addWatch(this, "testresources", INotifyMask.IN_CREATE);
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(wd), not(equalTo(Integer.valueOf(-1))));
    assertThat(ex, nullValue());
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(1)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();

    /* create normal event */

    File tmpFile1 = File.createTempFile("demo-1-", null, this.testDirFile);
    while (this.events.isEmpty()) {
      Thread.sleep(10);
    }
    try {
      assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
      assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(1)));
      final INotifyEvent event = this.events.get(0);
      assertThat(event.toString(), Integer.valueOf(event.getWd()), equalTo(Integer.valueOf(wd)));
      assertThat(event.toString(), event.getName(), equalTo(tmpFile1.getName()));
      assertThat(event.toString(), Long.valueOf(event.getMask()), equalTo(Long.valueOf(INotifyMask.IN_CREATE)));
      assertThat(event.toString(), Long.valueOf(event.getCookie()), equalTo(Long.valueOf(0)));
    }
    finally {
      tmpFile1.delete();
    }
    this.events.clear();
    this.logger.logs.clear();

    /* create event that throws an exception during handling, with logger */
    this.throwOnNotify = true;
    tmpFile1 = File.createTempFile("demo-1-", null, this.testDirFile);
    while (this.events.isEmpty() || this.logger.logs.isEmpty()) {
      Thread.sleep(10);
    }
    try {
      assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
      assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(1)));
      final INotifyEvent event = this.events.get(0);
      assertThat(event.toString(), Integer.valueOf(event.getWd()), equalTo(Integer.valueOf(wd)));
      assertThat(event.toString(), event.getName(), equalTo(tmpFile1.getName()));
      assertThat(event.toString(), Long.valueOf(event.getMask()), equalTo(Long.valueOf(INotifyMask.IN_CREATE)));
      assertThat(event.toString(), Long.valueOf(event.getCookie()), equalTo(Long.valueOf(0)));
    }
    finally {
      tmpFile1.delete();
    }
    this.events.clear();
    this.logger.logs.clear();
    this.throwOnNotify = false;

    /* create event that throws an exception during handling, without logger */
    this.impl.unsetLogger(this.logger);
    this.throwOnNotify = true;
    tmpFile1 = File.createTempFile("demo-1-", null, this.testDirFile);
    while (this.events.isEmpty()) {
      Thread.sleep(10);
    }
    try {
      assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
      assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(1)));
      final INotifyEvent event = this.events.get(0);
      assertThat(event.toString(), Integer.valueOf(event.getWd()), equalTo(Integer.valueOf(wd)));
      assertThat(event.toString(), event.getName(), equalTo(tmpFile1.getName()));
      assertThat(event.toString(), Long.valueOf(event.getMask()), equalTo(Long.valueOf(INotifyMask.IN_CREATE)));
      assertThat(event.toString(), Long.valueOf(event.getCookie()), equalTo(Long.valueOf(0)));
    }
    finally {
      tmpFile1.delete();
    }
    this.events.clear();
    this.logger.logs.clear();
    this.throwOnNotify = false;

    /* remove watch */
    /* wd from OK addWatch */
    ex = null;
    try {
      this.impl.removeWatch(wd, true);
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(wd), not(equalTo(Integer.valueOf(-1))));
    assertThat(ex, nullValue());
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(0)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.events.clear();
    this.logger.logs.clear();
  }

  @Test(timeout = 8000)
  public void testActivateAgain() throws INotifyException {
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(0)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));

    /* activate again */

    this.impl.activate();

    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(0)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
  }

  @SuppressWarnings("null")
  @Test(timeout = 8000)
  public void testActivateFailure1() {
    this.tearDown();

    /*
     * activate 1st: callbacks not found & ENOENT
     */

    this.logger = new MockLogService();
    this.impl = new INotifyImpl();
    this.impl.setLogger(this.logger);

    assertThat(this.impl.logger, notNullValue());
    assertThat(this.impl.logger.get(), equalTo((LogService) this.logger));
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));

    this.checkState(CheckState.INITIAL);

    INotifyException ex = null;

    this.impl.callbackTestMap = new TreeMap<>();
    this.impl.callbackTestMap.clear();
    this.impl.callbackTestMap.put("activate", Integer.valueOf(1));

    try {
      this.impl.activate();
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(ex, notNullValue());
    final int errno = ex.getErrno();
    assertThat(Integer.valueOf(errno), equalTo(Integer.valueOf(INotifyLinuxErrno.ENOENT)));
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));

    this.checkState(CheckState.INITIAL);

    /* to appease tearDown */
    this.impl.run.set(false);

    this.impl.callbackTestMap.clear();
  }

  @SuppressWarnings("null")
  @Test(timeout = 8000)
  public void testActivateFailure2() {
    this.tearDown();

    /*
     * activate 2nd: invalid state & EPERM
     */

    this.logger = new MockLogService();
    this.impl = new INotifyImpl();
    this.impl.setLogger(this.logger);

    assertThat(this.impl.logger, notNullValue());
    assertThat(this.impl.logger.get(), equalTo((LogService) this.logger));
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));

    this.checkState(CheckState.INITIAL);

    INotifyException ex = null;

    this.impl.callbackTestMap = new TreeMap<>();
    this.impl.callbackTestMap.clear();
    this.impl.callbackTestMap.put("activate", Integer.valueOf(2));

    try {
      this.impl.activate();
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(ex, notNullValue());
    final int errno = ex.getErrno();
    assertThat(Integer.valueOf(errno), equalTo(Integer.valueOf(INotifyLinuxErrno.EPERM)));
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));

    this.checkState(CheckState.INITIAL);

    /* to appease tearDown */
    this.impl.run.set(false);

    this.impl.callbackTestMap.clear();
  }

  @SuppressWarnings("null")
  @Test(timeout = 8000)
  public void testActivateFailure3() {
    this.tearDown();

    /*
     * activate 3rd: event buffer allocation failure & ENOMEM
     */

    this.logger = new MockLogService();
    this.impl = new INotifyImpl();
    this.impl.setLogger(this.logger);

    assertThat(this.impl.logger, notNullValue());
    assertThat(this.impl.logger.get(), equalTo((LogService) this.logger));
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));

    this.checkState(CheckState.INITIAL);

    INotifyException ex = null;

    this.impl.callbackTestMap = new TreeMap<>();
    this.impl.callbackTestMap.clear();
    this.impl.callbackTestMap.put("activate", Integer.valueOf(3));

    try {
      this.impl.activate();
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(ex, notNullValue());
    final int errno = ex.getErrno();
    assertThat(Integer.valueOf(errno), equalTo(Integer.valueOf(INotifyLinuxErrno.ENOMEM)));
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));

    this.checkState(CheckState.INITIAL);

    /* to appease tearDown */
    this.impl.run.set(false);

    this.impl.callbackTestMap.clear();
  }

  @SuppressWarnings("null")
  @Test(timeout = 8000)
  public void testActivateFailure4() throws InterruptedException {
    this.tearDown();

    /*
     * activate 4th: event loop failure & EPERM
     */

    this.logger = new MockLogService();
    this.impl = new INotifyImpl();
    this.impl.setLogger(this.logger);

    assertThat(this.impl.logger, notNullValue());
    assertThat(this.impl.logger.get(), equalTo((LogService) this.logger));
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));

    this.checkState(CheckState.INITIAL);

    INotifyException ex = null;

    this.impl.callbackTestMap = new TreeMap<>();
    this.impl.callbackTestMap.clear();
    this.impl.callbackTestMap.put("eventLoop", Integer.valueOf(1));

    try {
      this.impl.activate();
    }
    catch (final INotifyException e) {
      ex = e;
    }
    while (this.logger.logs.isEmpty()) {
      Thread.sleep(100);
    }
    assertThat(ex, notNullValue());
    final int errno = ex.getErrno();
    assertThat(Integer.valueOf(errno), equalTo(Integer.valueOf(INotifyLinuxErrno.EPERM)));
    this.commonStateErrno = INotifyLinuxErrno.EPERM;
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();

    this.checkState(CheckState.INITIAL);

    /* to appease checkstate */
    this.impl.run.set(false);

    this.impl.callbackTestMap.clear();
  }

  @SuppressWarnings("null")
  @Test(timeout = 8000)
  public void testActivateFailure5() {
    this.tearDown();

    /*
     * activate 5th: event loop failure & EINVAL
     */

    this.logger = new MockLogService();
    this.impl = new INotifyImpl();
    this.impl.setLogger(this.logger);

    assertThat(this.impl.logger, notNullValue());
    assertThat(this.impl.logger.get(), equalTo((LogService) this.logger));
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));

    this.checkState(CheckState.INITIAL);

    INotifyException ex = null;

    this.impl.callbackTestMap = new TreeMap<>();
    this.impl.callbackTestMap.clear();
    this.impl.callbackTestMap.put("eventLoop", Integer.valueOf(2));

    try {
      this.impl.activate();
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(ex, notNullValue());
    final int errno = ex.getErrno();
    assertThat(Integer.valueOf(errno), equalTo(Integer.valueOf(INotifyLinuxErrno.EINVAL)));
    this.commonStateErrno = INotifyLinuxErrno.EINVAL;
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();

    this.checkState(CheckState.INITIAL);

    /* to appease checkstate */
    this.impl.run.set(false);

    this.impl.callbackTestMap.clear();
  }

  @SuppressWarnings("null")
  @Test(timeout = 8000)
  public void testActivateFailure6() {
    this.tearDown();

    /*
     * activate 5th: event loop failure & EMFILE
     */

    this.logger = new MockLogService();
    this.impl = new INotifyImpl();
    this.impl.setLogger(this.logger);

    assertThat(this.impl.logger, notNullValue());
    assertThat(this.impl.logger.get(), equalTo((LogService) this.logger));
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));

    this.checkState(CheckState.INITIAL);

    INotifyException ex = null;

    this.impl.callbackTestMap = new TreeMap<>();
    this.impl.callbackTestMap.clear();
    this.impl.callbackTestMap.put("eventLoop", Integer.valueOf(3));

    try {
      this.impl.activate();
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(ex, notNullValue());
    final int errno = ex.getErrno();
    assertThat(Integer.valueOf(errno), equalTo(Integer.valueOf(INotifyLinuxErrno.EMFILE)));
    this.commonStateErrno = INotifyLinuxErrno.EMFILE;
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();

    this.checkState(CheckState.INITIAL);

    /* to appease checkstate */
    this.impl.run.set(false);

    this.impl.callbackTestMap.clear();
  }

  @Test(timeout = 8000)
  public void testDeactivateFailure1() {
    this.tearDown();

    /*
     * deactivate 1st: invalid state & EPERM
     */

    this.logger = new MockLogService();
    this.impl = new INotifyImpl();
    this.impl.setLogger(this.logger);

    assertThat(this.impl.logger, notNullValue());
    assertThat(this.impl.logger.get(), equalTo((LogService) this.logger));
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));

    this.checkState(CheckState.INITIAL);

    this.impl.callbackTestMap = new TreeMap<>();
    this.impl.callbackTestMap.clear();
    this.impl.callbackTestMap.put("deactivate", Integer.valueOf(1));

    this.impl.deactivate();
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));

    this.checkState(CheckState.DEACTIVATED);

    this.impl.callbackTestMap.clear();
  }

  @Test(timeout = 8000)
  public void testDeactivateWithWatches() throws IOException, InterruptedException {
    int wd;
    Exception ex;

    /*
     * add watch
     */

    wd = -1;
    ex = null;
    try {
      wd = this.impl.addWatch(this, "testresources", INotifyMask.IN_CREATE);
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(wd), not(equalTo(Integer.valueOf(-1))));
    assertThat(ex, nullValue());
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(1)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();

    /*
     * generate event and set slow processing
     */

    this.sleepOnNotify = 10000;

    final File tmpFile1 = File.createTempFile("demo-1-", null, this.testDirFile);
    while (this.events.isEmpty()) {
      Thread.sleep(1);
    }
    try {
      assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
      assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(1)));
      final INotifyEvent event = this.events.get(0);
      assertThat(event.toString(), Integer.valueOf(event.getWd()), equalTo(Integer.valueOf(wd)));
      assertThat(event.toString(), event.getName(), equalTo(tmpFile1.getName()));
      assertThat(event.toString(), Long.valueOf(event.getMask()), equalTo(Long.valueOf(INotifyMask.IN_CREATE)));
      assertThat(event.toString(), Long.valueOf(event.getCookie()), equalTo(Long.valueOf(0)));
    }
    finally {
      tmpFile1.delete();
    }
    this.events.clear();
    this.logger.logs.clear();

    /*
     * deactivate
     */

    this.impl.deactivate();

    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(2)));
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(0)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(1)));
    final INotifyEvent event = this.events.get(0);
    assertThat(event.toString(), Integer.valueOf(event.getWd()), equalTo(Integer.valueOf(wd)));
    assertThat(event.toString(), event.getName(), equalTo(""));
    assertThat(event.toString(), Long.valueOf(event.getMask()), equalTo(Long.valueOf(INotifyMask.IN_IGNORED)));
    assertThat(event.toString(), Long.valueOf(event.getCookie()), equalTo(Long.valueOf(0)));
    this.events.clear();
    this.logger.logs.clear();
  }

  @Test(timeout = 8000)
  public void testDeactivateWithWatchesThrowing() {
    int wd;
    Exception ex;

    /*
     * add watch
     */

    wd = -1;
    ex = null;
    try {
      wd = this.impl.addWatch(this, "testresources", INotifyMask.IN_CREATE);
    }
    catch (final INotifyException e) {
      ex = e;
    }
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(wd), not(equalTo(Integer.valueOf(-1))));
    assertThat(ex, nullValue());
    assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
        equalTo(Integer.valueOf(1)));
    assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(0)));
    this.logger.logs.clear();

    /* fake watch */
    this.impl.clientMap.put(Integer.valueOf(42), this);

    this.throwOnNotify = true;

    /*
     * deactivate
     */

    try {
      this.impl.deactivate();

      assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(2)));
      assertThat(this.impl.clientMap.toString(), Integer.valueOf(this.impl.clientMap.size()),
          equalTo(Integer.valueOf(0)));
      assertThat(this.events.toString(), Integer.valueOf(this.events.size()), equalTo(Integer.valueOf(1)));
      final INotifyEvent event = this.events.get(0);
      assertThat(event.toString(), Integer.valueOf(event.getWd()), equalTo(Integer.valueOf(wd)));
      assertThat(event.toString(), event.getName(), equalTo(""));
      assertThat(event.toString(), Long.valueOf(event.getMask()), equalTo(Long.valueOf(INotifyMask.IN_IGNORED)));
      assertThat(event.toString(), Long.valueOf(event.getCookie()), equalTo(Long.valueOf(0)));
      this.events.clear();
      this.logger.logs.clear();
    }
    finally {
      this.impl.clientMap.remove(Integer.valueOf(42));
    }
  }

  @Test(timeout = 8000, expected = INotifyException.class)
  public void testLoadLibraryThrowing() throws INotifyException {
    INotifyImpl.loadLibrary("somedummylibrarynamethatdoesnotexistreally");
  }

  @Test(timeout = 8000)
  public void testWaitUntilEventQueueIsEmpty() {
    this.impl.clientMap.put(Integer.valueOf(-1), this);
    final ThreadStopper stopper = new ThreadStopper(this.impl, Thread.currentThread(), 100);
    stopper.start();
    this.impl.waitUntilEventQueueIsEmpty();
    this.impl.clientMap.remove(Integer.valueOf(-1));
  }

  @Test(timeout = 8000)
  public void testWaitForEventQueueReaderThreadEnd() {
    final ThreadStopper stopper = new ThreadStopper(null, Thread.currentThread(), 100);
    stopper.start();
    this.impl.waitForEventQueueReaderThreadEnd();
  }

  @Test(timeout = 8000)
  public void testWaitForEventLoopEnd() {
    final ThreadStopper stopper = new ThreadStopper(null, Thread.currentThread(), 100);
    stopper.start();
    this.impl.waitForEventLoopEnd();
  }

  @Test(timeout = 8000)
  public void testLogWhenEventLoopFailedFail() {

    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));

    this.impl.logWhenEventLoopFailed();
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(0)));

    this.impl.inotifyEventLoopThreadResult = 1;

    this.impl.logWhenEventLoopFailed();
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));

    this.impl.inotifyEventLoopThreadResult = 0;

    this.impl.logWhenEventLoopFailed();
    assertThat(this.logger.logs.toString(), Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
  }

  @Test(timeout = 8000)
  public void testWaitForRunningAndGet() {
    final ThreadStopper stopper = new ThreadStopper(null, Thread.currentThread(), 100);
    stopper.start();
    final boolean r = this.impl.waitForRunningAndGet();

    assertThat(Boolean.valueOf(r), equalTo(Boolean.TRUE));
  }
}