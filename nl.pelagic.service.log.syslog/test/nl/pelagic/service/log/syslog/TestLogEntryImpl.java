package nl.pelagic.service.log.syslog;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;

import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentConstants;
import org.osgi.service.event.Event;
import org.osgi.service.log.LogService;

@SuppressWarnings({
    "static-method",
    "javadoc"
})
public class TestLogEntryImpl {
  @Test(timeout = 8000)
  public void testInstance() {
    assertThat(new LogEntryImpl(1, null, null, 2, "msg", null, null).toString(), notNullValue());
  }

  @Test(timeout = 8000)
  public void testLogEntryImpl() {
    final MockServiceReference<?> reference = new MockServiceReference<>();
    final MockBundle bundle = new MockBundle();
    final MockBundle bundle2 = new MockBundle();
    final String message = "message";
    final Exception exception = new Exception();

    reference.bundle = bundle2;

    LogEntryImpl impl;

    impl = new LogEntryImpl(41, null, null, LogService.LOG_INFO, null, null, null);
    assertThat(Long.valueOf(impl.getTime()), equalTo(Long.valueOf(41)));
    assertThat(impl.getServiceReference(), nullValue());
    assertThat(impl.getBundle(), nullValue());
    assertThat(Integer.valueOf(impl.getLevel()), equalTo(Integer.valueOf(LogService.LOG_INFO)));
    assertThat(impl.getMessage(), nullValue());
    assertThat(impl.getException(), nullValue());

    impl = new LogEntryImpl(42, bundle, reference, LogService.LOG_ERROR, message, exception, null);
    assertThat(Long.valueOf(impl.getTime()), equalTo(Long.valueOf(42)));
    assert (impl.getServiceReference() == reference);
    assert (impl.getBundle() == bundle);
    assertThat(Integer.valueOf(impl.getLevel()), equalTo(Integer.valueOf(LogService.LOG_ERROR)));
    assertThat(impl.getMessage(), equalTo(message));
    assert (impl.getException() == exception);

    impl = new LogEntryImpl(42, null, reference, LogService.LOG_ERROR, message, exception, null);
    assertThat(Long.valueOf(impl.getTime()), equalTo(Long.valueOf(42)));
    assert (impl.getServiceReference() == reference);
    assert (impl.getBundle() == bundle2);
    assertThat(Integer.valueOf(impl.getLevel()), equalTo(Integer.valueOf(LogService.LOG_ERROR)));
    assertThat(impl.getMessage(), equalTo(message));
    assert (impl.getException() == exception);
  }

  @Test(timeout = 8000)
  public void testToLogMessage() {
    final MockServiceReference<?> reference = new MockServiceReference<>();
    final MockBundle bundle = new MockBundle();
    final String message = "message";
    final Exception exception = new Exception("ex");

    LogEntryImpl impl;
    String r;
    final String lvl = LogUtils.levelToSyslogString(LogService.LOG_INFO);

    impl = new LogEntryImpl(41, null, null, LogService.LOG_INFO, null, null, null);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl));

    /* bundle */

    bundle.bsn = null;
    bundle.location = null;
    impl = new LogEntryImpl(41, bundle, null, LogService.LOG_INFO, message, null, null);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + "[" + bundle.bundleId + "]"
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    bundle.bsn = "bsn1";
    bundle.location = null;
    impl = new LogEntryImpl(41, bundle, null, LogService.LOG_INFO, message, null, null);
    r = impl.toLogMessage();
    assertThat(r, equalTo(
        lvl + LogEntryImpl.LM_FIELD_SEPARATOR + "[" + "bsn1" + "]" + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    bundle.bsn = null;
    bundle.location = "/some/where.jar";
    impl = new LogEntryImpl(41, bundle, null, LogService.LOG_INFO, message, null, null);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + "[" + bundle.location + "]"
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    bundle.bsn = "bsn1";
    bundle.location = null;

    /* reference on ServiceEvent */

    final MockServiceReference<?> sr = new MockServiceReference<>();
    final ServiceEvent ev = new ServiceEvent(ServiceEvent.MODIFIED, sr);

    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, ev);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME + " Service []"
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.put(Constants.SERVICE_DESCRIPTION, "service description");
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, ev);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + " Service [service description]" + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.put(ComponentConstants.COMPONENT_NAME, "component name");
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, ev);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + " Service [component name]" + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.put(Constants.SERVICE_PID, "service pid");
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, ev);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + " Service [service pid]" + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.put(Constants.SERVICE_ID, "service id");
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, ev);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + " Service [service pid, service id]" + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    final String[] objs1 = {
        "class1",
        "class2"
    };
    reference.props.put(Constants.OBJECTCLASS, objs1);
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, ev);
    r = impl.toLogMessage();
    assertThat(r,
        equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
            + " Service [service pid, service id, " + Arrays.toString(objs1) + "]" + LogEntryImpl.LM_FIELD_SEPARATOR
            + message));

    reference.props.remove(Constants.SERVICE_PID);
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, ev);
    r = impl.toLogMessage();
    assertThat(r,
        equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
            + " Service [component name, service id, " + Arrays.toString(objs1) + "]" + LogEntryImpl.LM_FIELD_SEPARATOR
            + message));

    reference.props.remove(ComponentConstants.COMPONENT_NAME);
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, ev);
    r = impl.toLogMessage();
    assertThat(r,
        equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
            + " Service [service description, service id, " + Arrays.toString(objs1) + "]"
            + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.remove(Constants.SERVICE_DESCRIPTION);
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, ev);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + " Service [service id, " + Arrays.toString(objs1) + "]" + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.remove(Constants.SERVICE_ID);
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, ev);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME + " Service ["
        + Arrays.toString(objs1) + "]" + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.clear();

    /* reference with BundleEvent */

    final MockBundle bundleb = new MockBundle();
    final BundleEvent evb = new BundleEvent(BundleEvent.INSTALLED, bundleb);

    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, evb);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.put(Constants.SERVICE_DESCRIPTION, "service description");
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, evb);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.put(ComponentConstants.COMPONENT_NAME, "component name");
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, evb);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.put(Constants.SERVICE_PID, "service pid");
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, evb);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.put(Constants.SERVICE_ID, "service id");
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, evb);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    final String[] objs2 = {
        "class1",
        "class2"
    };
    reference.props.put(Constants.OBJECTCLASS, objs2);
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, evb);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.remove(Constants.SERVICE_PID);
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, evb);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.remove(ComponentConstants.COMPONENT_NAME);
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, evb);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.remove(Constants.SERVICE_DESCRIPTION);
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, evb);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.remove(Constants.SERVICE_ID);
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, evb);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.clear();

    /* reference */

    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, null);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.put(Constants.SERVICE_DESCRIPTION, "service description");
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, null);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.put(ComponentConstants.COMPONENT_NAME, "component name");
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, null);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.put(Constants.SERVICE_PID, "service pid");
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, null);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.put(Constants.SERVICE_ID, "service id");
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, null);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    final String[] objs3 = {
        "class1",
        "class2"
    };
    reference.props.put(Constants.OBJECTCLASS, objs3);
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, null);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.remove(Constants.SERVICE_PID);
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, null);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.remove(ComponentConstants.COMPONENT_NAME);
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, null);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.remove(Constants.SERVICE_DESCRIPTION);
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, null);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.remove(Constants.SERVICE_ID);
    impl = new LogEntryImpl(41, null, reference, LogService.LOG_INFO, message, null, null);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + Constants.SYSTEM_BUNDLE_SYMBOLICNAME
        + LogEntryImpl.LM_FIELD_SEPARATOR + message));

    reference.props.clear();

    /* exception */

    String st = "<invalid>";
    try (StringWriter sw = new StringWriter(); //
        PrintWriter pw = new PrintWriter(sw)) {
      exception.printStackTrace(pw);
      st = sw.toString();
    }
    catch (final IOException e) {
      /* swallow */
    }
    bundle.bsn = "bsn1";
    impl = new LogEntryImpl(41, bundle, null, LogService.LOG_INFO, message, exception, null);
    r = impl.toLogMessage();
    final String exp = lvl + LogEntryImpl.LM_FIELD_SEPARATOR + "[" + "bsn1" + "]" + LogEntryImpl.LM_FIELD_SEPARATOR
        + message + " (" + exception + ")" + LogEntryImpl.LM_STACKTRACE_PREFIX + st;
    assertThat(r, equalTo(exp));
    bundle.bsn = null;

    /* event */

    final BundleEvent be = new BundleEvent(BundleEvent.INSTALLED, bundle);
    final FrameworkEvent fe = new FrameworkEvent(FrameworkEvent.STARTLEVEL_CHANGED, bundle, null);

    impl = new LogEntryImpl(41, null, null, LogService.LOG_INFO, null, null, fe);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl));

    impl = new LogEntryImpl(41, null, null, LogService.LOG_INFO, "message", null, be);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + "message"));

    impl = new LogEntryImpl(41, null, null, LogService.LOG_INFO, "message", null, fe);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + "message"));

    final MockFrameworkStartLevel fsl = new MockFrameworkStartLevel();
    fsl.startLevel = 42;
    bundle.adaptReturn = fsl;

    impl = new LogEntryImpl(41, null, null, LogService.LOG_INFO, "message", null, fe);
    r = impl.toLogMessage();
    assertThat(r, equalTo(lvl + LogEntryImpl.LM_FIELD_SEPARATOR + "message to 42"));
  }

  @SuppressWarnings("rawtypes")
  @Test(timeout = 8000)
  public void testToEventAdminEvent() {
    final MockServiceReference<?> reference = new MockServiceReference<>();
    final MockBundle bundle = new MockBundle();
    final MockBundle bundle2 = new MockBundle();
    final String message = "message";
    final Exception exception = new Exception("ex");

    bundle.bsn = "bsn1";
    bundle2.bsn = "bsn2";
    reference.bundle = bundle;

    LogEntryImpl impl;
    Event r;
    Object prop;

    /*
     * 'empty'
     */

    impl = new LogEntryImpl(41, null, null, LogService.LOG_INFO, null, null, null);
    r = impl.toEventAdminEvent();
    assertThat(r, notNullValue());
    assertThat(r.getTopic(), equalTo(LogEntryImpl.EA_TOPIC_PREFIX + "LOG_INFO"));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_BUNDLE_ID)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_BUNDLE_BSN)), equalTo(Boolean.FALSE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_BUNDLE)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_LOG_LEVEL)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_MESSAGE)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_TIMESTAMP)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_LOG_ENTRY)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_EXCEPTION_CLASS)), equalTo(Boolean.FALSE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_EXCEPTION_MESSAGE)), equalTo(Boolean.FALSE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_EXCEPTION)), equalTo(Boolean.FALSE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_SERVICE)), equalTo(Boolean.FALSE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_SERVICE_ID)), equalTo(Boolean.FALSE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_SERVICE_PID)), equalTo(Boolean.FALSE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_SERVICE_OBJECTCLASS)), equalTo(Boolean.FALSE));

    prop = r.getProperty(LogEntryImpl.EA_BUNDLE_ID);
    assertThat(prop, nullValue());

    prop = r.getProperty(LogEntryImpl.EA_BUNDLE);
    assertThat(prop, nullValue());

    prop = r.getProperty(LogEntryImpl.EA_LOG_LEVEL);
    assertThat(prop, notNullValue());
    assert (prop instanceof Integer);
    assertThat((Integer) prop, equalTo(Integer.valueOf(LogService.LOG_INFO)));

    prop = r.getProperty(LogEntryImpl.EA_MESSAGE);
    assertThat(prop, nullValue());

    prop = r.getProperty(LogEntryImpl.EA_TIMESTAMP);
    assertThat(prop, notNullValue());
    assert (prop instanceof Long);
    assertThat((Long) prop, equalTo(Long.valueOf(41)));

    prop = r.getProperty(LogEntryImpl.EA_LOG_ENTRY);
    assertThat(prop, notNullValue());
    assert (prop instanceof LogEntryImpl);
    assertThat((LogEntryImpl) prop, equalTo(impl));

    /*
     * 'full'
     */

    reference.props.put(Constants.SERVICE_ID, Long.valueOf(12));
    reference.props.put(Constants.SERVICE_PID, "pid");
    final String[] clazzes = {
        "a",
        "b",
        "c"
    };
    reference.props.put(Constants.OBJECTCLASS, clazzes);

    impl = new LogEntryImpl(42, bundle2, reference, LogService.LOG_WARNING, message, exception, null);
    r = impl.toEventAdminEvent();
    assertThat(r, notNullValue());
    assertThat(r.getTopic(), equalTo(LogEntryImpl.EA_TOPIC_PREFIX + "LOG_WARNING"));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_BUNDLE_ID)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_BUNDLE_BSN)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_BUNDLE)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_LOG_LEVEL)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_MESSAGE)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_TIMESTAMP)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_LOG_ENTRY)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_EXCEPTION_CLASS)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_EXCEPTION_MESSAGE)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_EXCEPTION)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_SERVICE)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_SERVICE_ID)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_SERVICE_PID)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_SERVICE_OBJECTCLASS)), equalTo(Boolean.TRUE));

    prop = r.getProperty(LogEntryImpl.EA_BUNDLE_ID);
    assertThat(prop, notNullValue());
    assert (prop instanceof Long);
    assertThat((Long) prop, equalTo(Long.valueOf(-1)));

    prop = r.getProperty(LogEntryImpl.EA_BUNDLE_BSN);
    assertThat(prop, notNullValue());
    assert (prop instanceof String);
    assertThat((String) prop, equalTo("bsn2"));

    prop = r.getProperty(LogEntryImpl.EA_BUNDLE);
    assertThat(prop, notNullValue());
    assert (prop instanceof Bundle);
    assertThat((Bundle) prop, equalTo((Bundle) bundle2));

    prop = r.getProperty(LogEntryImpl.EA_LOG_LEVEL);
    assertThat(prop, notNullValue());
    assert (prop instanceof Integer);
    assertThat((Integer) prop, equalTo(Integer.valueOf(LogService.LOG_WARNING)));

    prop = r.getProperty(LogEntryImpl.EA_MESSAGE);
    assertThat(prop, notNullValue());
    assert (prop instanceof String);
    assertThat((String) prop, equalTo(message));

    prop = r.getProperty(LogEntryImpl.EA_TIMESTAMP);
    assertThat(prop, notNullValue());
    assert (prop instanceof Long);
    assertThat((Long) prop, equalTo(Long.valueOf(42)));

    prop = r.getProperty(LogEntryImpl.EA_LOG_ENTRY);
    assertThat(prop, notNullValue());
    assert (prop instanceof LogEntryImpl);
    assertThat((LogEntryImpl) prop, equalTo(impl));

    prop = r.getProperty(LogEntryImpl.EA_EXCEPTION_CLASS);
    assertThat(prop, notNullValue());
    assert (prop instanceof String);
    assertThat((String) prop, equalTo(exception.getClass().getName()));

    prop = r.getProperty(LogEntryImpl.EA_EXCEPTION_MESSAGE);
    assertThat(prop, notNullValue());
    assert (prop instanceof String);
    assertThat((String) prop, equalTo("ex"));

    prop = r.getProperty(LogEntryImpl.EA_EXCEPTION);
    assertThat(prop, notNullValue());
    assert (prop instanceof Throwable);
    assertThat((Throwable) prop, equalTo((Throwable) exception));

    prop = r.getProperty(LogEntryImpl.EA_SERVICE);
    assertThat(prop, notNullValue());
    assert (prop instanceof ServiceReference<?>);
    assertThat((ServiceReference) prop, equalTo(((ServiceReference) reference)));

    prop = r.getProperty(LogEntryImpl.EA_SERVICE_ID);
    assertThat(prop, notNullValue());
    assert (prop instanceof Long);
    assertThat((Long) prop, equalTo(Long.valueOf(12)));

    prop = r.getProperty(LogEntryImpl.EA_SERVICE_PID);
    assertThat(prop, notNullValue());
    assert (prop instanceof String);
    assertThat((String) prop, equalTo("pid"));

    prop = r.getProperty(LogEntryImpl.EA_SERVICE_OBJECTCLASS);
    assertThat(prop, notNullValue());
    assert (prop instanceof String[]);
    assertThat((String[]) prop, equalTo(clazzes));

    /*
     * 'full' but no service pid
     */

    bundle.bundleId = 13;
    reference.props.remove(Constants.SERVICE_PID);

    impl = new LogEntryImpl(43, bundle, reference, LogService.LOG_ERROR, message, exception, null);
    r = impl.toEventAdminEvent();
    assertThat(r, notNullValue());
    assertThat(r.getTopic(), equalTo(LogEntryImpl.EA_TOPIC_PREFIX + "LOG_ERROR"));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_BUNDLE_ID)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_BUNDLE_BSN)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_BUNDLE)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_LOG_LEVEL)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_MESSAGE)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_TIMESTAMP)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_LOG_ENTRY)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_EXCEPTION_CLASS)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_EXCEPTION_MESSAGE)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_EXCEPTION)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_SERVICE)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_SERVICE_ID)), equalTo(Boolean.TRUE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_SERVICE_PID)), equalTo(Boolean.FALSE));
    assertThat(Boolean.valueOf(r.containsProperty(LogEntryImpl.EA_SERVICE_OBJECTCLASS)), equalTo(Boolean.TRUE));

    prop = r.getProperty(LogEntryImpl.EA_BUNDLE_ID);
    assertThat(prop, notNullValue());
    assert (prop instanceof Long);
    assertThat((Long) prop, equalTo(Long.valueOf(13)));

    prop = r.getProperty(LogEntryImpl.EA_BUNDLE_BSN);
    assertThat(prop, notNullValue());
    assert (prop instanceof String);
    assertThat((String) prop, equalTo("bsn1"));

    prop = r.getProperty(LogEntryImpl.EA_BUNDLE);
    assertThat(prop, notNullValue());
    assert (prop instanceof Bundle);
    assertThat((Bundle) prop, equalTo((Bundle) bundle));

    prop = r.getProperty(LogEntryImpl.EA_LOG_LEVEL);
    assertThat(prop, notNullValue());
    assert (prop instanceof Integer);
    assertThat((Integer) prop, equalTo(Integer.valueOf(LogService.LOG_ERROR)));

    prop = r.getProperty(LogEntryImpl.EA_MESSAGE);
    assertThat(prop, notNullValue());
    assert (prop instanceof String);
    assertThat((String) prop, equalTo(message));

    prop = r.getProperty(LogEntryImpl.EA_TIMESTAMP);
    assertThat(prop, notNullValue());
    assert (prop instanceof Long);
    assertThat((Long) prop, equalTo(Long.valueOf(43)));

    prop = r.getProperty(LogEntryImpl.EA_LOG_ENTRY);
    assertThat(prop, notNullValue());
    assert (prop instanceof LogEntryImpl);
    assertThat((LogEntryImpl) prop, equalTo(impl));

    prop = r.getProperty(LogEntryImpl.EA_EXCEPTION_CLASS);
    assertThat(prop, notNullValue());
    assert (prop instanceof String);
    assertThat((String) prop, equalTo(exception.getClass().getName()));

    prop = r.getProperty(LogEntryImpl.EA_EXCEPTION_MESSAGE);
    assertThat(prop, notNullValue());
    assert (prop instanceof String);
    assertThat((String) prop, equalTo("ex"));

    prop = r.getProperty(LogEntryImpl.EA_EXCEPTION);
    assertThat(prop, notNullValue());
    assert (prop instanceof Throwable);
    assertThat((Throwable) prop, equalTo((Throwable) exception));

    prop = r.getProperty(LogEntryImpl.EA_SERVICE);
    assertThat(prop, notNullValue());
    assert (prop instanceof ServiceReference<?>);
    assertThat((ServiceReference) prop, equalTo(((ServiceReference) reference)));

    prop = r.getProperty(LogEntryImpl.EA_SERVICE_ID);
    assertThat(prop, notNullValue());
    assert (prop instanceof Long);
    assertThat((Long) prop, equalTo(Long.valueOf(12)));

    prop = r.getProperty(LogEntryImpl.EA_SERVICE_OBJECTCLASS);
    assertThat(prop, notNullValue());
    assert (prop instanceof String[]);
    assertThat((String[]) prop, equalTo(clazzes));
  }
}