package nl.pelagic.mac.ids;

import nl.pelagic.mac.ids.api.VendorMACPrefix;

class VendorMACPrefixImpl implements VendorMACPrefix {
  private String vendor = null;
  private long   prefix = -1;

  VendorMACPrefixImpl(final String vendor, final long prefix) {
    this.prefix = prefix;
    this.vendor = vendor;
  }

  @Override
  public long getPrefix() {
    return this.prefix;
  }

  @Override
  public String getVendor() {
    return this.vendor;
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("VendorMAC [prefix=");
    builder.append(String.format("%06x", Long.valueOf(this.prefix)));
    builder.append(", vendor=");
    builder.append(this.vendor);
    builder.append("]");
    return builder.toString();
  }
}