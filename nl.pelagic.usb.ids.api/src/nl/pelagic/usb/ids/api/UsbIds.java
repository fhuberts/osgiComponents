package nl.pelagic.usb.ids.api;

import org.osgi.annotation.versioning.ProviderType;

/**
 * USB ID database service interface
 */
@ProviderType
public interface UsbIds {
  /**
   * Get a {@link UsbId} of a USB device.
   *
   * @param vendorId the vendor id of the USB device
   * @param productId the product id of the USB device
   * @return null when the vendor is not found, or when the product is not found for that vendor null. A {@link UsbId}
   *         otherwise.
   */
  UsbId getUsbId(int vendorId, int productId);
}