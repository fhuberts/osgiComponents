package nl.pelagic.service.log.syslog;

import java.util.Dictionary;

import org.junit.Ignore;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

@SuppressWarnings("javadoc")
@Ignore
public class MockServiceRegistration<S> implements ServiceRegistration<S> {

  ServiceReference<S> reference = null;

  @Override
  public ServiceReference<S> getReference() {
    return this.reference;
  }

  @Override
  public void setProperties(final Dictionary<String, ?> properties) {
    throw new IllegalStateException();
  }

  @Override
  public void unregister() {
    this.reference = null;
  }
}