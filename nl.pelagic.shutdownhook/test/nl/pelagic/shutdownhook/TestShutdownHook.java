package nl.pelagic.shutdownhook;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("javadoc")
public class TestShutdownHook {

  private ShutdownHook                 shutdownHook                  = null;
  private DummyShutdownHookParticipant dummyShutdownHookParticipant1 = null;
  private DummyShutdownHookParticipant dummyShutdownHookParticipant2 = null;

  @Before
  public void setUp() {
    this.shutdownHook = new ShutdownHook();
    this.dummyShutdownHookParticipant1 = new DummyShutdownHookParticipant();
    this.dummyShutdownHookParticipant2 = new DummyShutdownHookParticipant();
  }

  @After
  public void tearDown() {
    this.dummyShutdownHookParticipant1 = null;
    this.dummyShutdownHookParticipant2 = null;
    this.shutdownHook = null;
  }

  @Test(timeout = 8000)
  public void testActivateDeactive() {
    this.shutdownHook.addShutdownHookParticipant(this.dummyShutdownHookParticipant1);

    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant1.callCount), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant2.callCount), equalTo(Integer.valueOf(0)));

    this.shutdownHook.activate();

    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant1.callCount), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant2.callCount), equalTo(Integer.valueOf(0)));

    this.shutdownHook.run();

    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant1.callCount), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant2.callCount), equalTo(Integer.valueOf(0)));

    this.shutdownHook.deactivate();

    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant1.callCount), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant2.callCount), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testNoParticipants() {
    this.shutdownHook.run();

    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant1.callCount), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant2.callCount), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testOneParticipant() {
    this.shutdownHook.addShutdownHookParticipant(this.dummyShutdownHookParticipant1);

    this.shutdownHook.run();

    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant1.callCount), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant2.callCount), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testOneParticipantThrows() {
    this.shutdownHook.addShutdownHookParticipant(this.dummyShutdownHookParticipant1);
    this.dummyShutdownHookParticipant1.throwException = true;

    this.shutdownHook.run();

    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant1.callCount), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant2.callCount), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testOneParticipantRemoved() {
    this.shutdownHook.addShutdownHookParticipant(this.dummyShutdownHookParticipant1);
    this.shutdownHook.removeShutdownHookParticipant(this.dummyShutdownHookParticipant1);

    this.shutdownHook.run();

    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant1.callCount), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant2.callCount), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testTwoParticipants() {
    this.shutdownHook.addShutdownHookParticipant(this.dummyShutdownHookParticipant1);
    this.shutdownHook.addShutdownHookParticipant(this.dummyShutdownHookParticipant2);

    this.shutdownHook.run();

    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant1.callCount), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant2.callCount), equalTo(Integer.valueOf(1)));
  }

  @Test(timeout = 8000)
  public void testTwoParticipantsOneRemoved() {
    this.shutdownHook.addShutdownHookParticipant(this.dummyShutdownHookParticipant1);
    this.shutdownHook.addShutdownHookParticipant(this.dummyShutdownHookParticipant2);
    this.shutdownHook.removeShutdownHookParticipant(this.dummyShutdownHookParticipant1);

    this.shutdownHook.run();

    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant1.callCount), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(this.dummyShutdownHookParticipant2.callCount), equalTo(Integer.valueOf(1)));
  }

  @Test(timeout = 8000)
  public void testRemoveNotAllowed() {
    final SecurityManager smOrg = System.getSecurityManager();
    final MySecurityManager sm = new MySecurityManager(smOrg);
    System.setSecurityManager(sm);

    try {
      this.shutdownHook.addShutdownHookParticipant(this.dummyShutdownHookParticipant1);

      assertThat(Integer.valueOf(this.dummyShutdownHookParticipant1.callCount), equalTo(Integer.valueOf(0)));
      assertThat(Integer.valueOf(this.dummyShutdownHookParticipant2.callCount), equalTo(Integer.valueOf(0)));

      this.shutdownHook.activate();

      assertThat(Integer.valueOf(this.dummyShutdownHookParticipant1.callCount), equalTo(Integer.valueOf(0)));
      assertThat(Integer.valueOf(this.dummyShutdownHookParticipant2.callCount), equalTo(Integer.valueOf(0)));

      this.shutdownHook.run();

      assertThat(Integer.valueOf(this.dummyShutdownHookParticipant1.callCount), equalTo(Integer.valueOf(1)));
      assertThat(Integer.valueOf(this.dummyShutdownHookParticipant2.callCount), equalTo(Integer.valueOf(0)));

      sm.deny = true;

      this.shutdownHook.deactivate();

      assertThat(Integer.valueOf(this.dummyShutdownHookParticipant1.callCount), equalTo(Integer.valueOf(1)));
      assertThat(Integer.valueOf(this.dummyShutdownHookParticipant2.callCount), equalTo(Integer.valueOf(0)));
    }
    finally {
      System.setSecurityManager(smOrg);
      this.shutdownHook.deactivate();
    }
  }
}