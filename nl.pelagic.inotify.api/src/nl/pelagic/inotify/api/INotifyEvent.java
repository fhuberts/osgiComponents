package nl.pelagic.inotify.api;

import org.osgi.annotation.versioning.ProviderType;

/**
 * <p>
 * An INotify event, as sent to {@link INotifyClient}s.
 * </p>
 * <p>
 * This is the 'struct inotify_event' as specified in <code>'/usr/include/linux/inotify.h'.</code>.
 * </p>
 */
@ProviderType
public class INotifyEvent {
  /** The watch descriptor */
  private final int    wd;

  /** The name that caused the event (never null) */
  private final String name;

  /** The watch mask */
  private final long   mask;

  /** A unique cookie to synchronise two events */
  private final long   cookie;

  /**
   * Constructor
   *
   * @param name The name that caused the event
   * @param wd The watch descriptor
   * @param mask The watch mask
   * @param cookie A unique cookie to synchronise two events
   */
  public INotifyEvent(final String name, final int wd, final long mask, final long cookie) {
    this.name = (name == null) ? "" : name;
    this.wd = wd;
    this.mask = mask;
    this.cookie = ((mask & INotifyMask.IN_MOVE) != 0) ? cookie : 0;
  }

  /**
   * @return The watch descriptor
   */
  public int getWd() {
    return this.wd;
  }

  /**
   * @return The name that caused the event (never null)
   */
  public String getName() {
    return this.name;
  }

  /**
   * @return The watch mask
   */
  public long getMask() {
    return this.mask;
  }

  /**
   * @return A unique cookie to synchronise two events
   */
  public long getCookie() {
    return this.cookie;
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("WatchEvent [wd=");
    builder.append(this.wd);
    builder.append(", name=");
    builder.append(this.name);
    builder.append(", mask=");
    builder.append(INotifyMask.maskToString(this.mask));
    builder.append(", cookie=");
    builder.append(this.cookie);
    builder.append("]");
    return builder.toString();
  }
}