package nl.pelagic.mac.ids;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

import org.junit.Test;

@SuppressWarnings({
    "static-method",
    "javadoc"
})
public class TestVendorMACPrefixImpl {
  @Test(timeout = 8000)
  public void testVendorMAC() {
    final VendorMACPrefixImpl r = new VendorMACPrefixImpl("vendor", 0x112233L);
    assertThat(r, notNullValue());

    assertThat(Long.valueOf(r.getPrefix()), equalTo(Long.valueOf(0x112233L)));
    assertThat(r.getVendor(), equalTo("vendor"));
    assertThat(r.toString(), equalTo("VendorMAC [prefix=112233, vendor=vendor]"));
  }
}