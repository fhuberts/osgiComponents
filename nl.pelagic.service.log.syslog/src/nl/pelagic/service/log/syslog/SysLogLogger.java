package nl.pelagic.service.log.syslog;

import java.util.EventObject;

import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;

interface SysLogLogger {
  void log(final Bundle bundle, final ServiceReference<?> sr, final int level, final String message,
      final Throwable exception, final EventObject event);
}