package nl.pelagic.time.skew.restarter;

import java.util.concurrent.atomic.AtomicBoolean;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.log.LogService;
import org.osgi.service.metatype.annotations.Designate;

/**
 * <p>
 * This component will initiate an OSGi framework update (which effectively is an application restart) when the time
 * jumps more than a configured amount.
 * </p>
 * <p>
 * Depending on the used launcher, an OSGi framework update requires extra support. For example: the bnd launcher does
 * not natively supports a framework restart and just exits with exit code 252, thereby delegating the application
 * restart.
 * </p>
 * <p>
 * The Java libraries can't handle time jumps, thereby making this component needed on certain platforms that do not
 * have a real-time clock.
 * </p>
 * <p>
 * An example of affected classes is the Timer class: it doesn't use monotonic clocks even when the underlying platform
 * does support them.
 * </p>
 */
@Component(immediate = true, service = {})
@Designate(ocd = Config.class)
public class TimeSkewRestarter extends Thread {
  /*
   * Services
   */

  private LogService logger = null;

  @Reference
  void setLogger(final LogService logger) {
    this.logger = logger;
  }

  /*
   * Lifecycle
   */

  @Activate
  void activate(final BundleContext ctx, final Config config) {
    this.setName(this.getClass().getSimpleName());
    this.setDaemon(true);

    this.run.set(true);
    synchronized (this.configLock) {
      this.ctx = ctx;
      this.setupConfig(config);
    }

    this.start();
  }

  @Deactivate
  void deactivate() {
    if (this.run.get()) {
      this.run.set(false);
      this.interrupt();
    }
  }

  /*
   * Config
   */

  boolean       testMode   = false;

  Object        configLock = new Object();
  BundleContext ctx        = null;
  Config        config     = null;

  /* MUST be called in a synchronized (configLock) block */
  Config setupConfig(final Config config) {
    final Config preConfig = this.config;
    this.config = config;
    return preConfig;
  }

  @Modified
  void modified(final Config config) {
    Config preConfig;
    synchronized (this.configLock) {
      preConfig = this.setupConfig(config);
    }

    if ((preConfig.checkIntervalMilliSeconds() != config.checkIntervalMilliSeconds()) //
        || (preConfig.maximumTimeSkewMilliSeconds() != config.maximumTimeSkewMilliSeconds())) {
      this.interrupt();
    }
  }

  /*
   * Runnable
   */

  AtomicBoolean run = new AtomicBoolean(true);

  @Override
  public void run() {
    while (this.run.get()) {
      long checkIntervalMilliSeconds;
      long maximumTimeSkewMilliSeconds;
      synchronized (this.configLock) {
        checkIntervalMilliSeconds = this.config.checkIntervalMilliSeconds();
        maximumTimeSkewMilliSeconds = this.config.maximumTimeSkewMilliSeconds();
      }

      final long startTime = System.currentTimeMillis();
      try {
        Thread.sleep(checkIntervalMilliSeconds);
      }
      catch (final InterruptedException e) {
        if (!this.run.get()) {
          continue;
        }
      }
      final long endTime = System.currentTimeMillis();

      synchronized (this.configLock) {
        if ((checkIntervalMilliSeconds != this.config.checkIntervalMilliSeconds()) //
            || (maximumTimeSkewMilliSeconds != this.config.maximumTimeSkewMilliSeconds())) {
          continue;
        }
      }

      final long timeSkew = Math.abs(endTime - checkIntervalMilliSeconds - startTime);
      if (timeSkew >= maximumTimeSkewMilliSeconds) {
        this.logger.log(LogService.LOG_ERROR,
            String.format("Time jumped at least %d milliseconds, going to restart the framework",
                Long.valueOf(maximumTimeSkewMilliSeconds)));

        Bundle frameworkBundle = null;
        synchronized (this.configLock) {
          frameworkBundle = this.ctx.getBundle(0);
        }

        try {
          frameworkBundle.update();
        }
        catch (final BundleException e) {
          /* swallow */
        }

        break;
      }
    }
  }
}