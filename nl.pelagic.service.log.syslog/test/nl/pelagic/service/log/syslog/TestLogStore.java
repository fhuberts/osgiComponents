package nl.pelagic.service.log.syslog;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.fail;

import java.util.Enumeration;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogService;

@SuppressWarnings("javadoc")
public class TestLogStore {
  LogStore impl;

  @Before
  public void setUp() {
    this.impl = new LogStore(SysLog.ENV_LOG_READER_DEPTH_DEFAULT);

  }

  @After
  public void tearDown() {
    this.impl = null;
  }

  @Test(timeout = 8000)
  public void testInitial() {
    assertThat(Integer.valueOf(this.impl.depth), equalTo(Integer.valueOf(SysLog.ENV_LOG_READER_DEPTH_DEFAULT)));
    assertThat(this.impl.logs, notNullValue());
    assertThat(Integer.valueOf(this.impl.logs.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testSetMaxSize() {
    this.impl.setDepth(-1);
    assertThat(Integer.valueOf(this.impl.depth), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(this.impl.logs.size()), equalTo(Integer.valueOf(0)));

    this.impl.setDepth(0);
    assertThat(Integer.valueOf(this.impl.depth), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(this.impl.logs.size()), equalTo(Integer.valueOf(0)));

    this.impl.setDepth(3);
    assertThat(Integer.valueOf(this.impl.depth), equalTo(Integer.valueOf(3)));
    assertThat(Integer.valueOf(this.impl.logs.size()), equalTo(Integer.valueOf(0)));

    final LogEntry le1 = new LogEntryImpl(1, null, null, LogService.LOG_ERROR, "line 1", null, null);
    this.impl.add(le1);
    assertThat(Integer.valueOf(this.impl.logs.size()), equalTo(Integer.valueOf(1)));
    assertThat(this.impl.logs.get(0), equalTo(le1));

    final LogEntry le2 = new LogEntryImpl(2, null, null, LogService.LOG_ERROR, "line 2", null, null);
    this.impl.add(le2);
    assertThat(Integer.valueOf(this.impl.logs.size()), equalTo(Integer.valueOf(2)));
    assertThat(this.impl.logs.get(0), equalTo(le2));

    final LogEntry le3 = new LogEntryImpl(3, null, null, LogService.LOG_ERROR, "line 3", null, null);
    this.impl.add(le3);
    assertThat(Integer.valueOf(this.impl.logs.size()), equalTo(Integer.valueOf(3)));
    assertThat(this.impl.logs.get(0), equalTo(le3));

    final LogEntry le4 = new LogEntryImpl(4, null, null, LogService.LOG_ERROR, "line 4", null, null);
    this.impl.add(le4);
    assertThat(Integer.valueOf(this.impl.logs.size()), equalTo(Integer.valueOf(3)));
    assertThat(this.impl.logs.get(0), equalTo(le4));

    final Enumeration<LogEntry> en = this.impl.getLogs();
    int index = 0;
    while (en.hasMoreElements()) {
      final LogEntry logEntry = en.nextElement();
      switch (index) {
        case 0:
          assertThat(logEntry, equalTo(le4));
          break;

        case 1:
          assertThat(logEntry, equalTo(le3));
          break;

        case 2:
          assertThat(logEntry, equalTo(le2));
          break;

        default:
          fail();
          break;
      }

      index++;
    }
    assertThat(Integer.valueOf(index), equalTo(Integer.valueOf(3)));

    this.impl.setDepth(2);
    assertThat(Integer.valueOf(this.impl.logs.size()), equalTo(Integer.valueOf(2)));

    this.impl.setDepth(-10);
    assertThat(Integer.valueOf(this.impl.logs.size()), equalTo(Integer.valueOf(0)));

    final LogEntry le5 = new LogEntryImpl(5, null, null, LogService.LOG_ERROR, "line 5", null, null);
    this.impl.add(le5);
    assertThat(Integer.valueOf(this.impl.logs.size()), equalTo(Integer.valueOf(0)));
  }
}