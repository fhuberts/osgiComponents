package nl.pelagic.service.log.syslog;

import java.util.LinkedList;
import java.util.List;

import org.junit.Ignore;
import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogListener;

@SuppressWarnings("javadoc")
@Ignore
public class MockLogListener implements LogListener {
  List<LogEntry> logs     = new LinkedList<>();

  long           logSleep = 0;

  @Override
  public void logged(final LogEntry entry) {
    this.logs.add(entry);
    if (this.logSleep > 0) {
      try {
        Thread.sleep(this.logSleep);
      }
      catch (final InterruptedException e) {
        /* swallow */
      }
    }
  }
}