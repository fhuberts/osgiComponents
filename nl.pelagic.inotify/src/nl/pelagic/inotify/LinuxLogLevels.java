package nl.pelagic.inotify;

import org.osgi.service.log.LogService;

/**
 * <p>
 * Linux syslog constants
 * </p>
 * <p>
 * Copied from '/usr/include/sys/syslog.h'.
 * </p>
 */
public class LinuxLogLevels {
  /** system is unusable */
  public static final int LOG_EMERG   = 0;

  /** action must be taken immediately */
  public static final int LOG_ALERT   = 1;

  /** critical conditions */
  public static final int LOG_CRIT    = 2;

  /** error conditions */
  public static final int LOG_ERR     = 3;

  /** warning conditions */
  public static final int LOG_WARNING = 4;

  /** normal but significant condition */
  public static final int LOG_NOTICE  = 5;

  /** informational */
  public static final int LOG_INFO    = 6;

  /** debug-level messages */
  public static final int LOG_DEBUG   = 7;

  /**
   * Convert a Linux log priority to an OSGi log priority.
   *
   * Unknown log levels will be mapped to {@link LogService#LOG_DEBUG}.
   *
   * @param level The Linux log priority
   * @return level The OSGi log priority
   */
  static int getLogServiceLevel(final int level) {
    switch (level) {
      case LinuxLogLevels.LOG_EMERG:
      case LinuxLogLevels.LOG_ALERT:
      case LinuxLogLevels.LOG_CRIT:
      case LinuxLogLevels.LOG_ERR:
        return LogService.LOG_ERROR;

      case LinuxLogLevels.LOG_WARNING:
        return LogService.LOG_WARNING;

      case LinuxLogLevels.LOG_NOTICE:
      case LinuxLogLevels.LOG_INFO:
        return LogService.LOG_INFO;

      case LinuxLogLevels.LOG_DEBUG:
      default:
        return LogService.LOG_DEBUG;
    }
  }

  /**
   * Convert an OSGi log priorityto a Linux log priority
   *
   * Unknown log levels will be mapped to {@link #LOG_DEBUG}.
   *
   * @param level The OSGi log priority
   * @return level The Linux log priority
   */
  static int getLinuxLogLevel(final int level) {
    switch (level) {
      case LogService.LOG_ERROR:
        return LOG_ERR;

      case LogService.LOG_WARNING:
        return LOG_WARNING;

      case LogService.LOG_INFO:
        return LOG_INFO;

      case LogService.LOG_DEBUG:
      default:
        return LOG_DEBUG;
    }
  }
}