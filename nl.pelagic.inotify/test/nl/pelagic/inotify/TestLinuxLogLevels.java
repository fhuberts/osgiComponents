package nl.pelagic.inotify;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

import org.junit.Test;
import org.osgi.service.log.LogService;

@SuppressWarnings({
    "static-method", "javadoc"
})
public class TestLinuxLogLevels {

  @Test(timeout = 8000)
  public void testGetLogServiceLevel() {
    final LinuxLogLevels lll = new LinuxLogLevels();
    assertThat(lll, notNullValue());

    final int actual[] = {
        LinuxLogLevels.LOG_EMERG,
        LinuxLogLevels.LOG_ALERT,
        LinuxLogLevels.LOG_CRIT,
        LinuxLogLevels.LOG_ERR,
        LinuxLogLevels.LOG_WARNING,
        LinuxLogLevels.LOG_NOTICE,
        LinuxLogLevels.LOG_INFO,
        LinuxLogLevels.LOG_DEBUG,
        //
        -1,
        99999,
        Integer.MIN_VALUE,
        Integer.MAX_VALUE
    };

    final int expected[] = {
        LogService.LOG_ERROR,
        LogService.LOG_ERROR,
        LogService.LOG_ERROR,
        LogService.LOG_ERROR,
        LogService.LOG_WARNING,
        LogService.LOG_INFO,
        LogService.LOG_INFO,
        LogService.LOG_DEBUG,
        //
        LogService.LOG_DEBUG,
        LogService.LOG_DEBUG,
        LogService.LOG_DEBUG,
        LogService.LOG_DEBUG
    };

    for (int i = 0; i < actual.length; i++) {
      assertThat(Integer.valueOf(LinuxLogLevels.getLogServiceLevel(actual[i])), equalTo(Integer.valueOf(expected[i])));
    }
  }

  @Test(timeout = 8000)
  public void testGetLinuxLogLevel() {

    final int actual[] = {
        LogService.LOG_ERROR,
        LogService.LOG_WARNING,
        LogService.LOG_INFO,
        LogService.LOG_DEBUG,
        //
        0,
        99999,
        Integer.MIN_VALUE,
        Integer.MAX_VALUE
    };

    final int expected[] = {
        LinuxLogLevels.LOG_ERR,
        LinuxLogLevels.LOG_WARNING,
        LinuxLogLevels.LOG_INFO,
        LinuxLogLevels.LOG_DEBUG,
        //
        LinuxLogLevels.LOG_DEBUG,
        LinuxLogLevels.LOG_DEBUG,
        LinuxLogLevels.LOG_DEBUG,
        LinuxLogLevels.LOG_DEBUG
    };

    for (int i = 0; i < actual.length; i++) {
      assertThat("Index " + i, Integer.valueOf(LinuxLogLevels.getLinuxLogLevel(actual[i])),
          equalTo(Integer.valueOf(expected[i])));
    }
  }
}