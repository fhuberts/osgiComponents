package nl.pelagic.service.log.syslog;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

import org.junit.Test;

@SuppressWarnings({
    "static-method",
    "javadoc"
})
public class TestLinuxLogConstants {
  @Test(timeout = 8000)
  public void testInstance() {
    assertThat(new LinuxLogConstants().toString(), notNullValue());
  }
}