package nl.pelagic.service.log.syslog;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

import nl.pelagic.service.log.syslog.LogServiceFactory.SysLogWrapper;

@SuppressWarnings({
    "rawtypes",
    "javadoc"
})
public class TestLogServiceFactory {
  MockBundle        bundle;
  MockSysLogLogger  logger;

  LogServiceFactory impl;

  @Before
  public void setUp() {
    this.bundle = new MockBundle();
    this.logger = new MockSysLogLogger();
    this.impl = new LogServiceFactory(this.logger);

    assertThat(this.impl.logger, equalTo((SysLogLogger) this.logger));
  }

  @After
  public void tearDown() {
    this.impl = null;
    this.logger = null;
    this.bundle = null;
  }

  @Test(timeout = 8000)
  public void testGetAndUngetService() {
    final LogService r = this.impl.getService(this.bundle, null);
    assertThat(r, notNullValue());
    assertThat(Boolean.valueOf(r instanceof SysLogWrapper), equalTo(Boolean.TRUE));
    assertThat(((SysLogWrapper) r).log, equalTo(this.impl.logger));
    assertThat(((SysLogWrapper) r).bundle, equalTo((Bundle) this.bundle));

    this.impl.ungetService(this.bundle, null, null);
    assertThat(((SysLogWrapper) r).log, equalTo(this.impl.logger));
    assertThat(((SysLogWrapper) r).bundle, equalTo((Bundle) this.bundle));

    final LogService loggermock = new LogService() {
      @Override
      public void log(final ServiceReference sr, final int level, final String message, final Throwable exception) {
        /* dummy */
      }

      @Override
      public void log(final ServiceReference sr, final int level, final String message) {
        /* dummy */
      }

      @Override
      public void log(final int level, final String message, final Throwable exception) {
        /* dummy */
      }

      @Override
      public void log(final int level, final String message) {
        /* dummy */
      }
    };

    this.impl.ungetService(this.bundle, null, loggermock);
    assertThat(((SysLogWrapper) r).log, equalTo(this.impl.logger));
    assertThat(((SysLogWrapper) r).bundle, equalTo((Bundle) this.bundle));

    this.impl.ungetService(this.bundle, null, r);
    assertThat(((SysLogWrapper) r).log, nullValue());
    assertThat(((SysLogWrapper) r).bundle, nullValue());
  }

  @Test(timeout = 8000)
  public void testLogging() {
    final LogService r = this.impl.getService(this.bundle, null);

    /* case 1 */

    r.log(1, "msg 1");
    assertThat(Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    LogEntryImpl entry = this.logger.logs.get(0);
    assertThat(entry.getBundle(), equalTo((Bundle) this.bundle));
    assertThat(entry.getServiceReference(), nullValue());
    assertThat(Integer.valueOf(entry.getLevel()), equalTo(Integer.valueOf(1)));
    assertThat(entry.getMessage(), equalTo("msg 1"));
    assertThat(entry.getException(), nullValue());
    this.logger.logs.clear();

    /* case 2 */

    final Throwable ex = new IOException("dummy");
    r.log(2, "msg 2", ex);
    assertThat(Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    entry = this.logger.logs.get(0);
    assertThat(entry.getBundle(), equalTo((Bundle) this.bundle));
    assertThat(entry.getServiceReference(), nullValue());
    assertThat(Integer.valueOf(entry.getLevel()), equalTo(Integer.valueOf(2)));
    assertThat(entry.getMessage(), equalTo("msg 2"));
    assertThat(entry.getException(), equalTo(ex));
    this.logger.logs.clear();

    /* case 3 */

    final MockServiceReference<Object> sr = new MockServiceReference<>();
    r.log(sr, 3, "msg 3");
    assertThat(Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    entry = this.logger.logs.get(0);
    assertThat(entry.getBundle(), equalTo((Bundle) this.bundle));
    assertThat(entry.getServiceReference(), equalTo((ServiceReference) sr));
    assertThat(Integer.valueOf(entry.getLevel()), equalTo(Integer.valueOf(3)));
    assertThat(entry.getMessage(), equalTo("msg 3"));
    assertThat(entry.getException(), nullValue());
    this.logger.logs.clear();

    /* case 4 */

    r.log(sr, 4, "msg 4", ex);
    assertThat(Integer.valueOf(this.logger.logs.size()), equalTo(Integer.valueOf(1)));
    entry = this.logger.logs.get(0);
    assertThat(entry.getBundle(), equalTo((Bundle) this.bundle));
    assertThat(entry.getServiceReference(), equalTo((ServiceReference) sr));
    assertThat(Integer.valueOf(entry.getLevel()), equalTo(Integer.valueOf(4)));
    assertThat(entry.getMessage(), equalTo("msg 4"));
    assertThat(entry.getException(), equalTo(ex));
    this.logger.logs.clear();

    this.impl.ungetService(this.bundle, null, r);
  }
}