package nl.pelagic.service.log.syslog;

import java.util.Locale;

import org.osgi.framework.BundleEvent;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.ServiceEvent;
import org.osgi.service.log.LogService;

class LogUtils {
  static int clipLevel(final int level) {
    return Math.min(Math.max(LogService.LOG_ERROR, level), LogService.LOG_DEBUG);
  }

  static int parseLevel(final Object level, final int dflt) {
    if (level == null) {
      return clipLevel(dflt);
    }

    if (level instanceof Number) {
      return clipLevel(((Number) level).intValue());
    }

    if (!(level instanceof String)) {
      return clipLevel(dflt);
    }

    String lvl = ((String) level).trim().toUpperCase(Locale.US);

    if (lvl.isEmpty()) {
      return clipLevel(dflt);
    }

    if (lvl.matches("\\d+")) {
      return clipLevel(readUintFromObject(lvl, clipLevel(dflt)));
    }

    if (lvl.startsWith("LOG_")) {
      lvl = lvl.substring(4);
    }

    if ("ERROR".compareTo(lvl) == 0) {
      return LogService.LOG_ERROR;
    }

    if ("WARNING".compareTo(lvl) == 0) {
      return LogService.LOG_WARNING;
    }

    if ("INFO".compareTo(lvl) == 0) {
      return LogService.LOG_INFO;
    }

    if ("DEBUG".compareTo(lvl) == 0) {
      return LogService.LOG_DEBUG;
    }

    return clipLevel(dflt);
  }

  static String levelToTopicLevel(final int level) {
    switch (level) {
      case LogService.LOG_ERROR:
        return "LOG_ERROR";

      case LogService.LOG_WARNING:
        return "LOG_WARNING";

      case LogService.LOG_INFO:
        return "LOG_INFO";

      case LogService.LOG_DEBUG:
        return "LOG_DEBUG";

      default:
        return "LOG_OTHER";
    }
  }

  static String levelToSyslogString(final int level) {
    switch (level) {
      case LogService.LOG_ERROR:
        return "*ERROR*";

      case LogService.LOG_WARNING:
        return "*WARN*";

      case LogService.LOG_INFO:
        return "*INFO*";

      case LogService.LOG_DEBUG:
        return "*DEBUG*";

      default:
        return "*" + level + "*";
    }
  }

  static String bundleEventToString(final BundleEvent event) {
    if (event == null) {
      /* beyond the spec */
      return "BundleEvent <null>";
    }

    switch (event.getType()) {
      case BundleEvent.INSTALLED:
        return "BundleEvent INSTALLED";

      case BundleEvent.STARTED:
        return "BundleEvent STARTED";

      case BundleEvent.STOPPED:
        return "BundleEvent STOPPED";

      case BundleEvent.UPDATED:
        return "BundleEvent UPDATED";

      case BundleEvent.UNINSTALLED:
        return "BundleEvent UNINSTALLED";

      case BundleEvent.RESOLVED:
        return "BundleEvent RESOLVED";

      case BundleEvent.UNRESOLVED:
        return "BundleEvent UNRESOLVED";

      /* beyond the spec */

      case BundleEvent.STARTING:
        return "BundleEvent STARTING";

      case BundleEvent.STOPPING:
        return "BundleEvent STOPPING";

      case BundleEvent.LAZY_ACTIVATION:
        return "BundleEvent LAZY_ACTIVATION";

      default:
        return "BundleEvent " + Integer.toString(event.getType());
    }
  }

  static String frameworkEventToString(final FrameworkEvent event) {
    if (event == null) {
      /* beyond the spec */
      return "FrameworkEvent <null>";
    }

    switch (event.getType()) {
      case FrameworkEvent.STARTED:
        return "FrameworkEvent STARTED";

      case FrameworkEvent.ERROR:
        return "FrameworkEvent ERROR";

      case FrameworkEvent.PACKAGES_REFRESHED:
        return "FrameworkEvent PACKAGES_REFRESHED";

      case FrameworkEvent.STARTLEVEL_CHANGED:
        return "FrameworkEvent STARTLEVEL_CHANGED";

      case FrameworkEvent.WARNING:
        return "FrameworkEvent WARNING";

      case FrameworkEvent.INFO:
        return "FrameworkEvent INFO";

      /* beyond the spec */

      case FrameworkEvent.STOPPED:
        return "FrameworkEvent STOPPED";

      case FrameworkEvent.STOPPED_UPDATE:
        return "FrameworkEvent STOPPED_UPDATE";

      case FrameworkEvent.STOPPED_BOOTCLASSPATH_MODIFIED:
        return "FrameworkEvent STOPPED_BOOTCLASSPATH_MODIFIED";

      case FrameworkEvent.WAIT_TIMEDOUT:
        return "FrameworkEvent WAIT_TIMEDOUT";

      default:
        return "FrameworkEvent " + Integer.toString(event.getType());
    }
  }

  static String serviceEventToString(final ServiceEvent event) {
    if (event == null) {
      /* beyond the spec */
      return "ServiceEvent <null>";
    }

    switch (event.getType()) {
      case ServiceEvent.REGISTERED:
        return "ServiceEvent REGISTERED";

      case ServiceEvent.MODIFIED:
        return "ServiceEvent MODIFIED";

      case ServiceEvent.UNREGISTERING:
        return "ServiceEvent UNREGISTERING";

      /* beyond the spec */

      case ServiceEvent.MODIFIED_ENDMATCH:
        return "ServiceEvent MODIFIED_ENDMATCH";

      default:
        return "ServiceEvent " + Integer.toString(event.getType());
    }
  }

  static int readUintFromObject(final Object value, final int dflt) {
    if (value == null) {
      return Math.max(0, dflt);
    }

    if (value instanceof Number) {
      return Math.max(0, ((Number) value).intValue());
    }

    if (value instanceof String) {
      try {
        return Math.max(0, Integer.parseInt(((String) value).trim()));
      }
      catch (final Exception e) {
        return Math.max(0, dflt);
      }
    }

    return Math.max(0, dflt);
  }

  /**
   * Convert an OSGi log priority to a native log priority.
   *
   * Unknown log levels will be mapped to {@link LogService#LOG_INFO}.
   *
   * @param osgiPriority The OSGi log priority
   * @return The native log priority
   * @throws IllegalArgumentException When the current platform/OS is not supported
   */
  static int osgiPriorityToOSPriority(final int osgiPriority) throws IllegalArgumentException {
    switch (osgiPriority) {
      case LogService.LOG_ERROR:
        return LinuxLogConstants.LOG_ERR;

      case LogService.LOG_WARNING:
        return LinuxLogConstants.LOG_WARNING;

      case LogService.LOG_INFO:
        return LinuxLogConstants.LOG_INFO;

      case LogService.LOG_DEBUG:
      default:
        return LinuxLogConstants.LOG_DEBUG;
    }
  }
}