package nl.pelagic.service.log.syslog;

/**
 * Linux syslog constants
 *
 */
class LinuxLogConstants {
  /*
   * Copied from /usr/include/sys/syslog.h
   */

  /*
   * priorities/facilities are encoded into a single 32-bit quantity, where the bottom 3 bits are the priority (0-7) and
   * the top 28 bits are the facility (0-big number). Both the priorities and the facilities map roughly one-to-one to
   * strings in the syslogd(8) source code. This mapping is included in this file.
   */

  /*
   * Priorities (these are ordered)
   */

  /** system is unusable */
  public static final int LOG_EMERG    = 0;

  /** action must be taken immediately */
  public static final int LOG_ALERT    = 1;

  /** critical conditions */
  public static final int LOG_CRIT     = 2;

  /** error conditions */
  public static final int LOG_ERR      = 3;

  /** warning conditions */
  public static final int LOG_WARNING  = 4;

  /** normal but significant condition */
  public static final int LOG_NOTICE   = 5;

  /** informational */
  public static final int LOG_INFO     = 6;

  /** debug-level messages */
  public static final int LOG_DEBUG    = 7;

  /*
   * Facility codes
   */

  /** kernel messages */
  public static final int LOG_KERN     = (0 << 3);

  /** random user-level messages */
  public static final int LOG_USER     = (1 << 3);

  /** mail system */
  public static final int LOG_MAIL     = (2 << 3);

  /** system daemons */
  public static final int LOG_DAEMON   = (3 << 3);

  /** security/authorization messages */
  public static final int LOG_AUTH     = (4 << 3);

  /** messages generated internally by syslogd */
  public static final int LOG_SYSLOG   = (5 << 3);

  /** line printer subsystem */
  public static final int LOG_LPR      = (6 << 3);

  /** network news subsystem */
  public static final int LOG_NEWS     = (7 << 3);

  /** UUCP subsystem */
  public static final int LOG_UUCP     = (8 << 3);

  /** clock daemon */
  public static final int LOG_CRON     = (9 << 3);

  /** security/authorization messages (private) */
  public static final int LOG_AUTHPRIV = (10 << 3);

  /** ftp daemon */
  public static final int LOG_FTP      = (11 << 3);

  /*
   * Option flags for openlog. LOG_ODELAY no longer does anything. LOG_NDELAY is the inverse of what it used to be.
   */

  /** log the pid with each message */
  public static final int LOG_PID      = 0x01;

  /** log on the console if errors in sending */
  public static final int LOG_CONS     = 0x02;

  /** delay open until first syslog() (default) */
  public static final int LOG_ODELAY   = 0x04;

  /** don't delay open */
  public static final int LOG_NDELAY   = 0x08;

  /** don't wait for console forks: DEPRECATED */
  public static final int LOG_NOWAIT   = 0x10;

  /** log to stderr as well */
  public static final int LOG_PERROR   = 0x20;
}