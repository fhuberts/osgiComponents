package nl.pelagic.mac.ids;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

import org.junit.Test;

@SuppressWarnings({
    "static-method",
    "javadoc"
})
public class TestMACUtils {
  @Test(timeout = 8000)
  public void testInstance() {
    final MACUtils mu = new MACUtils();
    assertThat(mu, notNullValue());
  }

  @Test(timeout = 8000)
  public void testGetMacAddressNumber() {
    long r;

    r = MACUtils.getMacAddressNumber(null);
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(-1)));

    r = MACUtils.getMacAddressNumber("");
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(-1)));

    r = MACUtils.getMacAddressNumber("  ");
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(-1)));

    r = MACUtils.getMacAddressNumber("invalid");
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(-1)));

    r = MACUtils.getMacAddressNumber("11:22:33:44:55");
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(0x1122334455L)));

    r = MACUtils.getMacAddressNumber("11:22:33:44:55:66:77");
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(-1)));

    r = MACUtils.getMacAddressNumber("11:22:33:44:55:66");
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(0x112233445566L)));

    r = MACUtils.getMacAddressNumber("1:2:33:44:55:66");
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(0x010233445566L)));
  }

  @Test(timeout = 8000)
  public void testGetMacAddressPrefixNumber() {
    long r;

    r = MACUtils.getMacAddressPrefixNumber(null);
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(-1)));

    r = MACUtils.getMacAddressPrefixNumber("");
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(-1)));

    r = MACUtils.getMacAddressPrefixNumber("  ");
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(-1)));

    r = MACUtils.getMacAddressPrefixNumber("invalid");
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(-1)));

    r = MACUtils.getMacAddressPrefixNumber("11:22");
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(0x1122L)));

    r = MACUtils.getMacAddressPrefixNumber("11:22:33:44");
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(0x223344L)));

    r = MACUtils.getMacAddressPrefixNumber("11:22:33");
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(0x112233L)));

    r = MACUtils.getMacAddressPrefixNumber("11:2:33");
    assertThat(Long.valueOf(r), equalTo(Long.valueOf(0x110233L)));
  }

  @Test(timeout = 8000)
  public void testPrefixToString() {
    String r;

    r = MACUtils.prefixToString(-1);
    assertThat(r, equalTo("ff:ff:ff"));

    r = MACUtils.prefixToString(0);
    assertThat(r, equalTo("00:00:00"));

    r = MACUtils.prefixToString(0x213476L);
    assertThat(r, equalTo("21:34:76"));

    r = MACUtils.prefixToString(0xaaaa213476L);
    assertThat(r, equalTo("21:34:76"));
  }
}