package nl.pelagic.inotify.api;

import java.io.IOException;

import org.osgi.annotation.versioning.ProviderType;

/**
 * <p>
 * An INotify exception
 * </p>
 * <p>
 * This is just an IOException that also stores 'errno'.
 * </p>
 */
@ProviderType
public class INotifyException extends IOException {
  private static final long serialVersionUID = 4790439244049188564L;

  /** The error number */
  private int               errno            = Integer.MIN_VALUE;

  /**
   * Constructor.
   *
   */
  public INotifyException() {
    super();
  }

  /**
   * Constructor.
   *
   * @param s The message.
   * @param e The exception.
   */
  public INotifyException(final String s, final Throwable e) {
    super(s, e);
  }

  /**
   * Constructor.
   *
   * @param s The message.
   * @param errno The error number (see {@link INotifyLinuxErrno}).
   */
  public INotifyException(final String s, final int errno) {
    super(s);
    this.errno = errno;
  }

  /**
   * @return errno Integer.MIN_VALUE when not set, the error number otherwise (see {@link INotifyLinuxErrno}).
   */
  public int getErrno() {
    return this.errno;
  }

  @Override
  public String toString() {
    final String s = super.toString();

    if (this.errno == Integer.MIN_VALUE) {
      return s;
    }

    return s + String.format(", errno = %d", Integer.valueOf(this.errno));
  }
}