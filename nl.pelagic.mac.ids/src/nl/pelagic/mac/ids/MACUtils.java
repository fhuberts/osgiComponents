package nl.pelagic.mac.ids;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class MACUtils {
  static private final String  BYTE_REGEX        = "[0-9a-fA-F]{1,2}";
  static private final String  MAC_ADDRESS_REGEX = "^\\s*(" + BYTE_REGEX + "(\\s*:\\s*" + BYTE_REGEX + "){0,5})\\s*$";
  static private final Pattern macAddressPattern = Pattern.compile(MAC_ADDRESS_REGEX);

  /**
   * Convert a MAC address into a number
   *
   * @param address the MAC address
   * @return -1 when the MAC address is not valid
   */
  static public long getMacAddressNumber(final String address) {
    if ((address == null) //
        || address.isEmpty()) {
      return -1;
    }

    final Matcher m = macAddressPattern.matcher(address);
    if (!m.matches()) {
      return -1;
    }

    final String[] split = m.group(1).split("\\s*:\\s*");
    long r = 0;
    int i = 0;
    for (i = 0; i < split.length; i++) {
      r <<= 8;
      r += Long.parseUnsignedLong(split[i], 16);
    }

    return r;
  }

  /**
   * Convert a MAC address prefix into a number
   *
   * @param prefix the MAC address prefix
   * @return -1 when the MAC address prefix is not valid
   */
  static public long getMacAddressPrefixNumber(final String prefix) {
    final long r = getMacAddressNumber(prefix);
    if (r == -1) {
      return -1;
    }

    return (r & 0x0ffffffL);
  }

  /**
   * Convert a MAC address prefix into a string (like '11:22:33').
   *
   * @param prefix The MAC address prefix
   * @return The string representation of the MAC address prefix
   */
  static public String prefixToString(final long prefix) {
    final long b0 = (prefix & 0x0ff0000L) >> 16;
    final long b1 = (prefix & 0x000ff00L) >> 8;
    final long b2 = (prefix & 0x00000ffL) >> 0;
    return String.format("%02x:%02x:%02x", Long.valueOf(b0), Long.valueOf(b1), Long.valueOf(b2));
  }
}