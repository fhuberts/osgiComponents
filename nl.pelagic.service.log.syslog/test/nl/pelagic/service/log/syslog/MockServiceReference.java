package nl.pelagic.service.log.syslog;

import java.util.HashMap;
import java.util.Map;

import org.junit.Ignore;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;

@SuppressWarnings("javadoc")
@Ignore
public class MockServiceReference<S> implements ServiceReference<S> {

  Map<String, Object> props = new HashMap<>();

  @Override
  public Object getProperty(final String key) {
    return this.props.get(key);
  }

  @Override
  public String[] getPropertyKeys() {
    throw new IllegalStateException();
  }

  Bundle bundle = null;

  @Override
  public Bundle getBundle() {
    return this.bundle;
  }

  @Override
  public Bundle[] getUsingBundles() {
    throw new IllegalStateException();
  }

  @Override
  public boolean isAssignableTo(final Bundle bundle, final String className) {
    throw new IllegalStateException();
  }

  @Override
  public int compareTo(final Object reference) {
    throw new IllegalStateException();
  }
}