package nl.pelagic.usb.ids.api;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

import org.junit.Test;

@SuppressWarnings({
    "static-method",
    "javadoc"
})
public class TestUsbId {
  @Test(timeout = 8000)
  public void testUsbId() {
    final UsbId id = new UsbId(1, 2, "3", "4");

    assertThat(Integer.valueOf(id.getVendorId()), equalTo(Integer.valueOf(1)));
    assertThat(Integer.valueOf(id.getProductId()), equalTo(Integer.valueOf(2)));
    assertThat(id.getVendorDescription(), equalTo("3"));
    assertThat(id.getProductDescription(), equalTo("4"));

    assertThat(id.toString(), notNullValue());
  }

  @Test(timeout = 8000)
  public void testHashCode() {
    UsbId id;

    id = new UsbId(1, 2, null, null);
    assertThat(Integer.valueOf(id.hashCode()), equalTo(Integer.valueOf(955234)));

    id = new UsbId(1, 2, "3", "4");
    assertThat(Integer.valueOf(id.hashCode()), equalTo(Integer.valueOf(956867)));
  }

  @Test(timeout = 8000)
  public void testEquals() {
    UsbId id1 = new UsbId(1, 2, null, null);
    UsbId id2 = null;

    /* self */

    assertThat(Boolean.valueOf(id1.equals(id1)), equalTo(Boolean.TRUE));

    /* null */

    assertThat(Boolean.valueOf(id1.equals(null)), equalTo(Boolean.FALSE));

    /* wrong class */

    assertThat(Boolean.valueOf(id1.equals(new Object())), equalTo(Boolean.FALSE));

    /* vendorId */

    id1 = new UsbId(1, 2, "", null);
    id2 = new UsbId(2, 2, "", null);
    assertThat(Boolean.valueOf(id1.equals(id2)), equalTo(Boolean.FALSE));

    /* productId */

    id1 = new UsbId(1, 1, null, "");
    id2 = new UsbId(1, 2, null, "");
    assertThat(Boolean.valueOf(id1.equals(id2)), equalTo(Boolean.FALSE));

    /* vendorDescription */

    id1 = new UsbId(1, 2, null, null);
    id2 = new UsbId(1, 2, null, null);
    assertThat(Boolean.valueOf(id1.equals(id2)), equalTo(Boolean.TRUE));

    id1 = new UsbId(1, 2, null, null);
    id2 = new UsbId(1, 2, "", null);
    assertThat(Boolean.valueOf(id1.equals(id2)), equalTo(Boolean.FALSE));

    id1 = new UsbId(1, 2, "", null);
    id2 = new UsbId(1, 2, null, null);
    assertThat(Boolean.valueOf(id1.equals(id2)), equalTo(Boolean.FALSE));

    id1 = new UsbId(1, 2, "1", null);
    id2 = new UsbId(1, 2, "2", null);
    assertThat(Boolean.valueOf(id1.equals(id2)), equalTo(Boolean.FALSE));

    /* productDescription */

    id1 = new UsbId(1, 2, null, null);
    id2 = new UsbId(1, 2, null, null);
    assertThat(Boolean.valueOf(id1.equals(id2)), equalTo(Boolean.TRUE));

    id1 = new UsbId(1, 2, null, null);
    id2 = new UsbId(1, 2, null, "");
    assertThat(Boolean.valueOf(id1.equals(id2)), equalTo(Boolean.FALSE));

    id1 = new UsbId(1, 2, null, "");
    id2 = new UsbId(1, 2, null, null);
    assertThat(Boolean.valueOf(id1.equals(id2)), equalTo(Boolean.FALSE));

    id1 = new UsbId(1, 2, null, "1");
    id2 = new UsbId(1, 2, null, "2");
    assertThat(Boolean.valueOf(id1.equals(id2)), equalTo(Boolean.FALSE));

    /* equal */

    id1 = new UsbId(1, 2, "1", "2");
    id2 = new UsbId(1, 2, "1", "2");
    assertThat(Boolean.valueOf(id1.equals(id2)), equalTo(Boolean.TRUE));
  }
}