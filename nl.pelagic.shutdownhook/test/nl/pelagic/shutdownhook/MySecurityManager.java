package nl.pelagic.shutdownhook;

import java.security.Permission;

import org.junit.Ignore;

@SuppressWarnings("javadoc")
@Ignore
public class MySecurityManager extends SecurityManager {

  SecurityManager sm   = null;

  boolean         deny = false;

  public MySecurityManager(final SecurityManager sm) {
    super();
    this.sm = sm;
  }

  @Override
  public void checkPermission(final Permission perm) {
    if (this.deny) {
      if (!(perm instanceof RuntimePermission)) {
        return;
      }

      final RuntimePermission rtperm = (RuntimePermission) perm;

      if (rtperm.getName().equals("shutdownHooks")) {
        throw new SecurityException("TEST DENY");
      }
    }

    if (this.sm != null) {
      this.sm.checkPermission(perm);
    }
  }
}