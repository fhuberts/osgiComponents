package nl.pelagic.time.skew.restarter;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Time Skew Restarter", description = "Configuration for the Time Skew Restarter service")
@interface Config {
  /* 1 minute */
  static final long CHECK_INTERVAL_MSEC_DEFAULT = 60000L;
  /* 1 second */
  static final long CHECK_INTERVAL_MSEC_MIN     = 1000L;

  /* 10 minutes */
  static final long MAX_SKEW_MSEC_DEFAULT       = 600000L;
  /* 1 second */
  static final long MAX_SKEW_MSEC_MIN           = 1000L;

  @AttributeDefinition(min = "" + CHECK_INTERVAL_MSEC_MIN, required = false,
      description = "The interval (msec) on which to check for a time jump (default = " + CHECK_INTERVAL_MSEC_DEFAULT
          + ")")
  long checkIntervalMilliSeconds() default CHECK_INTERVAL_MSEC_DEFAULT;

  @AttributeDefinition(min = "" + MAX_SKEW_MSEC_MIN, required = false,
      description = "The maximum time skew (msec). Higher time skews will restart the framework (default = "
          + MAX_SKEW_MSEC_DEFAULT + ")")
  long maximumTimeSkewMilliSeconds() default MAX_SKEW_MSEC_DEFAULT;
}