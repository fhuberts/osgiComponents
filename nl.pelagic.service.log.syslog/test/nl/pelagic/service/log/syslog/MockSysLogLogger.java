package nl.pelagic.service.log.syslog;

import java.util.EventObject;
import java.util.LinkedList;
import java.util.List;

import org.junit.Ignore;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;

@Ignore
class MockSysLogLogger implements SysLogLogger {

  List<LogEntryImpl> logs = new LinkedList<>();

  @Override
  public void log(final Bundle bundle, final ServiceReference<?> sr, final int level, final String message,
      final Throwable exception, final EventObject event) {
    this.logs.add(new LogEntryImpl(System.currentTimeMillis(), bundle, sr, level, message, exception, event));
  }
}