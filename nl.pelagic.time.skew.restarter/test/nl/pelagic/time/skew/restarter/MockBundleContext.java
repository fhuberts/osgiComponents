package nl.pelagic.time.skew.restarter;

import java.io.File;
import java.io.InputStream;
import java.util.Collection;
import java.util.Dictionary;

import org.junit.Ignore;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkListener;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceObjects;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

@Ignore
@SuppressWarnings("javadoc")
public class MockBundleContext implements BundleContext {

  MockBundle bundle = null;

  public MockBundleContext(final MockBundle bundle) {
    super();
    assert (bundle != null);
    this.bundle = bundle;
  }

  @Override
  public String getProperty(final String key) {
    throw new NullPointerException();
  }

  @Override
  public Bundle getBundle() {
    throw new NullPointerException();
  }

  @Override
  public Bundle installBundle(final String location, final InputStream input) throws BundleException {
    throw new NullPointerException();
  }

  @Override
  public Bundle installBundle(final String location) throws BundleException {
    throw new NullPointerException();
  }

  @Override
  public Bundle getBundle(final long id) {
    return this.bundle;
  }

  @Override
  public Bundle[] getBundles() {
    throw new NullPointerException();
  }

  @Override
  public void addServiceListener(final ServiceListener listener, final String filter) throws InvalidSyntaxException {
    throw new NullPointerException();
  }

  @Override
  public void addServiceListener(final ServiceListener listener) {
    throw new NullPointerException();
  }

  @Override
  public void removeServiceListener(final ServiceListener listener) {
    throw new NullPointerException();
  }

  @Override
  public void addBundleListener(final BundleListener listener) {
    throw new NullPointerException();
  }

  @Override
  public void removeBundleListener(final BundleListener listener) {
    throw new NullPointerException();
  }

  @Override
  public void addFrameworkListener(final FrameworkListener listener) {
    throw new NullPointerException();
  }

  @Override
  public void removeFrameworkListener(final FrameworkListener listener) {
    throw new NullPointerException();
  }

  @Override
  public ServiceRegistration<?> registerService(final String[] clazzes, final Object service,
      final Dictionary<String, ?> properties) {
    throw new NullPointerException();
  }

  @Override
  public ServiceRegistration<?> registerService(final String clazz, final Object service,
      final Dictionary<String, ?> properties) {
    throw new NullPointerException();
  }

  @Override
  public <S> ServiceRegistration<S> registerService(final Class<S> clazz, final S service,
      final Dictionary<String, ?> properties) {
    throw new NullPointerException();
  }

  @Override
  public ServiceReference<?>[] getServiceReferences(final String clazz, final String filter)
      throws InvalidSyntaxException {
    throw new NullPointerException();
  }

  @Override
  public ServiceReference<?>[] getAllServiceReferences(final String clazz, final String filter)
      throws InvalidSyntaxException {
    throw new NullPointerException();
  }

  @Override
  public ServiceReference<?> getServiceReference(final String clazz) {
    throw new NullPointerException();
  }

  @Override
  public <S> ServiceReference<S> getServiceReference(final Class<S> clazz) {
    throw new NullPointerException();
  }

  @Override
  public <S> Collection<ServiceReference<S>> getServiceReferences(final Class<S> clazz, final String filter)
      throws InvalidSyntaxException {
    throw new NullPointerException();
  }

  @Override
  public <S> S getService(final ServiceReference<S> reference) {
    throw new NullPointerException();
  }

  @Override
  public boolean ungetService(final ServiceReference<?> reference) {
    throw new NullPointerException();
  }

  @Override
  public File getDataFile(final String filename) {
    throw new NullPointerException();
  }

  @Override
  public Filter createFilter(final String filter) throws InvalidSyntaxException {
    throw new NullPointerException();
  }

  @Override
  public Bundle getBundle(final String location) {
    throw new NullPointerException();
  }

  @Override
  public <S> ServiceRegistration<S> registerService(final Class<S> clazz, final ServiceFactory<S> factory,
      final Dictionary<String, ?> properties) {
    throw new NullPointerException();
  }

  @Override
  public <S> ServiceObjects<S> getServiceObjects(final ServiceReference<S> reference) {
    throw new NullPointerException();
  }
}