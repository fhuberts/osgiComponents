#include "nl_pelagic_inotify_INotifyImpl.h"

#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <syslog.h>
#include <unistd.h>

/*
 * See /usr/include/linux/inotify.h
 *
 * struct inotify_event {
 *   __s32   wd;      // watch descriptor
 *   __u32   mask;    // watch mask
 *   __u32   cookie;  // cookie to synchronize two events
 *   __u32   len;     // length (including nulls) of name
 *   char    name[0]; // stub for possible name
 * };
 */

/*
 * Constants
 */

#define DEACTIVATION_SIGNAL  SIGUSR1
#define EVENT_BUFFER_SIZE    (4096 * 4)

/*
 * Callbacks (this code calls these methods in the Java code)
 */

static const char * callbackLogMethodName = "callbackLog";
static const char * callbackLogMethodSignature = "(ILjava/lang/String;)V";
jmethodID callbackLogMethod = NULL;

static const char * callbackProcessEventMethodName = "callbackProcessEvent";
static const char * callbackProcessEventMethodSignature = "(Ljava/lang/String;IJJ)V";
jmethodID callbackProcessEventMethod = NULL;

static const char * callbackRunningMethodName = "callbackRunning";
static const char * callbackRunningMethodSignature = "(Z)V";
jmethodID callbackRunningMethod = NULL;

/*
 * State
 */

typedef enum {
  ACTIVATING, ACTIVATED, RUNNING, DEACTIVATING, DEACTIVATED
} State;

const char * const stateStrings[] = { "ACTIVATING", "ACTIVATED", "RUNNING", "DEACTIVATING", "DEACTIVATED" };

volatile State state = DEACTIVATED;

/*
 * Variables
 */

volatile int inotifyFD = -1;
volatile bool stopping = false;
volatile bool run = false;
volatile pthread_t threadIdSelf = 0;
char *eventBuffer = NULL;

/*
 * Forward declarations
 */

__attribute__ ((format(printf, 4, 5))) static void logging(JNIEnv *env, jobject this, int level, const char *format,
    ...);

/*
 * Tests
 */

#ifdef JUNIT_TESTS
#define CALLBACK_TEST_ACTIVATE     "activate"
#define CALLBACK_TEST_DEACTIVATE   "deactivate"
#define CALLBACK_TEST_EVENT_LOOP   "eventLoop"
#define CALLBACK_TEST_ADD_WATCH    "addWatch"
#define CALLBACK_TEST_REMOVE_WATCH "removeWatch"

static const char * callbackTestMethodName = "callbackTest";
static const char * callbackTestMethodSignature = "(Ljava/lang/String;)I";
jmethodID callbackTestMethod = NULL;

static int getTestAction(JNIEnv *env, jobject this, const char * function) {
  int action = 0;
  jstring name = (*env)->NewStringUTF(env, function);
  if (!name) {
    logging(env, this, LOG_ERR, "Could not allocate memory for the function of an test action; the action is ignored");
    action = 0;
    goto end;
  }

  action = (*env)->CallIntMethod(env, this, callbackTestMethod, name);

  if (name) {
    (*env)->DeleteLocalRef(env, name);
  }

  end:
  return action;
}
#endif

/*
 * Destructor
 */

__attribute__ ((destructor)) static void destroy(void) {
  if (inotifyFD != -1) {
    close(inotifyFD);
    inotifyFD = -1;
  }
}

/*
 * Logging
 */

__attribute__ ((format(printf, 4, 5))) static void logging(JNIEnv *env, jobject this, int level, const char *format,
    ...) {
  int sz = 0;
  char *msg = NULL;
  va_list ap;
  jstring msgj;

  if (!callbackLogMethod) {
    syslog: free(msg);
    msg = NULL;
    va_start(ap, format);
    vsyslog(level, format, ap);
    va_end(ap);
    return;
  }

  /* Determine required size */
  va_start(ap, format);
  sz = vsnprintf(NULL, 0, format, ap);
  va_end(ap);

  if (sz < 0) {
    goto syslog;
  }

  /* Allocate buffer */
  sz++; /* For '\0' */
  msg = malloc((size_t) sz);
  if (!msg) {
    goto syslog;
  }

  /* Format string */
  va_start(ap, format);
  sz = vsnprintf(msg, (size_t) sz, format, ap);
  va_end(ap);

  if (sz < 0) {
    goto syslog;
  }

  msgj = (*env)->NewStringUTF(env, msg);
  if (!msgj) {
    goto syslog;
  }

  (*env)->CallVoidMethod(env, this, callbackLogMethod, level, msgj);

  (*env)->DeleteLocalRef(env, msgj);

  free(msg);
}

/*
 * Signals
 */

/*
 * Empty callback function; used for the DEACTIVATION_SIGNAL signal which is only used to wake up the thread main loop.
 * This is really needed (in combination with closing inotifyFD), otherwise a blocking read invocation is not
 * interrupted properly.
 */
static void emptySignalHandler(__attribute__ ((unused)) int signal) {
  return;
}

/*
 * Install an empty signal handler for DEACTIVATION_SIGNAL and unblock the signal.
 *
 * @return -errno
 */
static int registerSignalHandler(JNIEnv *env, jobject this) {
  struct sigaction signalsAction;
  sigset_t signals;

  memset(&signalsAction, 0, sizeof(signalsAction));
  sigemptyset(&signals);

  signalsAction.sa_handler = &emptySignalHandler;
  sigemptyset(&signalsAction.sa_mask);
  signalsAction.sa_flags = SA_RESTART;

  if (sigaction(DEACTIVATION_SIGNAL, &signalsAction, NULL)) {
    int r = -errno;
    logging(env, this, LOG_ERR, "Could not register a signal handler for the %s signal: %s",
        strsignal(DEACTIVATION_SIGNAL), strerror(-r));
    return r;
  }

  if (sigaddset(&signals, DEACTIVATION_SIGNAL)) {
    int r = -errno;
    logging(env, this, LOG_ERR, "Could not add the %s signal to a signal set: %s", strsignal(DEACTIVATION_SIGNAL),
        strerror(-r));
    return r;
  }

  if (sigprocmask(SIG_UNBLOCK, &signals, NULL)) {
    int r = -errno;
    logging(env, this, LOG_ERR, "Could not unblock the %s signal: %s", strsignal(DEACTIVATION_SIGNAL), strerror(-r));
    return r;
  }

  return 0;
}

/*
 * JNI Utilities
 */

static bool findMethod(JNIEnv *env, jobject this, jclass clazz, volatile jmethodID * method, const char * const name,
    const char * const signature) {
  if (!*method) {
    *method = (*env)->GetMethodID(env, clazz, name, signature);
    if (!*method) {
      logging(env, this, LOG_ERR, "Method '%s' was not found", name);
      return false;
    }
  }

  return true;
}

/*
 * Lifecycle
 */

/*
 * Class:     nl_pelagic_inotify_INotifyImpl
 * Method:    activateNative
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_nl_pelagic_inotify_INotifyImpl_activateNative(JNIEnv *env, jobject this) {
  int r = 0;
  jclass clazz = (*env)->GetObjectClass(env, this);
  State expectedState;

  /*
   * Determine callbacks
   */

#ifdef JUNIT_TESTS
  int action = 0;

  if (!findMethod(env, this, clazz, &callbackTestMethod, callbackTestMethodName, callbackTestMethodSignature)) {
    r = -EDOM;
    goto err;
  }

  action = getTestAction(env, this, CALLBACK_TEST_ACTIVATE);
#endif

  if (!findMethod(env, this, clazz, &callbackLogMethod, callbackLogMethodName, callbackLogMethodSignature)
      || !findMethod(env, this, clazz, &callbackRunningMethod, callbackRunningMethodName,
          callbackRunningMethodSignature)
      || !findMethod(env, this, clazz, &callbackProcessEventMethod, callbackProcessEventMethodName,
          callbackProcessEventMethodSignature)
#ifdef JUNIT_TESTS
          || (action == 1)
#endif
          ) {
    r = -ENOENT;
    goto err;
  }

  /*
   * State check
   */

  if (state == ACTIVATED) {
    return 0;
  }

  expectedState = DEACTIVATED;
  if (!__atomic_compare_exchange_n(&state, &expectedState, ACTIVATING, false, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST)
#ifdef JUNIT_TESTS
      || (action == 2)
#endif
      ) {
    logging(env, this, LOG_ERR, "Can only activate when in the DEACTIVATED state, not when in the %s state",
        stateStrings[expectedState]);
    r = -EPERM;
    goto err;
  }

  /*
   * Initialise
   */

#ifdef JUNIT_TESTS
  if (action == 3) {
    eventBuffer = NULL;
  } else
#endif
  eventBuffer = malloc(EVENT_BUFFER_SIZE);
  if (!eventBuffer) {
    logging(env, this, LOG_ERR, "Could not allocate memory for the event buffer");
    r = -ENOMEM;
    goto err;
  }

  /*
   Prepare the event loop for running
   */

  stopping = false;
  run = true;
  threadIdSelf = 0;

  state = ACTIVATED;
  return 0;

  err: free(eventBuffer);
  eventBuffer = NULL;
  callbackRunningMethod = NULL;
  callbackProcessEventMethod = NULL;
  callbackLogMethod = NULL;

  state = DEACTIVATED;
  return r;
}

/*
 * Class:     nl_pelagic_inotify_INotifyImpl
 * Method:    deactivateNative
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_nl_pelagic_inotify_INotifyImpl_deactivateNative(__attribute__ ((unused)) JNIEnv *env,
    __attribute__ ((unused)) jobject this) {
  State expectedState;

#ifdef JUNIT_TESTS
  int action = getTestAction(env, this, CALLBACK_TEST_DEACTIVATE);
#endif

  /*
   * State check
   */

  if ((state == DEACTIVATED)
#ifdef JUNIT_TESTS
  && (action != 1)
#endif
  ) {
    return 0;
  }

  expectedState = RUNNING;
  if (!__atomic_compare_exchange_n(&state, &expectedState, DEACTIVATING, false, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST)
#ifdef JUNIT_TESTS
      || (action == 1)
#endif
      ) {
    expectedState = ACTIVATED;
    if (!__atomic_compare_exchange_n(&state, &expectedState, DEACTIVATING, false, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST)
#ifdef JUNIT_TESTS
        || (action == 1)
#endif
        ) {
      logging(env, this, LOG_ERR,
          "Can only deactivate when in the ACTIVATED or RUNNING states, not when in the %s state",
          stateStrings[expectedState]);
      return -EPERM;
    }
  }

  /*
   * Stop the event loop (if it's running)
   */

  stopping = true;
  run = false;

  destroy();

  if (threadIdSelf) {
    pthread_kill(threadIdSelf, DEACTIVATION_SIGNAL);

    while (stopping || run) {
      pthread_yield();
    }
  }

  /*
   * Uninitialise
   */

  free(eventBuffer);
  eventBuffer = NULL;

  /*
   * Clear callbacks
   */

  callbackRunningMethod = NULL;
  callbackProcessEventMethod = NULL;
  callbackLogMethod = NULL;

  state = DEACTIVATED;
  return 0;
}

/*
 * Class:     nl_pelagic_inotify_INotifyImpl
 * Method:    inotifyLoopNative
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_nl_pelagic_inotify_INotifyImpl_inotifyLoopNative(JNIEnv *env, jobject this) {
  int r = 0;
  ssize_t readCount = 0;
  ssize_t currentByte = 0;
  State expectedState;

#ifdef JUNIT_TESTS
  int action = getTestAction(env, this, CALLBACK_TEST_EVENT_LOOP);
#endif

  /*
   * State check
   */

  expectedState = ACTIVATED;
  if (!__atomic_compare_exchange_n(&state, &expectedState, RUNNING, false, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST)
#ifdef JUNIT_TESTS
      || (action == 1)
#endif
      ) {
    logging(env, this, LOG_ERR, "Can only run the event loop when in the ACTIVATED state, not when in the %s state",
        stateStrings[expectedState]);
    r = -EPERM;
    goto end;
  }

  /*
   * Run the event loop
   */

#ifdef JUNIT_TESTS
  if (action == 2) {
    r = -EINVAL;
  } else
#endif
  r = registerSignalHandler(env, this);
  if (r) {
    goto end;
  }

#ifdef JUNIT_TESTS
  if (action == 3) {
    inotifyFD = -1;
    errno = EMFILE;
  } else
#endif
  inotifyFD = inotify_init();
  if (inotifyFD == -1) {
    r = -errno;
    logging(env, this, LOG_ERR, "Failed to initialise inotify: %s", strerror(-r));
    goto end;
  }

#ifdef JUNIT_TESTS
  usleep(100 * 1000);
#endif

  threadIdSelf = pthread_self();

  (*env)->CallVoidMethod(env, this, callbackRunningMethod, JNI_TRUE);

  while (run && !stopping) {
    currentByte = 0;
    while (currentByte < readCount) {
      jstring name;
      struct inotify_event *event = (struct inotify_event *) &eventBuffer[currentByte];
      currentByte += (ssize_t) sizeof(struct inotify_event) + (ssize_t) event->len;

      if (event->len) {
        name = (*env)->NewStringUTF(env, event->name);
        if (!name) {
          logging(env, this, LOG_ERR, "Could not allocate memory for the filename of an event; the event is ignored");
          continue;
        }
      } else {
        name = NULL;
      }

      (*env)->CallVoidMethod(env, this, callbackProcessEventMethod, name, event->wd, event->mask, event->cookie);

      if (name) {
        (*env)->DeleteLocalRef(env, name);
      }
    }

    readCount = read(inotifyFD, eventBuffer, EVENT_BUFFER_SIZE);

    if (((inotifyFD == -1) || !run || stopping) || !readCount) {
      /* deactivate || EOF */
      break;
    }

    if (readCount == -1) {
      logging(env, this, LOG_ERR, "Failed to read from the inotify queue (ignored): %s", strerror(errno));
      readCount = 0;
    }
  }

  r = 0;

  end: destroy();

  (*env)->CallVoidMethod(env, this, callbackRunningMethod, JNI_FALSE);

  threadIdSelf = 0;

  run = false;
  stopping = false;

  expectedState = RUNNING;
  __atomic_compare_exchange_n(&state, &expectedState, ACTIVATED, false, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST);

  return r;
}

/*
 * Watches
 */

/*
 * Class:     nl_pelagic_inotify_INotifyImpl
 * Method:    addWatchNative
 * Signature: (Ljava/lang/String;J)I
 */
JNIEXPORT jint JNICALL Java_nl_pelagic_inotify_INotifyImpl_addWatchNative(JNIEnv *env, jobject this, jstring path,
    jlong mask) {
  int wd;
  const char * pathName;
  State expectedState;

#ifdef JUNIT_TESTS
  int action = getTestAction(env, this, CALLBACK_TEST_ADD_WATCH);
#endif

  /*
   * State check
   */

  expectedState = RUNNING;
  if (!__atomic_compare_exchange_n(&state, &expectedState, RUNNING, false, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST)
#ifdef JUNIT_TESTS
      || (action == 1)
#endif
      ) {
    logging(env, this, LOG_ERR, "Can only add a watch when in the RUNNING state, not when in the %s state",
        stateStrings[expectedState]);
    return -EPERM;
  }

  /*
   * Add the watch
   */

#ifdef JUNIT_TESTS
  if (action == 2) {
    pathName = NULL;
  } else
#endif
  pathName = (*env)->GetStringUTFChars(env, path, NULL);
  if (!pathName) {
    logging(env, this, LOG_ERR, "Could not allocate memory for the path to add a watch on");
    return -ENOMEM;
  }

  wd = inotify_add_watch(inotifyFD, pathName, (uint32_t) mask);
  if (wd == -1) {
    wd = -errno;
    logging(env, this, LOG_ERR, "Failed to add a watch on '%s': %s", pathName, strerror(-wd));
  }

  (*env)->ReleaseStringUTFChars(env, path, pathName);

  return wd;
}

/*
 * Class:     nl_pelagic_inotify_INotifyImpl
 * Method:    removeWatchNative
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_nl_pelagic_inotify_INotifyImpl_removeWatchNative(JNIEnv *env, jobject this, jint wd) {
  int r;
  State expectedState;

#ifdef JUNIT_TESTS
  int action = getTestAction(env, this, CALLBACK_TEST_REMOVE_WATCH);
#endif

  /*
   * State check
   */

  expectedState = RUNNING;
  if (!__atomic_compare_exchange_n(&state, &expectedState, RUNNING, false, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST)
#ifdef JUNIT_TESTS
      || (action == 1)
#endif
      ) {
    logging(env, this, LOG_ERR, "Can only remove a watch when in the RUNNING state, not when in the %s state",
        stateStrings[expectedState]);
    return -EPERM;
  }

  /*
   * Remove the watch
   */

  r = inotify_rm_watch(inotifyFD, wd);
  if (r == -1) {
    r = -errno;
    logging(env, this, LOG_ERR, "Failed to remove watch %d: %s", wd, strerror(-r));
  }

#ifdef JUNIT_TESTS
  usleep(100 * 1000);
#endif

  return r;
}

/*
 * Utilities
 */

/*
 * Class:     nl_pelagic_inotify_INotifyImpl
 * Method:    getStrErrorNative
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_nl_pelagic_inotify_INotifyImpl_getStrErrorNative(JNIEnv *env,
    __attribute__ ((unused)) jobject this, jint errNo) {
  return (*env)->NewStringUTF(env, strerror(errNo));
}
