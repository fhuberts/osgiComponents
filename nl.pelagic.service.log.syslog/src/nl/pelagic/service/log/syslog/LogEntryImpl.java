package nl.pelagic.service.log.syslog;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;

import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.startlevel.FrameworkStartLevel;
import org.osgi.service.component.ComponentConstants;
import org.osgi.service.event.Event;
import org.osgi.service.log.LogEntry;

class LogEntryImpl implements LogEntry {
  final long                timestamp;
  final ServiceReference<?> serviceReference;
  final Bundle              bundle;
  final int                 level;
  final String              message;
  final Throwable           exception;
  final EventObject         event;

  /*
   * Constructor
   */

  LogEntryImpl(final long time, final Bundle bundle, final ServiceReference<?> serviceReference, final int level,
      final String message, final Throwable exception, final EventObject event) {
    this.timestamp = time;
    this.serviceReference = serviceReference;
    if ((bundle == null) //
        && (serviceReference != null)) {
      this.bundle = this.serviceReference.getBundle();
    } else {
      this.bundle = bundle;
    }
    this.level = LogUtils.clipLevel(level);
    this.message = message;
    this.exception = exception;
    this.event = event;
  }

  /*
   * Getters
   */

  @Override
  public long getTime() {
    return this.timestamp;
  }

  @Override
  public ServiceReference getServiceReference() {
    return this.serviceReference;
  }

  @Override
  public Bundle getBundle() {
    return this.bundle;
  }

  @Override
  public int getLevel() {
    return this.level;
  }

  @Override
  public String getMessage() {
    return this.message;
  }

  @Override
  public Throwable getException() {
    return this.exception;
  }

  /*
   * Helpers
   */

  static final char LM_FIELD_SEPARATOR   = ' ';
  static final char LM_STACKTRACE_PREFIX = '\n';

  public String toLogMessage() {
    final StringBuilder sb = new StringBuilder();

    sb.append(LogUtils.levelToSyslogString(this.level));

    final boolean canGetBundle = (this.serviceReference != null) //
        || (this.bundle != null);

    if (canGetBundle) {
      Bundle bundle = this.bundle;
      if (bundle == null) {
        bundle = this.serviceReference.getBundle();
      }

      if (bundle == null) {
        /* no bundle: use system bundle name */
        sb.append(LM_FIELD_SEPARATOR);
        sb.append(Constants.SYSTEM_BUNDLE_SYMBOLICNAME);
      } else {
        /* use the bundle symbolic name */
        String bsn = bundle.getSymbolicName();

        if (bsn == null) {
          /* fall-back to location */
          bsn = bundle.getLocation();
        }

        if (bsn == null) {
          /* fall-back to bundle id */
          bsn = Long.toString(bundle.getBundleId());
        }

        sb.append(LM_FIELD_SEPARATOR);
        sb.append("[");
        sb.append(bsn);
        sb.append("]");
      }

      boolean comma = true;

      if ((this.serviceReference != null) //
          && (this.event != null) //
          && (this.event instanceof ServiceEvent)) {
        sb.append(LM_FIELD_SEPARATOR);
        sb.append("Service [");

        final Object pid = this.serviceReference.getProperty(Constants.SERVICE_PID);
        if (pid != null) {
          sb.append(pid);
        } else {
          final Object componentName = this.serviceReference.getProperty(ComponentConstants.COMPONENT_NAME);
          if (componentName != null) {
            sb.append(componentName);
          } else {
            final Object serviceDescription = this.serviceReference.getProperty(Constants.SERVICE_DESCRIPTION);
            if (serviceDescription != null) {
              sb.append(serviceDescription);
            } else {
              comma = false;
            }
          }
        }

        final Object serviceId = this.serviceReference.getProperty(Constants.SERVICE_ID);
        if (serviceId != null) {
          if (comma) {
            sb.append(", ");
          }
          sb.append(serviceId);
          comma = true;
        }

        final String[] objs = (String[]) this.serviceReference.getProperty(Constants.OBJECTCLASS);
        if (objs != null) {
          if (comma) {
            sb.append(", ");
          }
          sb.append(Arrays.toString(objs));
          comma = true;
        }

        sb.append(']');
      }
    }

    if (this.message != null) {
      sb.append(LM_FIELD_SEPARATOR);
      sb.append(this.message);

      if ((this.event != null) //
          && (this.event instanceof FrameworkEvent)) {
        final FrameworkStartLevel sl = ((FrameworkEvent) this.event).getBundle().adapt(FrameworkStartLevel.class);
        if (sl != null) {
          sb.append(" to ");
          sb.append(sl.getStartLevel());
        }
      }
    }

    if (this.exception != null) {
      sb.append(" (");
      sb.append(this.exception.toString());
      sb.append(')');

      final StringWriter sw = new StringWriter(); //
      @SuppressWarnings("resource")
      final PrintWriter pw = new PrintWriter(sw);
      this.exception.printStackTrace(pw);
      sb.append(LM_STACKTRACE_PREFIX);
      sb.append(sw.toString());

      /* sw is closed by closing pw (besides, closing sw is a NOP, making the closing of pw a NOP as well) */
      pw.close();
    }

    return sb.toString();
  }

  static final String EA_TOPIC_PREFIX        = "org/osgi/service/log/LogEntry/";
  static final String EA_BUNDLE_ID           = "bundle.id";
  static final String EA_BUNDLE_BSN          = "bundle.symbolicName";
  static final String EA_BUNDLE              = "bundle";
  static final String EA_LOG_LEVEL           = "log.level";
  static final String EA_MESSAGE             = "message";
  static final String EA_TIMESTAMP           = "timestamp";
  static final String EA_LOG_ENTRY           = "log.entry";
  static final String EA_EXCEPTION_CLASS     = "exception.class";
  static final String EA_EXCEPTION_MESSAGE   = "exception.message";
  static final String EA_EXCEPTION           = "exception";
  static final String EA_SERVICE             = "service";
  static final String EA_SERVICE_ID          = "service.id";
  static final String EA_SERVICE_PID         = "service.pid";
  static final String EA_SERVICE_OBJECTCLASS = "service.objectClass";

  public Event toEventAdminEvent() {
    final String topic = EA_TOPIC_PREFIX + LogUtils.levelToTopicLevel(this.level);
    final Map<String, Object> props = new HashMap<>();

    props.put(EA_BUNDLE_ID, //
        (this.bundle == null) //
            ? null //
            : Long.valueOf(this.bundle.getBundleId()));
    final String bsn = (this.bundle == null) //
        ? null //
        : this.bundle.getSymbolicName();
    if (bsn != null) {
      props.put(EA_BUNDLE_BSN, bsn);
    }
    props.put(EA_BUNDLE, this.bundle);
    props.put(EA_LOG_LEVEL, Integer.valueOf(this.level));
    props.put(EA_MESSAGE, this.message);
    props.put(EA_TIMESTAMP, Long.valueOf(this.timestamp));
    props.put(EA_LOG_ENTRY, this);

    if (this.exception != null) {
      props.put(EA_EXCEPTION_CLASS, this.exception.getClass().getName());
      props.put(EA_EXCEPTION_MESSAGE, this.exception.getMessage());
      props.put(EA_EXCEPTION, this.exception);
    }

    if (this.serviceReference != null) {
      props.put(EA_SERVICE, this.serviceReference);
      props.put(EA_SERVICE_ID, this.serviceReference.getProperty(Constants.SERVICE_ID));
      final Object pid = this.serviceReference.getProperty(Constants.SERVICE_PID);
      if (pid != null) {
        props.put(EA_SERVICE_PID, pid);
      }
      props.put(EA_SERVICE_OBJECTCLASS, this.serviceReference.getProperty(Constants.OBJECTCLASS));
    }

    return new Event(topic, props);
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("LogEntryImpl [timestamp=");
    builder.append(this.timestamp);
    builder.append(", serviceReference=");
    builder.append(this.serviceReference);
    builder.append(", bundle=");
    builder.append(this.bundle);
    builder.append(", level=");
    builder.append(this.level);
    builder.append(", message=");
    builder.append(this.message);
    builder.append(", exception=");
    builder.append(this.exception);
    builder.append(", event=");
    builder.append(this.event);
    builder.append("]");
    return builder.toString();
  }
}