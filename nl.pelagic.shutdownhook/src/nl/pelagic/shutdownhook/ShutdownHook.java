package nl.pelagic.shutdownhook;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

import nl.pelagic.shutdownhook.api.ShutdownHookParticipant;

/**
 * <p>
 * This component registers itself as a VM shutdown hook handler: when the VM shuts down then the handler in this bundle
 * is invoked, which again calls all {@link ShutdownHookParticipant}s, so that they can stop running in a controlled
 * fashion.
 * </p>
 * <p>
 * The component will install signal handlers for the HUP, INT and TERM signals. When either of these signals is
 * received, all {@link ShutdownHookParticipant} s are invoked.
 * </p>
 */
@Component(immediate = true)
public class ShutdownHook extends Thread {
  /** All shutdown hook participants */
  private final Set<ShutdownHookParticipant> shutdownHookParticipants = new CopyOnWriteArraySet<>();

  /**
   * @param shutdownHookParticipant The shutdown hook participant to add
   */
  @Reference(cardinality = ReferenceCardinality.AT_LEAST_ONE)
  void addShutdownHookParticipant(final ShutdownHookParticipant shutdownHookParticipant) {
    this.shutdownHookParticipants.add(shutdownHookParticipant);
  }

  /**
   * @param shutdownHookParticipant The shutdown hook participant to remove
   */
  void removeShutdownHookParticipant(final ShutdownHookParticipant shutdownHookParticipant) {
    this.shutdownHookParticipants.remove(shutdownHookParticipant);
  }

  /** The stop handler for the HUP signal */
  private final StopHandler stopHandler_HUP  = new StopHandler("HUP", this);

  /** The stop handler for the INT signal */
  private final StopHandler stopHandler_INT  = new StopHandler("INT", this);

  /** The stop handler for the TERM signal */
  private final StopHandler stopHandler_TERM = new StopHandler("TERM", this);

  /**
   * Bundle activator
   */
  @Activate
  public void activate() {
    this.setName(this.getClass().getSimpleName());
    this.setDaemon(true);

    Runtime.getRuntime().addShutdownHook(this);
    this.stopHandler_HUP.activate();
    this.stopHandler_INT.activate();
    this.stopHandler_TERM.activate();
  }

  /**
   * Bundle deactivator
   */
  @Deactivate
  public void deactivate() {
    this.stopHandler_TERM.deactivate();
    this.stopHandler_INT.deactivate();
    this.stopHandler_HUP.deactivate();
    try {
      Runtime.getRuntime().removeShutdownHook(this);
    }
    catch (final Throwable e) {
      /* swallow */
    }
  }

  /**
   * Invoke all shutdown hook participants
   */
  void stopAllShutdownHookParticipants() {
    for (final ShutdownHookParticipant shutdownHookParticipant : this.shutdownHookParticipants) {
      try {
        shutdownHookParticipant.shutdownHook();
      }
      catch (final Throwable e) {
        /* swallow */
      }
    }
  }

  @Override
  public void run() {
    this.stopAllShutdownHookParticipants();
  }
}