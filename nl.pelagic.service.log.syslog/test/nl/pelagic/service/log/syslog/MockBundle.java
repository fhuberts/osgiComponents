package nl.pelagic.service.log.syslog;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Version;

@SuppressWarnings("javadoc")
@Ignore
public class MockBundle implements Bundle {

  @Override
  public int compareTo(final Bundle o) {
    throw new IllegalStateException();
  }

  @Override
  public int getState() {
    throw new IllegalStateException();
  }

  @Override
  public void start(final int options) throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public void start() throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public void stop(final int options) throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public void stop() throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public void update(final InputStream input) throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public void update() throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public void uninstall() throws BundleException {
    throw new IllegalStateException();
  }

  @Override
  public Dictionary<String, String> getHeaders() {
    throw new IllegalStateException();
  }

  long bundleId = -1;

  @Override
  public long getBundleId() {
    return this.bundleId;
  }

  String location = null;

  @Override
  public String getLocation() {
    return this.location;
  }

  @Override
  public ServiceReference<?>[] getRegisteredServices() {
    throw new IllegalStateException();
  }

  @Override
  public ServiceReference<?>[] getServicesInUse() {
    throw new IllegalStateException();
  }

  @Override
  public boolean hasPermission(final Object permission) {
    throw new IllegalStateException();
  }

  @Override
  public URL getResource(final String name) {
    throw new IllegalStateException();
  }

  @Override
  public Dictionary<String, String> getHeaders(final String locale) {
    throw new IllegalStateException();
  }

  String bsn = null;

  @Override
  public String getSymbolicName() {
    return this.bsn;
  }

  @Override
  public Class<?> loadClass(final String name) throws ClassNotFoundException {
    throw new IllegalStateException();
  }

  @Override
  public Enumeration<URL> getResources(final String name) throws IOException {
    throw new IllegalStateException();
  }

  @Override
  public Enumeration<String> getEntryPaths(final String path) {
    throw new IllegalStateException();
  }

  @Override
  public URL getEntry(final String path) {
    throw new IllegalStateException();
  }

  @Override
  public long getLastModified() {
    throw new IllegalStateException();
  }

  @Override
  public Enumeration<URL> findEntries(final String path, final String filePattern, final boolean recurse) {
    throw new IllegalStateException();
  }

  @Override
  public BundleContext getBundleContext() {
    throw new IllegalStateException();
  }

  @Override
  public Map<X509Certificate, List<X509Certificate>> getSignerCertificates(final int signersType) {
    throw new IllegalStateException();
  }

  @Override
  public Version getVersion() {
    throw new IllegalStateException();
  }

  Object adaptReturn = null;

  @SuppressWarnings("unchecked")
  @Override
  public <A> A adapt(final Class<A> type) {
    return (A) this.adaptReturn;
  }

  @Override
  public File getDataFile(final String filename) {
    throw new IllegalStateException();
  }
}