package nl.pelagic.service.log.syslog;

import java.util.Arrays;
import java.util.Dictionary;

import org.junit.Ignore;

@SuppressWarnings("javadoc")
@Ignore
public class SVCRegistration {
  public String[]              clazzes;
  public Object                service;
  public Dictionary<String, ?> properties;

  public SVCRegistration(final String[] clazzes, final Object service, final Dictionary<String, ?> properties) {
    super();
    this.clazzes = clazzes;
    this.service = service;
    this.properties = properties;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = (prime * result) + Arrays.hashCode(this.clazzes);
    result = (prime * result) + ((this.properties == null) ? 0 : this.properties.hashCode());
    result = (prime * result) + ((this.service == null) ? 0 : this.service.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    final SVCRegistration other = (SVCRegistration) obj;
    if (!Arrays.equals(this.clazzes, other.clazzes)) {
      return false;
    }
    if (this.properties == null) {
      if (other.properties != null) {
        return false;
      }
    } else if (!this.properties.equals(other.properties)) {
      return false;
    }
    if (this.service == null) {
      if (other.service != null) {
        return false;
      }
    } else if (!this.service.equals(other.service)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("SVCRegistration [clazzes=");
    builder.append(Arrays.toString(this.clazzes));
    builder.append(", service=");
    builder.append(this.service);
    builder.append(", properties=");
    builder.append(this.properties);
    builder.append("]");
    return builder.toString();
  }
}