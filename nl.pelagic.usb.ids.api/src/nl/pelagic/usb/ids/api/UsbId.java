package nl.pelagic.usb.ids.api;

import org.osgi.annotation.versioning.ProviderType;

/**
 * A USB ID; stores vendor and product information (IDs and descriptions).
 */
@ProviderType
public class UsbId {
  private final int    vendorId;
  private final int    productId;
  private final String vendorDescription;
  private final String productDescription;

  /**
   * Constructor
   *
   * @param vendorId the vendor ID
   * @param productId the product ID
   * @param vendorDescription the vendor description
   * @param productDescription the product description
   */
  public UsbId(final int vendorId, final int productId, final String vendorDescription,
      final String productDescription) {
    super();
    this.vendorId = vendorId;
    this.productId = productId;
    this.vendorDescription = vendorDescription;
    this.productDescription = productDescription;
  }

  /**
   * @return the vendorId
   */
  public int getVendorId() {
    return this.vendorId;
  }

  /**
   * @return the productId
   */
  public int getProductId() {
    return this.productId;
  }

  /**
   * @return the vendorDescription
   */
  public String getVendorDescription() {
    return this.vendorDescription;
  }

  /**
   * @return the productDescription
   */
  public String getProductDescription() {
    return this.productDescription;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = (prime * result) + this.vendorId;
    result = (prime * result) + this.productId;
    result = (prime * result) + ((this.vendorDescription == null) ? 0 : this.vendorDescription.hashCode());
    result = (prime * result) + ((this.productDescription == null) ? 0 : this.productDescription.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (this.getClass() != obj.getClass()) {
      return false;
    }

    final UsbId other = (UsbId) obj;

    if (this.vendorId != other.vendorId) {
      return false;
    }

    if (this.productId != other.productId) {
      return false;
    }

    if (this.vendorDescription == null) {
      if (other.vendorDescription != null) {
        return false;
      }
    } else if (!this.vendorDescription.equals(other.vendorDescription)) {
      return false;
    }

    if (this.productDescription == null) {
      if (other.productDescription != null) {
        return false;
      }
    } else if (!this.productDescription.equals(other.productDescription)) {
      return false;
    }

    return true;
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("UsbId [vendorId=");
    builder.append(this.vendorId);
    builder.append(", productId=");
    builder.append(this.productId);
    builder.append(", vendorDescription=");
    builder.append(this.vendorDescription);
    builder.append(", productDescription=");
    builder.append(this.productDescription);
    builder.append("]");
    return builder.toString();
  }
}