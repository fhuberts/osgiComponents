package nl.pelagic.usb.ids;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "USB IDs", description = "Configuration for the USB IDs service")
@interface Config {
  static public final String USB_IDS_FILE_DEFAULT = "lib/usb.ids";

  @AttributeDefinition(required = false, description = "The file with USB IDs (default = " + USB_IDS_FILE_DEFAULT + ")")
  String usbIdsFile() default USB_IDS_FILE_DEFAULT;
}