package nl.pelagic.inotify.api;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Client interface for INotify events.
 */
@ConsumerType
public interface INotifyClient {
  /**
   * <p>
   * Called for each event on a watch.
   * </p>
   * <p>
   * When the watch is removed an event is sent for the watch descriptor with a mask of IN_IGNORED (name is empty and
   * cookie is 0).
   * </p>
   *
   * @param event The event
   * @throws Throwable Any thrown exception is logged and subsequently ignored
   */
  void notify(INotifyEvent event) throws Throwable;
}