package nl.pelagic.service.log.syslog;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.junit.Ignore;

@Ignore
class MockSysLogLibc implements SysLogLibc {
  public List<String>        log       = new LinkedList<>();

  public static final String LOG_LOAD  = "***LOAD***";
  public static final String LOG_OPEN  = "***OPEN***";
  public static final String LOG_CLOSE = "***CLOSE***";

  @Override
  public void load() throws IOException {
    this.log.add(LOG_LOAD);
  }

  @Override
  public void openlog(final String ident, final int option, final int facility) {
    this.log.add(LOG_OPEN + " " + ident + " " + option + " " + facility);
  }

  @Override
  public void syslog(final int priority, final String format, final Object... args) {
    this.syslog(priority, String.format(format, args));
  }

  @Override
  public void syslog(final int priority, final String s) {
    this.log.add(Integer.toString(priority) + ":" + s);
  }

  @Override
  public void closelog() {
    this.log.add(LOG_CLOSE);
  }
}