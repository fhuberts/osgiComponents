package nl.pelagic.usb.ids;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;
import org.osgi.service.log.LogService;
import org.osgi.service.metatype.annotations.Designate;

import nl.pelagic.usb.ids.api.UsbId;
import nl.pelagic.usb.ids.api.UsbIds;

/**
 * Implementation of the USB IDs database service interface
 */
@Component
@Designate(ocd = Config.class)
public class UsbIdsImpl implements UsbIds {
  private static final Charset              CHARSET = Charset.defaultCharset();

  /*
   * Services
   */

  private final AtomicReference<LogService> logger  = new AtomicReference<>(null);

  @Reference(cardinality = ReferenceCardinality.OPTIONAL, policy = ReferencePolicy.DYNAMIC,
      policyOption = ReferencePolicyOption.GREEDY)
  void setLogger(final LogService logger) {
    this.logger.set(logger);
  }

  void unsetLogger(final LogService logger) {
    this.logger.compareAndSet(logger, null);
  }

  /*
   * Lifecycle
   */

  Config config = null;

  @Activate
  void activate(final Config config) {
    this.config = config;
    final File f = new File(config.usbIdsFile());
    this.read(f);
  }

  @Deactivate
  void deactivate() {
    synchronized (this.usbIdsLock) {
      this.usbIds.clear();
    }
  }

  @Modified
  void modified(final Config config) {
    final Config preConfig = this.config;
    this.config = config;

    if (!preConfig.usbIdsFile().equals(config.usbIdsFile())) {
      final File f = new File(config.usbIdsFile());
      this.read(f);
    }
  }

  /*
   * Helpers
   */

  boolean checkFile(final File f) {
    if (!f.exists() //
        || f.isDirectory() //
        || !f.canRead()) {
      final LogService logger = this.logger.get();
      if (logger != null) {
        logger.log(LogService.LOG_ERROR,
            "USB IDs file \"" + f.getAbsolutePath() + "\" is a directory or can't be read, ignored");
      }
      return false;
    }

    return true;
  }

  static Pattern               commentPattern               = Pattern.compile("^\\s*(|#.*)$");
  static Pattern               vendorDeviceInterfacePattern = Pattern.compile("^([0-9a-fA-F]{4})\\s+(.+?)\\s*$");

  Object                       usbIdsLock                   = new Object();
  Map<Integer, VendorProducts> usbIds                       = new TreeMap<>();

  void read(final File f) {
    if (!this.checkFile(f)) {
      return;
    }

    this.readLoop(f);
  }

  void readLoop(final File f) {
    try (final BufferedReader reader =
        new BufferedReader(new InputStreamReader(Files.newInputStream(f.toPath()), CHARSET))) {
      final Map<Integer, VendorProducts> usbIds = new TreeMap<>();
      VendorProducts vendorProducts = null;

      String line;
      while ((line = reader.readLine()) != null) {
        Matcher m = UsbIdsImpl.commentPattern.matcher(line);
        if (m.matches()) {
          /* skip comments */
          continue;
        }

        /* line is at least 1 character long */

        if (line.charAt(0) != '\t') {
          /* chapter line */

          m = UsbIdsImpl.vendorDeviceInterfacePattern.matcher(line);
          if (!m.matches()) {
            /* skip this chapter */
            vendorProducts = null;
            continue;
          }

          /* Vendors, devices and interfaces chapter */

          /* can't throw an exception because of the regex */
          final int vendorId = Integer.parseInt(m.group(1), 16);

          vendorProducts = new VendorProducts(m.group(2));
          usbIds.put(Integer.valueOf(vendorId), vendorProducts);
          continue;
        }

        /* line starts with a tab character: non-chapter line */

        if (vendorProducts == null) {
          /* not in the 'Vendors, devices and interfaces' chapter: skip */
          continue;
        }

        if (line.charAt(1) == '\t') {
          /* skip interface lines */
          continue;
        }

        line = line.substring(1);

        m = UsbIdsImpl.vendorDeviceInterfacePattern.matcher(line);
        if (!m.matches()) {
          /* not a product line */
          continue;
        }

        /* product line */

        /* can't throw an exception because of the regex */
        final int productId = Integer.parseInt(m.group(1), 16);

        vendorProducts.products.put(Integer.valueOf(productId), m.group(2));
      }

      synchronized (this.usbIdsLock) {
        this.usbIds = usbIds;
      }
    }
    catch (final Exception e) {
      final LogService logger = this.logger.get();
      if (logger != null) {
        logger.log(LogService.LOG_ERROR, "Error while reading '" + f.getAbsolutePath() + "', ignored", e);
      }
    }
  }

  /*
   * UsbIds
   */

  @Override
  public UsbId getUsbId(final int vendorId, final int productId) {
    VendorProducts vendorProducts;
    synchronized (this.usbIdsLock) {
      vendorProducts = this.usbIds.get(Integer.valueOf(vendorId));
    }

    if (vendorProducts == null) {
      final LogService logger = this.logger.get();
      if (logger != null) {
        logger.log(LogService.LOG_WARNING,
            String.format("Unknown USB vendor ID 0x%04x encountered, with product ID 0x%04x", Integer.valueOf(vendorId),
                Integer.valueOf(productId)));
      }

      /* add this unknown vendor so that the log will only occur once */
      vendorProducts = new VendorProducts(String.format("Unknown vendor 0x%04x", Integer.valueOf(vendorId)));
      synchronized (this.usbIdsLock) {
        this.usbIds.put(Integer.valueOf(vendorId), vendorProducts);
      }
    }

    String productDescription = vendorProducts.products.get(Integer.valueOf(productId));
    if (productDescription == null) {
      final LogService logger = this.logger.get();
      if (logger != null) {
        logger.log(LogService.LOG_WARNING,
            String.format("Unknown USB product ID 0x%04x encountered for vendor ID 0x%04x", Integer.valueOf(productId),
                Integer.valueOf(vendorId)));
      }

      /* add this unknown product so that the log will only occur once */
      productDescription = String.format("Unknown product 0x%04x", Integer.valueOf(productId));
      vendorProducts.products.put(Integer.valueOf(productId), productDescription);
    }

    return new UsbId(vendorId, productId, vendorProducts.vendorDescription, productDescription);
  }
}