package nl.pelagic.service.log.syslog;

import java.util.SortedMap;

import org.junit.Ignore;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

@SuppressWarnings("javadoc")
@Ignore
public class MockTracker<S, T> extends ServiceTracker<S, T> {

  public MockTracker(final BundleContext context, final String clazz, final ServiceTrackerCustomizer<S, T> customizer) {
    super(context, clazz, customizer);
  }

  T service = null;

  @Override
  public T getService() {
    return this.service;
  }

  @Override
  public void open() {
    throw new IllegalArgumentException();
  }

  @Override
  public void open(final boolean trackAllServices) {
    throw new IllegalArgumentException();
  }

  @Override
  public void close() {
    throw new IllegalArgumentException();
  }

  @Override
  public T addingService(final ServiceReference<S> reference) {
    throw new IllegalArgumentException();
  }

  @Override
  public void modifiedService(final ServiceReference<S> reference, final T service) {
    throw new IllegalArgumentException();
  }

  @Override
  public void removedService(final ServiceReference<S> reference, final T service) {
    throw new IllegalArgumentException();
  }

  @Override
  public T waitForService(final long timeout) throws InterruptedException {
    throw new IllegalArgumentException();
  }

  @Override
  public ServiceReference<S>[] getServiceReferences() {
    throw new IllegalArgumentException();
  }

  @Override
  public ServiceReference<S> getServiceReference() {
    throw new IllegalArgumentException();
  }

  @Override
  public T getService(final ServiceReference<S> reference) {
    throw new IllegalArgumentException();
  }

  @Override
  public Object[] getServices() {
    throw new IllegalArgumentException();
  }

  @Override
  public void remove(final ServiceReference<S> reference) {
    throw new IllegalArgumentException();
  }

  @Override
  public int size() {
    throw new IllegalArgumentException();
  }

  @Override
  public int getTrackingCount() {
    throw new IllegalArgumentException();
  }

  @Override
  public SortedMap<ServiceReference<S>, T> getTracked() {
    throw new IllegalArgumentException();
  }

  @Override
  public boolean isEmpty() {
    throw new IllegalArgumentException();
  }

  @Override
  public T[] getServices(final T[] array) {
    throw new IllegalArgumentException();
  }
}