package nl.pelagic.service.log.syslog;

import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.log.LogService;

/**
 * Wrap the actual LogService
 */
public class LogServiceFactory implements ServiceFactory<LogService> {

  final SysLogLogger logger;

  LogServiceFactory(final SysLogLogger logger) {
    this.logger = logger;
  }

  /*
   * ServiceFactory
   */

  @Override
  public LogService getService(final Bundle bundle, final ServiceRegistration<LogService> registration) {
    return new SysLogWrapper(this.logger, bundle);
  }

  @Override
  public void ungetService(final Bundle bundle, final ServiceRegistration<LogService> registration,
      final LogService service) {
    if ((service == null) //
        || !(service instanceof SysLogWrapper)) {
      return;
    }

    ((SysLogWrapper) service).unget();
  }

  /*
   * LogService
   */

  static class SysLogWrapper implements LogService {

    SysLogLogger log;
    Bundle       bundle;

    SysLogWrapper(final SysLogLogger log, final Bundle bundle) {
      this.log = log;
      this.bundle = bundle;
    }

    void unget() {
      this.log = null;
      this.bundle = null;
    }

    /*
     * LogService
     */

    @Override
    public void log(final int level, final String message) {
      this.log.log(this.bundle, null, level, message, null, null);
    }

    @Override
    public void log(final int level, final String message, final Throwable exception) {
      this.log.log(this.bundle, null, level, message, exception, null);
    }

    @Override
    public void log(final ServiceReference sr, final int level, final String message) {
      this.log.log(this.bundle, sr, level, message, null, null);
    }

    @Override
    public void log(final ServiceReference sr, final int level, final String message, final Throwable exception) {
      this.log.log(this.bundle, sr, level, message, exception, null);
    }
  }
}