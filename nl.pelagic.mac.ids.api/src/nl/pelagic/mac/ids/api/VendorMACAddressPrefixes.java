package nl.pelagic.mac.ids.api;

import java.util.Set;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Vendor MAC address prefixes service interface
 */
@ProviderType
public interface VendorMACAddressPrefixes {
  /**
   * Retrieve a {@link VendorMACPrefix} instance corresponding to a MAC address prefix
   *
   * @param prefix The MAC address prefix
   * @return null when the prefix is unknown
   */
  VendorMACPrefix getVendorMacPrefix(long prefix);

  /**
   * Retrieve all MAC address prefixes of a vendor
   *
   * @param vendor The vendor name
   * @return null when vendor is null or unknown
   */
  Set<Long> getVendorPrefixes(String vendor);
}