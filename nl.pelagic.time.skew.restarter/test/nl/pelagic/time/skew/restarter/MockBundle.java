package nl.pelagic.time.skew.restarter;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Version;

@Ignore
@SuppressWarnings("javadoc")
public class MockBundle implements Bundle {

  int updateCount = 0;

  @Override
  public int compareTo(final Bundle o) {
    throw new NullPointerException();
  }

  @Override
  public int getState() {
    throw new NullPointerException();
  }

  @Override
  public void start(final int options) throws BundleException {
    throw new NullPointerException();
  }

  @Override
  public void start() throws BundleException {
    throw new NullPointerException();
  }

  @Override
  public void stop(final int options) throws BundleException {
    throw new NullPointerException();
  }

  @Override
  public void stop() throws BundleException {
    throw new NullPointerException();
  }

  @Override
  public void update(final InputStream input) throws BundleException {
    throw new NullPointerException();
  }

  boolean updateThrow = false;

  @Override
  public void update() throws BundleException {
    this.updateCount++;

    if (this.updateThrow) {
      throw new BundleException("TEST UPDATE");
    }
  }

  @Override
  public void uninstall() throws BundleException {
    throw new NullPointerException();
  }

  @Override
  public Dictionary<String, String> getHeaders() {
    throw new NullPointerException();
  }

  @Override
  public long getBundleId() {
    throw new NullPointerException();
  }

  @Override
  public String getLocation() {
    throw new NullPointerException();
  }

  @Override
  public ServiceReference<?>[] getRegisteredServices() {
    throw new NullPointerException();
  }

  @Override
  public ServiceReference<?>[] getServicesInUse() {
    throw new NullPointerException();
  }

  @Override
  public boolean hasPermission(final Object permission) {
    throw new NullPointerException();
  }

  @Override
  public URL getResource(final String name) {
    throw new NullPointerException();
  }

  @Override
  public Dictionary<String, String> getHeaders(final String locale) {
    throw new NullPointerException();
  }

  @Override
  public String getSymbolicName() {
    throw new NullPointerException();
  }

  @Override
  public Class<?> loadClass(final String name) throws ClassNotFoundException {
    throw new NullPointerException();
  }

  @Override
  public Enumeration<URL> getResources(final String name) throws IOException {
    throw new NullPointerException();
  }

  @Override
  public Enumeration<String> getEntryPaths(final String path) {
    throw new NullPointerException();
  }

  @Override
  public URL getEntry(final String path) {
    throw new NullPointerException();
  }

  @Override
  public long getLastModified() {
    throw new NullPointerException();
  }

  @Override
  public Enumeration<URL> findEntries(final String path, final String filePattern, final boolean recurse) {
    throw new NullPointerException();
  }

  @Override
  public BundleContext getBundleContext() {
    throw new NullPointerException();
  }

  @Override
  public Map<X509Certificate, List<X509Certificate>> getSignerCertificates(final int signersType) {
    throw new NullPointerException();
  }

  @Override
  public Version getVersion() {
    throw new NullPointerException();
  }

  @Override
  public <A> A adapt(final Class<A> type) {
    throw new NullPointerException();
  }

  @Override
  public File getDataFile(final String filename) {
    throw new NullPointerException();
  }
}