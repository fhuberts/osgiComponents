package nl.pelagic.usb.ids;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

import org.junit.Test;

@SuppressWarnings({
    "static-method",
    "javadoc"
})
public class TestVendorProducts {
  @Test(timeout = 8000)
  public void testVendorProducts() {
    VendorProducts vp;

    vp = new VendorProducts("vendorDescription");

    assertThat(vp, notNullValue());
    assertThat(vp.vendorDescription, equalTo("vendorDescription"));
    assertThat(vp.products, notNullValue());
    assertThat(Integer.valueOf(vp.products.size()), equalTo(Integer.valueOf(0)));

    assertThat(vp.toString(), notNullValue());
  }
}