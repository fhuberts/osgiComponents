package nl.pelagic.service.log.syslog;

import org.junit.Ignore;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkListener;
import org.osgi.framework.startlevel.FrameworkStartLevel;

@SuppressWarnings("javadoc")
@Ignore
public class MockFrameworkStartLevel implements FrameworkStartLevel {
  @Override
  public Bundle getBundle() {
    throw new IllegalStateException();
  }

  int startLevel = -1;

  @Override
  public int getStartLevel() {
    return this.startLevel;
  }

  @Override
  public void setStartLevel(final int startlevel, final FrameworkListener... listeners) {
    throw new IllegalStateException();
  }

  @Override
  public int getInitialBundleStartLevel() {
    throw new IllegalStateException();
  }

  @Override
  public void setInitialBundleStartLevel(final int startlevel) {
    throw new IllegalStateException();
  }
}