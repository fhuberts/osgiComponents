package nl.pelagic.inotify;

import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;
import org.osgi.service.log.LogService;

import nl.pelagic.inotify.api.INotify;
import nl.pelagic.inotify.api.INotifyClient;
import nl.pelagic.inotify.api.INotifyEvent;
import nl.pelagic.inotify.api.INotifyException;
import nl.pelagic.inotify.api.INotifyLinuxErrno;
import nl.pelagic.inotify.api.INotifyMask;

@Component(service = {
    INotify.class
})
class INotifyImpl extends Thread implements INotify {

  /*
   * Services
   */

  final AtomicReference<LogService> logger = new AtomicReference<>(null);

  @Reference(cardinality = ReferenceCardinality.OPTIONAL, policy = ReferencePolicy.DYNAMIC,
      policyOption = ReferencePolicyOption.GREEDY)
  void setLogger(final LogService logger) {
    this.logger.set(logger);
  }

  void unsetLogger(final LogService logger) {
    this.logger.compareAndSet(logger, null);
  }

  /*
   * Native Methods
   */

  /**
   * Activate the native code.
   *
   * @return
   *         <ul>
   *         <li>0 When successful or when the native code is already in the ACTIVATED state,</li>
   *         <li>-ENOENT When the required callbacks (see {@link #callbackLog(int, String)},
   *         {@link #callbackProcessEvent(String, int, long, long)} and {@link #callbackRunning(boolean)}) could not be
   *         found in this class (logged).</li>
   *         <li>-ENOMEM When the memory allocation for the event buffer failed (logged).</li>
   *         <li>-EPERM When the native code is not in the DEACTIVATED state (logged).</li>
   *         </ul>
   */
  native int activateNative();

  /**
   * Deactivate the native code.
   *
   * @return
   *         <ul>
   *         <li>0 When successful or when the native code is already in the DEACTIVATED state.</li>
   *         <li>-EPERM When the native code is not in either the ACTIVATED state or the RUNNING state (logged).</li>
   *         </ul>
   */
  native int deactivateNative();

  /**
   * <p>
   * Run the native code's event loop.
   * </p>
   * <p>
   * Note: this method does NOT return until the event loop finishes and thus should be run in its own thread.
   * </p>
   *
   * @return
   *         <ul>
   *         <li>0 When the event loop finished successfully.</li>
   *         <li>-EINVAL When the native code tries to use an invalid signal (logged).</li>
   *         <li>-EMFILE When the user limit on the total number of inotify instances has been reached, or when the
   *         per-process limit on the number of open file descriptors has been reached (logged).</li>
   *         <li>-ENFILE When the system-wide limit on the total number of open files has been reached (logged).</li>
   *         <li>-ENOMEM When the is insufficient kernel memory available (logged).</li>
   *         <li>-EPERM When the native code is not in the ACTIVATED state (logged).</li>
   *         </ul>
   */
  native int inotifyLoopNative();

  /**
   * Add a watch to an initialised inotify instance (see 'man 2 inotify_add_watch').
   *
   * <pre>
   * SYNOPSIS
   *   int inotify_add_watch(int fd, const char *pathname, uint32_t mask);
   *
   * DESCRIPTION
   *   inotify_add_watch() adds a new watch, or modifies an existing watch, for the file whose location is specified in
   *   pathname; the caller must have read permission for this file. The fd argument is a file descriptor referring to
   *   the inotify instance whose watch list is to be modified. The events to be monitored for pathname are specified
   *   in the mask bit-mask argument. See inotify(7) for a description of the bits that can be set in mask.
   *
   *   A successful call to inotify_add_watch() returns a unique watch descriptor for this inotify instance, for the
   *   filesystem object that corresponds to pathname. If the filesystem object was not previously being watched by
   *   this inotify instance, then the watch descriptor is newly allocated. If the filesystem object was already being
   *   watched (perhaps via a different link to the same object), then the descriptor for the existing watch is
   *   returned.
   *
   *   The watch descriptor is returned by later read(2)s from the inotify file descriptor. These reads fetch
   *   inotify_event structures (see inotify(7)) indicating filesystem events; the watch descriptor inside this
   *   structure identifies the object for which the event occurred.
   *
   * RETURN VALUE
   *   On success, inotify_add_watch() returns a nonnegative watch descriptor. On error, -1 is returned and errno is set
   *   appropriately.
   *
   * ERRORS
   *   EACCES       Read access to the given file is not permitted.
   *   EBADF        The given file descriptor is not valid.
   *   EFAULT       Pathname points outside of the process's accessible address space.
   *   EINVAL       The given event mask contains no valid events; or fd is not an inotify file descriptor.
   *   ENAMETOOLONG Pathname is too long.
   *   ENOENT       A directory component in pathname does not exist or is a dangling symbolic link.
   *   ENOMEM       Insufficient kernel memory was available.
   *   ENOSPC       The user limit on the total number of inotify watches was reached or the kernel failed to allocate
   *                a needed resource.
   * </pre>
   *
   * @param path The path to add a watch on.
   * @param mask The event mask to use (see {@link INotifyMask}).
   * @return
   *         <ul>
   *         <li>A nonnegative watch descriptor on success.</li>
   *         <li>-EACCES When read access to the given path is not permitted (logged).</li>
   *         <li>-EINVAL When the given event mask contains no valid events (logged).</li>
   *         <li>-ENAMETOOLONG When the path is too long (logged).</li>
   *         <li>-ENOENT When a directory component in pathname does not exist or is a dangling symbolic link
   *         (logged).</li>
   *         <li>-ENOMEM When the memory allocation for the path string conversion failed or When there is insufficient
   *         kernel memory available (logged).</li>
   *         <li>-ENOSPC When the user limit on the total number of inotify watches was reached, or when the kernel
   *         failed to allocate a needed resource (logged).</li>
   *         <li>-EPERM When the native code is not in the RUNNING state (logged).</li>
   *         </ul>
   */
  native int addWatchNative(String path, long mask);

  /**
   * Remove an existing watch from an inotify instance.
   *
   * <pre>
   * SYNOPSIS
   *   int inotify_rm_watch(int fd, int wd);
   *
   * DESCRIPTION
   *   inotify_rm_watch() removes the watch associated with the watch descriptor wd from the inotify instance
   *   associated with the file descriptor fd.
   *
   *   Removing a watch causes an IN_IGNORED event to be generated for this watch descriptor; see inotify(7).
   *
   * RETURN VALUE
   *   On success, inotify_rm_watch() returns zero. On error, -1 is returned and errno is set to indicate the cause of
   *   the error.
   *
   * ERRORS
   *   EBADF  fd is not a valid file descriptor.
   *   EINVAL The watch descriptor wd is not valid; or fd is not an inotify file descriptor.
   * </pre>
   *
   * @param wd The watch descriptor to remove the watch from
   * @return
   *         <ul>
   *         <li>0 on success.</li>
   *         <li>-EINVAL When the watch descriptor wd is not valid (logged).</li>
   *         <li>-EPERM When the native code is not in the RUNNING state (logged).</li>
   *         </ul>
   */
  native int removeWatchNative(int wd);

  /**
   * Convert an error number into the corresponding error text.
   *
   * @param errno The error number.
   * @return The corresponding error text (can be null).
   */
  native String getStrErrorNative(int errno);

  /*
   * Lifecycle
   */

  Thread inotifyEventLoopThread       = null;
  int    inotifyEventLoopThreadResult = 0;

  void setInotifyEventLoopThreadResult(final int inotifyEventLoopThreadResult) {
    this.inotifyEventLoopThreadResult = inotifyEventLoopThreadResult;
  }

  static void loadLibrary(final String name) throws INotifyException {
    try {
      System.loadLibrary(name);
    }
    catch (final Throwable e) {
      throw new INotifyException("Failed to load the native code", e);
    }
  }

  @Activate
  void activate() throws INotifyException {
    if (this.inotifyEventLoopThread != null) {
      return;
    }

    INotifyImpl.loadLibrary("osgiinotify");

    final int r = this.activateNative();
    if (r != 0) {
      throw new INotifyException("Failed to activate the native code: " + this.getStrErrorNative(-r), -r);
    }

    /*
     * Native code's event loop thread
     */

    this.inotifyEventLoopThread = new Thread() {
      @Override
      public void run() {
        final int r = -INotifyImpl.this.inotifyLoopNative();
        INotifyImpl.this.setInotifyEventLoopThreadResult(r);
      }
    };

    this.inotifyEventLoopThread.setName(this.getClass().getSimpleName() + "-native-event-loop");
    this.inotifyEventLoopThread.setDaemon(true);

    /* synchronized to prevent a race */
    boolean running = false;
    synchronized (this.nativeIsRunning) {
      this.inotifyEventLoopThread.start();
      running = this.waitForRunningAndGet();
    }

    if (!running) {
      this.inotifyEventLoopThread = null;
      throw new INotifyException("The native code's event loop failed during activation with error: "
          + this.getStrErrorNative(this.inotifyEventLoopThreadResult), this.inotifyEventLoopThreadResult);
    }

    /*
     * Client notifier thread
     */

    this.run.set(true);
    this.setName(this.getClass().getSimpleName() + "-eventQueue-reader");
    this.setDaemon(true);
    this.start();
  }

  @Deactivate
  void deactivate() {
    /* Remove all remaining watches (and thus notify all their clients that their watch is being shut down) */
    for (final Map.Entry<Integer, INotifyClient> entry : this.clientMap.entrySet()) {
      final Integer wd = entry.getKey();

      try {
        this.removeWatch(wd.intValue(), false);
      }
      catch (final INotifyException e) {
        this.clientMap.remove(wd);
      }
    }

    this.waitUntilEventQueueIsEmpty();

    /* Stop the eventQueue reader thread */
    this.run.set(false);
    this.interrupt();
    this.waitForEventQueueReaderThreadEnd();

    /* Stop the native code's event loop thread */
    final int r = this.deactivateNative();
    if (r != 0) {
      this.callbackLog(LinuxLogLevels.LOG_ERR,
          "Failed to stop the native code's event loop with error: " + this.getStrErrorNative(-r) + ", errno = " + -r);
    } else if (this.inotifyEventLoopThread != null) {
      this.waitForEventLoopEnd();
      this.logWhenEventLoopFailed();
    }
    this.inotifyEventLoopThread = null;

    this.eventQueue.clear();
  }

  /**
   * Wait until the event queue is empty
   */
  void waitUntilEventQueueIsEmpty() {
    while (!this.eventQueue.isEmpty() //
        || !this.clientMap.isEmpty()) {
      try {
        Thread.sleep(1);
      }
      catch (final InterruptedException e) {
        /* swallow */
      }
    }
  }

  /**
   * Wait for the event queue reader thread to end
   */
  void waitForEventQueueReaderThreadEnd() {
    try {
      this.join();
    }
    catch (final InterruptedException e) {
      /* swallow */
    }
  }

  /**
   * Wait for the event loop thread to end
   */
  void waitForEventLoopEnd() {
    try {
      this.inotifyEventLoopThread.join();
    }
    catch (final InterruptedException e) {
      /* swallow */
    }
  }

  /**
   * Log a message when the event loop thread ended with a failure
   */
  void logWhenEventLoopFailed() {
    if (this.inotifyEventLoopThreadResult != 0) {
      this.callbackLog(LinuxLogLevels.LOG_ERR,
          "The native code's event loop exited during deactivation with error: "
              + this.getStrErrorNative(this.inotifyEventLoopThreadResult) + ", errno = "
              + this.inotifyEventLoopThreadResult);
    }
  }

  /*
   * Callbacks (the C code calls these). Do NOT rename these method or change their signatures unless you also update
   * those in the C code!
   */

  /**
   * Callback for the native code: called when it needs to log a message.
   *
   * @param level The Linux log level
   * @param s The message to log
   */
  void callbackLog(final int level, final String s) {
    final LogService logger = this.logger.get();
    if (logger != null) {
      logger.log(LinuxLogLevels.getLogServiceLevel(level), s);
    }
  }

  /**
   * Callback for the native code: called when its event loop needs to signal a change in its running state.
   *
   * @param running The running state of the native code's event loop.
   */
  void callbackRunning(final boolean running) {
    synchronized (this.nativeIsRunning) {
      this.nativeIsRunning.set(running);
      this.nativeIsRunning.notifyAll();
    }
  }

  /**
   * Callback for the native code: called for each available event.
   *
   * @param name The filename that caused the event(s).
   * @param wd The watch descriptor corresponding to the event(s).
   * @param mask The events, in the form of a bit-mask.
   * @param cookie A unique cookie for correlating events (moves).
   */
  void callbackProcessEvent(final String name, final int wd, final long mask, final long cookie) {
    this.eventQueue.offer(new INotifyEvent(name, wd, mask, cookie));
  }

  /*
   * Native code's event loop thread
   */

  final AtomicBoolean nativeIsRunning = new AtomicBoolean(false);

  boolean waitForRunningAndGet() {
    synchronized (this.nativeIsRunning) {
      try {
        this.nativeIsRunning.wait();
      }
      catch (final InterruptedException e) {
        /* swallow */
      }
      return this.nativeIsRunning.get();
    }
  }

  /*
   * eventQueue reader thread
   */

  volatile AtomicBoolean      run             = new AtomicBoolean(true);
  BlockingDeque<INotifyEvent> eventQueue      = new LinkedBlockingDeque<>();
  AtomicBoolean               eventQueuePause = new AtomicBoolean(false);

  @Override
  public void run() {
    while (this.run.get()) {
      INotifyEvent event = null;
      try {
        event = this.eventQueue.take();
      }
      catch (final InterruptedException e) {
        /* swallow */
      }

      if (event == null) {
        continue;
      }

      while (this.eventQueuePause.get()) {
        try {
          Thread.sleep(10);
        }
        catch (final InterruptedException e) {
          /* swallow */
        }
      }

      final int wd = event.getWd();

      final INotifyClient client = this.clientMap.get(Integer.valueOf(wd));
      if (client == null) {
        continue;
      }

      /* After a watch is removed we'll get an IN_IGNORED event for that watch */
      if (event.getMask() == INotifyMask.IN_IGNORED) {
        this.clientMap.remove(Integer.valueOf(wd));
      }

      try {
        client.notify(event);
      }
      catch (final Throwable e) {
        final LogService logger = this.logger.get();
        if (logger != null) {
          logger.log(LogService.LOG_ERROR,
              "Exception during event handling by " + client + " of event " + event.toString(), e);
        }
      }
    }
  }

  /*
   * INotify
   */

  Map<Integer, INotifyClient> clientMap = new ConcurrentHashMap<>();

  @Override
  public int addWatch(final INotifyClient client, final String path, final long mask) throws INotifyException {
    if (client == null) {
      throw new INotifyException("addWatch: client can not be null", INotifyLinuxErrno.EINVAL);
    }

    final int wd = this.addWatchNative(path, mask);
    if (wd < 0) {
      throw new INotifyException(
          String.format("Failed adding a watch on '%s' (mask %08x) for client class '%s' with error: %s", path,
              Long.valueOf(mask), client.getClass(), this.getStrErrorNative(-wd)),
          -wd);
    }

    this.clientMap.put(Integer.valueOf(wd), client);

    return wd;
  }

  @Override
  public void removeWatch(final int wd, final boolean immediate) throws INotifyException {
    this.eventQueuePause.set(true);
    try {
      final int r = this.removeWatchNative(wd);
      if (r < 0) {
        throw new INotifyException(
            String.format("Failed removing watch '%d' with error: %s", Integer.valueOf(wd), this.getStrErrorNative(-r)),
            -r);
      }

      /*
       * Note: the behaviour w.r.t. an IN_IGNORED event being generated after deletion of a watch depends entirely on
       * the Linux kernel. All kernels since 2.6.36 do this. If you run this code on an older kernel then such an event
       * must be offered to the eventQueue here (when !immediate).
       */

      if (immediate) {
        this.clientMap.remove(Integer.valueOf(wd));
      }
    }
    finally {
      this.eventQueuePause.set(false);
      this.interrupt();
    }
  }

  @Override
  public String getStrError(final int errno) {
    return this.getStrErrorNative(errno);
  }

  /*
   * ************** Test Support **************
   */

  Map<String, Integer> callbackTestMap = null;

  int callbackTest(final String function) {
    if (this.callbackTestMap == null) {
      return 0;
    }

    final Integer r = this.callbackTestMap.get(function);
    if (r == null) {
      return 0;
    }

    return r.intValue();
  }
}