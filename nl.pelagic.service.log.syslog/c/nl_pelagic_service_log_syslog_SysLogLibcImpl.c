#include "nl_pelagic_service_log_syslog_SysLogLibcImpl.h"

#include <syslog.h>

JNIEXPORT void JNICALL Java_nl_pelagic_service_log_syslog_SysLogLibcImpl_openlog(
    JNIEnv *env,
    jobject this __attribute__((unused)),
    jstring ident,
    jint option,
    jint facility) {
  const char * id = (*env)->GetStringUTFChars(env, ident, NULL);
  openlog(id, option, facility);
}

JNIEXPORT void JNICALL Java_nl_pelagic_service_log_syslog_SysLogLibcImpl_syslog(
    JNIEnv *env,
    jobject this __attribute__((unused)),
    jint priority,
    jstring message) {
  const char * msg = (*env)->GetStringUTFChars(env, message, NULL);
  if (!msg) {
    msg = "";
  }
  syslog(priority, "%s", msg);
}

JNIEXPORT void JNICALL Java_nl_pelagic_service_log_syslog_SysLogLibcImpl_closelog(
    JNIEnv *env __attribute__((unused)),
    jobject this __attribute__((unused))) {
  closelog();
}
