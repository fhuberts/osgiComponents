######################
#
# Settings
#
######################

DEBUG         ?= 0

BINDIR        ?= bin
BINDIRC       ?= bin_c
BINDIRCTEST   ?= $(BINDIRC)/test
BINDIRJNI     ?= $(BINDIRC)/java
LIBDIR        ?= lib
LIBDIRTEST    ?= testresources/$(LIBDIR)

STRIP         ?= :
MAKECMDPREFIX ?= @
EXTRACFLAGS   ?=
EXTRACPPFLAGS ?=
OPTIMIZE      ?=

MAKEFILEDIR    = $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
WSDIR          = $(realpath $(MAKEFILEDIR)/..)




#
# Java settings and detection
#

JAVASRCAPI     = $(WSDIR)/$(JAVAPKG).api/src
JAVASRC        = $(MAKEFILEDIR)/src
JAVAVERSIONSRC = 1.8
JAVAVERSIONDST = 1.8
JAVAPKG        = nl.pelagic.inotify
JAVACLASSNAME  = INotifyImpl
CSRCNAME       = $(JAVAPKGUNDER)_$(JAVACLASSNAME)
LIBNAME        = osgiinotify
JAVAPKGDIR     = $(subst .,/,$(JAVAPKG))
JAVAPKGUNDER   = $(subst .,_,$(JAVAPKG))

CNF            = $(WSDIR)/cnf
BUILDREPO      = $(CNF)/buildrepo
OSGICORE       = $(BUILDREPO)/osgi.core/osgi.core-6.0.0.jar
OSGICMPN       = $(BUILDREPO)/osgi.cmpn/osgi.cmpn-6.0.0.jar
OSGIANNO       = $(BUILDREPO)/osgi.annotation/osgi.annotation-6.0.1.jar

JAVAHOMEREGEX  = ^[[:space:]]*java\.home[[:space:]]*=[[:space:]]*(.+)[[:space:]]*$$
JAVAHOME       = $(shell java -XshowSettings 2>&1 | grep -E '$(JAVAHOMEREGEX)' | sed -r 's/$(JAVAHOMEREGEX)/\1/')

ifeq ($(JAVAHOME),)
  $(error No java detected)
endif

INCDIRJVM      = $(realpath $(JAVAHOME)/../include)

ifeq ($(wildcard $(INCDIRJVM)),)
  $(error No java include directory detected at $(INCDIRJVM))
endif

INCDIRJVMOS    = $(realpath $(INCDIRJVM)/linux)

ifeq ($(wildcard $(INCDIRJVMOS)),)
  $(error No java OS include directory detected at $(INCDIRJVMOS))
endif




#
# C settings
#

SONAME         = lib$(LIBNAME).so
CSRC           = c
CINC           = -I "$(INCDIRJVM)" -I "$(INCDIRJVMOS)"

CFLAGS        += -fPIC -D_POSIX_C_SOURCE=200809L -D_GNU_SOURCE
LDFLAGS       += -fPIC -shared -Wl,--warn-common -lpthread

# 32/64 cross compilation
ifdef M32
CFLAGS        += -m32
LDFLAGS       += -m32
endif

ifdef M64
CFLAGS        += -m64
LDFLAGS       += -m64
endif

# debugging or non-debugging flags
ifeq ($(DEBUG),1)
STRIP          = :
CPPFLAGS      += -DDEBUG
CFLAGS        += -ggdb
LDFLAGS       += -rdynamic
else
STRIP          = strip
CPPFLAGS      += -DNDEBUG
  ifeq ($(OPTIMIZE),)
  OPTIMIZE    += -O2 -D_FORTIFY_SOURCE=2
  endif
endif


CFLAGS        += \
  -fearly-inlining -finline-functions-called-once -finline-limit=350 -funit-at-a-time

WARNINGS      += \
 -Waggregate-return -Wall -Wbad-function-cast -Wcast-align -Wcast-qual -Wconversion -Wdeclaration-after-statement \
 -Wdisabled-optimization -Wendif-labels -Werror -Werror=format-security -Wextra -Wformat -Wformat-security \
 -Wformat-y2k -Winit-self -Winline -Wmissing-declarations -Wmissing-format-attribute -Wmissing-noreturn \
 -Wmissing-prototypes -Wno-deprecated-declarations -Wno-multichar -Wnull-dereference -Wold-style-definition \
 -Wpointer-arith -Wredundant-decls -Wsequence-point -Wshadow -Wshift-negative-value -Wshift-overflow \
 -Wsign-compare -Wstrict-prototypes -Wswitch-default -Wtautological-compare -Wundef -Wunreachable-code \
 -Wunused-parameter -Wwrite-strings

ifeq ($(DEBUG),0)
WARNINGS      += \
  -fomit-frame-pointer
endif

ifneq ($(CC),clang)
WARNINGS      += \
 -Wdouble-promotion -Wduplicated-cond -Wjump-misses-init -Wlogical-op -Wmisleading-indentation -Wsync-nand \
 -Wtrampolines
endif

CFLAGS   += $(WARNINGS)
CFLAGS   += $(OPTIMIZE)
CFLAGS   += $(EXTRACFLAGS)
CPPFLAGS += $(EXTRACPPFLAGS)




#
# Targets
#

all: prepare $(LIBDIR)/$(SONAME) $(LIBDIRTEST)/$(SONAME)


#
# Java --> C header file(s)
#

$(CSRC)/headers: $(JAVASRC)/$(JAVAPKGDIR)/*.java
	@echo "[JavaH  ]"
	$(MAKECMDPREFIX)$(WSDIR)/gradlew classes
	$(MAKECMDPREFIX)touch "$@"


#
# C compilation
#

# compile the c code
$(BINDIRC)/$(CSRCNAME).o: $(CSRC)/$(CSRCNAME).c $(CSRC)/headers
	@echo "[CC     ] $@ <-- $<"
	$(MAKECMDPREFIX)$(CC) $(CFLAGS) $(CPPFLAGS) $(CINC) -c -o "$@" "$<"
	$(MAKECMDPREFIX)touch "$@"

# link the c code into a library
$(LIBDIR)/$(SONAME): $(BINDIRC)/$(CSRCNAME).o
	@echo "[LD     ] $@ <-- $<"
	$(MAKECMDPREFIX)$(CC) $(LDFLAGS) -Wl,-soname=$(SONAME) -o "$@" $(BINDIRC)/$(CSRCNAME).o
	$(MAKECMDPREFIX)$(STRIP) "$@"
	$(MAKECMDPREFIX)touch "$@"


#
# C compilation for tests
#

# compile the c code
$(BINDIRCTEST)/$(CSRCNAME).o: $(CSRC)/$(CSRCNAME).c $(CSRC)/headers
	@echo "[CC     ] $@ <-- $<"
	$(MAKECMDPREFIX)$(CC) -DJUNIT_TESTS $(CFLAGS) $(CPPFLAGS) $(CINC) -c -o "$@" "$<"
	$(MAKECMDPREFIX)touch "$@"

# link the c code into a library
$(LIBDIRTEST)/$(SONAME): $(BINDIRCTEST)/$(CSRCNAME).o
	@echo "[LD     ] $@ <-- $<"
	$(MAKECMDPREFIX)$(CC) -DJUNIT_TESTS $(LDFLAGS) -Wl,-soname=$(SONAME) -o "$@" $(BINDIRCTEST)/$(CSRCNAME).o
	$(MAKECMDPREFIX)$(STRIP) "$@"
	$(MAKECMDPREFIX)touch "$@"




#
# Phony Targets
#

.PHONY: full release debug prepare scrub clean

full: release debug

release:
	@echo "[release]"
	$(MAKECMDPREFIX)$(MAKE) M32=1 DEBUG=0 LIBDIR=lib/release/32 scrub all
	$(MAKECMDPREFIX)$(MAKE) M64=1 DEBUG=0 LIBDIR=lib/release/64 scrub all

debug:
	@echo "[debug  ]"
	$(MAKECMDPREFIX)$(MAKE) M32=1 DEBUG=1 LIBDIR=lib/debug/32 scrub all
	$(MAKECMDPREFIX)$(MAKE) M64=1 DEBUG=1 LIBDIR=lib/debug/64 scrub all

prepare:
	@echo "[prepare]"
	$(MAKECMDPREFIX)mkdir -p \
      "$(BINDIRC)" \
      "$(BINDIRCTEST)" \
      "$(BINDIRJNI)" \
      "$(LIBDIR)" \
      "$(LIBDIRTEST)"

scrub:
	@echo "[scrub  ]"
	$(MAKECMDPREFIX)rm -fr \
      "$(BINDIRC)" \
      "$(BINDIRCTEST)"

clean: scrub
	@echo "[clean  ]"
	$(MAKECMDPREFIX)rm -fr \
      "$(BINDIRJNI)" \
      "$(LIBDIR)/"* \
      "$(LIBDIRTEST)/"* \
      "$(CSRC)/$(CSRCNAME).h" \
      "$(CSRC)/headers"
