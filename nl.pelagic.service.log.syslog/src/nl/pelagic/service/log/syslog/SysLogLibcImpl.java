package nl.pelagic.service.log.syslog;

import java.io.IOException;

class SysLogLibcImpl implements SysLogLibc {

  String libraryName = "sysloglibc";

  @Override
  public void load() throws IOException {
    try {
      System.loadLibrary(this.libraryName);
    }
    catch (final Throwable e) {
      throw new IOException(e);
    }
  }

  @Override
  public native void openlog(final String ident, final int option, final int facility);

  @Override
  public void syslog(final int priority, final String format, final Object... args) {
    this.syslog(priority, String.format(format, args));
  }

  @Override
  public native void syslog(final int priority, final String s);

  @Override
  public native void closelog();
}
