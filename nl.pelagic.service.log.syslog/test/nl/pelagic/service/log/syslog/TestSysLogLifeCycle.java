package nl.pelagic.service.log.syslog;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.log.LogReaderService;
import org.osgi.service.log.LogService;

@SuppressWarnings({
    "unused",
    "rawtypes",
    "unchecked",
    "javadoc",
})
public class TestSysLogLifeCycle {

  SysLog                              impl;
  MockBundleContext                   ctx;
  MockLogListener                     listener;
  MockSysLogLibc                      libc;
  MockEventAdmin                      ea;
  MockTracker<EventAdmin, EventAdmin> tracker;
  MockServiceRegistration<?>          regMulti;
  MockServiceReference<?>             refMulti;
  MockServiceRegistration<?>          regSingle;
  MockServiceReference<?>             refSingle;
  MockBundle                          bundle;

  @Before
  public void setUp() throws Exception {
    System.clearProperty(SysLog.ENV_LOG_SERVICE_NAME);
    System.clearProperty(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL);
    System.clearProperty(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC);
    System.clearProperty(SysLog.ENV_LOG_READER_DEPTH);

    this.regMulti = new MockServiceRegistration<>();
    this.refMulti = new MockServiceReference<>();
    this.regSingle = new MockServiceRegistration<>();
    this.refSingle = new MockServiceReference<>();
    this.bundle = new MockBundle();

    this.libc = new MockSysLogLibc();

    this.ea = new MockEventAdmin();

    this.ctx = new MockBundleContext();
    this.listener = new MockLogListener();

    this.impl = new SysLog();
    this.impl.libc = this.libc;

    this.tracker = new MockTracker<>(this.ctx, this.getClass().getName(), null);
    this.impl.eventAdminTracker = this.tracker;

    assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(0)));
  }

  @After
  public void tearDown() throws Exception {
    this.impl.run.set(false);
    synchronized (this.impl.queue) {
      this.impl.queue.notifyAll();
    }

    while (this.impl.isAlive()) {
      Thread.sleep(10);
    }

    this.impl.eventAdminTracker = null;
    this.tracker = null;
    this.impl = null;
    this.listener = null;
    this.ctx = null;
    this.ea = null;
    this.libc = null;
    this.bundle = null;
    this.refSingle = null;
    this.regSingle = null;
    this.refMulti = null;
    this.regMulti = null;
  }

  @Test(timeout = 8000)
  public void testStartStop() throws InterruptedException, IOException {
    assertThat(Integer.valueOf(this.ctx.bundleListeners.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(this.ctx.frameworkListeners.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(this.ctx.serviceListeners.size()), equalTo(Integer.valueOf(0)));

    this.regMulti.reference = (ServiceReference) this.refMulti;
    this.ctx.registerServiceReturnMulti = this.regMulti;

    this.regSingle.reference = (ServiceReference) this.refSingle;
    this.ctx.registerServiceReturnSingle = this.regSingle;

    this.ctx.bundle = this.bundle;

    this.impl.logServiceName = SysLog.ENV_LOG_SERVICE_NAME_DEFAULT + "*dummy*";

    /* start */

    this.impl.start(this.ctx);
    while (!this.impl.isAlive() && (this.libc.log.size() != 2)) {
      Thread.sleep(10);
    }

    assertThat(this.impl.logServiceName, equalTo(SysLog.ENV_LOG_SERVICE_NAME_DEFAULT + "*dummy*"));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_NAME),
        equalTo(SysLog.ENV_LOG_SERVICE_NAME_DEFAULT + "*dummy*"));

    assertThat(Integer.valueOf(this.impl.logServiceLowestLevel),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL),
        equalTo(LogUtils.levelToTopicLevel(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL_DEFAULT)));

    assertThat(Integer.valueOf(this.impl.logServiceQueueDrainWaitMsec),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC_DEFAULT)));
    assertThat(Integer.valueOf(System.getProperty(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC)),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC_DEFAULT)));

    assertThat(Integer.valueOf(this.impl.logReaderDepth),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_READER_DEPTH_DEFAULT)));
    assertThat(Integer.valueOf(this.impl.logStore.depth),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_READER_DEPTH_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_READER_DEPTH), equalTo("" + SysLog.ENV_LOG_READER_DEPTH_DEFAULT));
    assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(2)));
    assertThat(this.libc.log.get(0), equalTo(MockSysLogLibc.LOG_LOAD));
    assertThat(this.libc.log.get(1), equalTo(MockSysLogLibc.LOG_OPEN + " *dummy* 3 8"));
    assertThat(Boolean.valueOf(this.impl.run.get()), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(this.ctx.bundleListeners.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(this.ctx.bundleListeners.contains(this.impl)), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(this.ctx.frameworkListeners.size()), equalTo(Integer.valueOf(1)));
    assertThat(Boolean.valueOf(this.ctx.frameworkListeners.contains(this.impl)), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(this.ctx.serviceListeners.size()), equalTo(Integer.valueOf(2)));
    ServiceListener sl = this.ctx.serviceListeners.get(0);
    assertThat(sl, equalTo((ServiceListener) this.impl));
    sl = this.ctx.serviceListeners.get(1);
    assertThat(sl, notNullValue()); /* tracker */
    assertThat(Boolean.valueOf(this.impl.eventAdminTracker.isEmpty()), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(this.ctx.serviceRegistrations.size()), equalTo(Integer.valueOf(2)));

    final String[] clazzesMulti = {
        ManagedService.class.getName(), //
        LogReaderService.class.getName()
    };
    final Dictionary<String, Object> propsMulti = new Hashtable<>();
    propsMulti.put(Constants.SERVICE_PID, this.impl.getClass().getName());
    final SVCRegistration expregMulti = new SVCRegistration(clazzesMulti, this.impl, propsMulti);
    assertThat(Boolean.valueOf(this.ctx.serviceRegistrations.contains(expregMulti)), equalTo(Boolean.TRUE));
    assertThat(this.impl.serviceRegistration, equalTo((ServiceRegistration) this.regMulti));
    assertThat(this.impl.serviceReference, equalTo((ServiceReference) this.refMulti));

    final String[] clazzesSingle = {
        LogService.class.getName()
    };
    final Dictionary<String, Object> propsSingle = new Hashtable<>();
    propsSingle.put(Constants.SERVICE_PID, LogServiceFactory.class.getName());
    final SVCRegistration expregSingle = new SVCRegistration(clazzesSingle, this.impl.logServiceFactory, propsSingle);
    assertThat(Boolean.valueOf(this.ctx.serviceRegistrations.contains(expregSingle)), equalTo(Boolean.TRUE));
    assertThat(this.impl.serviceRegistrationLSF, equalTo((ServiceRegistration) this.regSingle));

    assertThat(this.impl.bundle, equalTo((Bundle) this.bundle));

    /* clear */

    this.libc.log.clear();

    this.impl.addLogListener(this.listener);

    /* stop */

    assertThat(this.regMulti.reference, notNullValue());
    assertThat(this.regSingle.reference, notNullValue());

    assertThat(Boolean.valueOf(this.impl.signalEmpty.get()), equalTo(Boolean.FALSE));

    this.listener.logSleep = 10;
    this.impl.log(null, null, LogService.LOG_ERROR, "dummy 1", null, null);
    this.impl.log(null, null, LogService.LOG_ERROR, "dummy 2", null, null);
    this.impl.log(null, null, LogService.LOG_ERROR, "dummy 3", null, null);

    while (this.libc.log.size() != 3) {
      Thread.sleep(10);
    }

    assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(3)));
    assertThat(this.libc.log.get(0), equalTo("3:*ERROR* dummy 1"));
    assertThat(this.libc.log.get(1), equalTo("3:*ERROR* dummy 2"));
    assertThat(this.libc.log.get(2), equalTo("3:*ERROR* dummy 3"));

    /* clear */

    this.libc.log.clear();

    Thread.sleep(100);

    this.impl.stop(this.ctx);

    while (this.impl.isAlive() || (this.libc.log.size() != 1)) {
      Thread.sleep(10);
    }

    assertThat(Boolean.valueOf(this.impl.signalEmpty.get()), equalTo(Boolean.FALSE));

    assertThat(Integer.valueOf(this.impl.queue.size()), equalTo(Integer.valueOf(0)));
    assertThat(this.regMulti.reference, nullValue());
    assertThat(this.regSingle.reference, nullValue());
    assertThat(Integer.valueOf(this.ctx.bundleListeners.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(this.ctx.frameworkListeners.size()), equalTo(Integer.valueOf(0)));
    assertThat(Integer.valueOf(this.ctx.serviceListeners.size()), equalTo(Integer.valueOf(0)));

    assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(1)));
    assertThat(this.libc.log.get(0), equalTo(MockSysLogLibc.LOG_CLOSE));

    assertThat(this.impl.eventAdminTracker, nullValue());
    assertThat(this.impl.serviceRegistration, nullValue());
    assertThat(this.impl.serviceReference, nullValue());
    assertThat(this.impl.bundle, nullValue());
    assertThat(Integer.valueOf(this.impl.logStore.logs.size()), equalTo(Integer.valueOf(0)));
  }
}