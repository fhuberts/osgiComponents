package nl.pelagic.service.log.syslog;

import java.io.IOException;

import org.junit.Test;
import org.osgi.service.log.LogService;

@SuppressWarnings({
    "static-method", "javadoc"
})
public class TestSysLogLibcImpl {
  @Test(timeout = 8000)
  public void testLoadOk() throws IOException {
    final SysLogLibcImpl impl = new SysLogLibcImpl();
    impl.load();
  }

  @Test(timeout = 8000, expected = IOException.class)
  public void testLoadFail() throws IOException {
    final SysLogLibcImpl impl = new SysLogLibcImpl();
    impl.libraryName = "unloadable library that causes a load error";
    impl.load();
  }

  @Test(timeout = 8000)
  public void testSyslog() throws IOException {
    final SysLogLibcImpl impl = new SysLogLibcImpl();

    impl.load();
    impl.syslog(LogService.LOG_DEBUG, "%s", "unit test");
  }
}