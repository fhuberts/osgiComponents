package nl.pelagic.service.log.syslog;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogService;

@SuppressWarnings({
    "rawtypes", "unused", "javadoc"
})
public class TestSysLog {

  SysLog                              impl;
  MockBundleContext                   ctx;
  MockLogListener                     listener;
  MockSysLogLibc                      libc;
  MockEventAdmin                      ea;
  MockTracker<EventAdmin, EventAdmin> tracker;

  @Before
  public void setUp() throws Exception {
    System.clearProperty(SysLog.ENV_LOG_SERVICE_NAME);
    System.clearProperty(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL);
    System.clearProperty(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC);
    System.clearProperty(SysLog.ENV_LOG_READER_DEPTH);

    this.libc = new MockSysLogLibc();

    this.ea = new MockEventAdmin();

    this.ctx = new MockBundleContext();
    this.listener = new MockLogListener();

    this.impl = new SysLog();
    this.impl.libc = this.libc;

    this.tracker = new MockTracker<>(this.ctx, this.getClass().getName(), null);
    this.impl.eventAdminTracker = this.tracker;

    this.impl.start();
    while (!this.impl.isAlive()) {
      Thread.sleep(10);
    }

    assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(0)));
  }

  @After
  public void tearDown() throws Exception {
    this.impl.run.set(false);
    this.impl.interrupt();

    while (this.impl.isAlive()) {
      Thread.sleep(10);
    }

    this.impl.eventAdminTracker = null;
    this.tracker = null;
    this.impl = null;
    this.ea = null;
    this.listener = null;
    this.ctx = null;
    this.libc = null;
  }

  @Test(timeout = 8000)
  public void testLogNoListeners1() throws InterruptedException {
    final long tm = System.currentTimeMillis();

    this.impl.log(null, null, LogService.LOG_ERROR, "message", null, null);

    while (this.libc.log.size() == 0) {
      Thread.sleep(10);
    }
    while (this.impl.logStore.logs.size() == 0) {
      Thread.sleep(10);
    }

    assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(1)));

    assertThat(Integer.valueOf(this.impl.logStore.logs.size()), equalTo(Integer.valueOf(1)));
    final LogEntry le = this.impl.logStore.logs.get(0);
    assertThat(le, notNullValue());
    assertThat(Boolean.valueOf(le.getTime() >= tm), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(le.getLevel()), equalTo(Integer.valueOf(LogService.LOG_ERROR)));
    assertThat(le.getMessage(), equalTo("message"));
    assertThat(le.getBundle(), nullValue());
    assertThat(le.getException(), nullValue());
    assertThat(le.getServiceReference(), nullValue());

    assertThat(Integer.valueOf(this.impl.queue.size()), equalTo(Integer.valueOf(0)));

    int count = 0;
    final Enumeration<LogEntry> logs = this.impl.getLog();
    while (logs.hasMoreElements()) {
      final LogEntry object = logs.nextElement();
      count++;
    }
    assertThat(Integer.valueOf(count), equalTo(Integer.valueOf(1)));
  }

  @Test(timeout = 8000)
  public void testLogNoListeners2() throws InterruptedException {
    final long tm = System.currentTimeMillis();
    final Throwable ex = new Exception("ex");

    this.impl.log(null, null, LogService.LOG_ERROR, "message", ex, null);

    while (this.libc.log.size() == 0) {
      Thread.sleep(10);
    }
    while (this.impl.logStore.logs.size() == 0) {
      Thread.sleep(10);
    }

    assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(1)));

    assertThat(Integer.valueOf(this.impl.logStore.logs.size()), equalTo(Integer.valueOf(1)));
    final LogEntry le = this.impl.logStore.logs.get(0);
    assertThat(le, notNullValue());
    assertThat(Boolean.valueOf(le.getTime() >= tm), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(le.getLevel()), equalTo(Integer.valueOf(LogService.LOG_ERROR)));
    assertThat(le.getMessage(), equalTo("message"));
    assertThat(le.getBundle(), nullValue());
    assertThat(le.getException(), equalTo(ex));
    assertThat(le.getServiceReference(), nullValue());

    assertThat(Integer.valueOf(this.impl.queue.size()), equalTo(Integer.valueOf(0)));

    int count = 0;
    final Enumeration<LogEntry> logs = this.impl.getLog();
    while (logs.hasMoreElements()) {
      final LogEntry object = logs.nextElement();
      count++;
    }
    assertThat(Integer.valueOf(count), equalTo(Integer.valueOf(1)));
  }

  @Test(timeout = 8000)
  public void testLogNoListeners3() throws InterruptedException {
    final long tm = System.currentTimeMillis();
    final MockBundle bundle = new MockBundle();
    final MockServiceReference<?> sr = new MockServiceReference<>();
    sr.bundle = bundle;

    this.impl.log(null, sr, LogService.LOG_ERROR, "message", null, null);

    while (this.libc.log.size() == 0) {
      Thread.sleep(10);
    }
    while (this.impl.logStore.logs.size() == 0) {
      Thread.sleep(10);
    }

    assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(1)));

    assertThat(Integer.valueOf(this.impl.logStore.logs.size()), equalTo(Integer.valueOf(1)));
    final LogEntry le = this.impl.logStore.logs.get(0);
    assertThat(le, notNullValue());
    assertThat(Boolean.valueOf(le.getTime() >= tm), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(le.getLevel()), equalTo(Integer.valueOf(LogService.LOG_ERROR)));
    assertThat(le.getMessage(), equalTo("message"));
    assertThat(le.getBundle(), equalTo((Bundle) bundle));
    assertThat(le.getException(), nullValue());
    assertThat(le.getServiceReference(), equalTo((ServiceReference) sr));

    assertThat(Integer.valueOf(this.impl.queue.size()), equalTo(Integer.valueOf(0)));

    int count = 0;
    final Enumeration<LogEntry> logs = this.impl.getLog();
    while (logs.hasMoreElements()) {
      final LogEntry object = logs.nextElement();
      count++;
    }
    assertThat(Integer.valueOf(count), equalTo(Integer.valueOf(1)));
  }

  @Test(timeout = 8000)
  public void testLogNoListeners4() throws InterruptedException {
    final long tm = System.currentTimeMillis();
    final Throwable ex = new Exception("ex");
    final MockBundle bundle = new MockBundle();
    final MockServiceReference<?> sr = new MockServiceReference<>();
    sr.bundle = bundle;

    this.impl.log(null, sr, LogService.LOG_ERROR, "message", ex, null);

    while (this.libc.log.size() == 0) {
      Thread.sleep(10);
    }
    while (this.impl.logStore.logs.size() == 0) {
      Thread.sleep(10);
    }

    assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(1)));

    assertThat(Integer.valueOf(this.impl.logStore.logs.size()), equalTo(Integer.valueOf(1)));
    final LogEntry le = this.impl.logStore.logs.get(0);
    assertThat(le, notNullValue());
    assertThat(Boolean.valueOf(le.getTime() >= tm), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(le.getLevel()), equalTo(Integer.valueOf(LogService.LOG_ERROR)));
    assertThat(le.getMessage(), equalTo("message"));
    assertThat(le.getBundle(), equalTo((Bundle) bundle));
    assertThat(le.getException(), equalTo(ex));
    assertThat(le.getServiceReference(), equalTo((ServiceReference) sr));

    assertThat(Integer.valueOf(this.impl.queue.size()), equalTo(Integer.valueOf(0)));

    int count = 0;
    final Enumeration<LogEntry> logs = this.impl.getLog();
    while (logs.hasMoreElements()) {
      final LogEntry object = logs.nextElement();
      count++;
    }
    assertThat(Integer.valueOf(count), equalTo(Integer.valueOf(1)));
  }

  @Test(timeout = 8000)
  public void testLogListenersNoEA() throws InterruptedException {
    final long tm = System.currentTimeMillis();

    this.impl.addLogListener(this.listener);

    try {
      this.impl.log(null, null, LogService.LOG_ERROR, "message", null, null);

      while (this.libc.log.size() == 0) {
        Thread.sleep(10);
      }
      while (this.listener.logs.size() == 0) {
        Thread.sleep(10);
      }
      while (this.impl.logStore.logs.size() == 0) {
        Thread.sleep(10);
      }

      assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(1)));

      assertThat(Integer.valueOf(this.listener.logs.size()), equalTo(Integer.valueOf(1)));
      final LogEntry ll = this.listener.logs.get(0);

      assertThat(Integer.valueOf(this.impl.logStore.logs.size()), equalTo(Integer.valueOf(1)));
      final LogEntry le = this.impl.logStore.logs.get(0);

      assertThat(ll, equalTo(le));

      assertThat(le, notNullValue());
      assertThat(Boolean.valueOf(le.getTime() >= tm), equalTo(Boolean.TRUE));
      assertThat(Integer.valueOf(le.getLevel()), equalTo(Integer.valueOf(LogService.LOG_ERROR)));
      assertThat(le.getMessage(), equalTo("message"));
      assertThat(le.getBundle(), nullValue());
      assertThat(le.getException(), nullValue());
      assertThat(le.getServiceReference(), nullValue());

      assertThat(Integer.valueOf(this.impl.queue.size()), equalTo(Integer.valueOf(0)));
    }
    finally {
      this.impl.removeLogListener(this.listener);
    }
  }

  @Test(timeout = 8000)
  public void testLogListenersEA() throws InterruptedException {
    final long tm = System.currentTimeMillis();

    this.impl.addLogListener(this.listener);
    this.tracker.service = this.ea;

    try {
      this.impl.log(null, null, LogService.LOG_ERROR, "message", null, null);

      while (this.libc.log.size() == 0) {
        Thread.sleep(10);
      }
      while (this.ea.postEvents.size() == 0) {
        Thread.sleep(10);
      }
      while (this.listener.logs.size() == 0) {
        Thread.sleep(10);
      }
      while (this.impl.logStore.logs.size() == 0) {
        Thread.sleep(10);
      }

      assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(1)));

      assertThat(Integer.valueOf(this.ea.postEvents.size()), equalTo(Integer.valueOf(1)));
      final Event p = this.ea.postEvents.get(0);
      final LogEntry lp = (LogEntry) p.getProperty(LogEntryImpl.EA_LOG_ENTRY);

      assertThat(Integer.valueOf(this.listener.logs.size()), equalTo(Integer.valueOf(1)));
      final LogEntry ll = this.listener.logs.get(0);

      assertThat(Integer.valueOf(this.impl.logStore.logs.size()), equalTo(Integer.valueOf(1)));
      final LogEntry le = this.impl.logStore.logs.get(0);

      assertThat(lp, equalTo(le));
      assertThat(ll, equalTo(le));

      assertThat(le, notNullValue());
      assertThat(Boolean.valueOf(le.getTime() >= tm), equalTo(Boolean.TRUE));
      assertThat(Integer.valueOf(le.getLevel()), equalTo(Integer.valueOf(LogService.LOG_ERROR)));
      assertThat(le.getMessage(), equalTo("message"));
      assertThat(le.getBundle(), nullValue());
      assertThat(le.getException(), nullValue());
      assertThat(le.getServiceReference(), nullValue());

      assertThat(Integer.valueOf(this.impl.queue.size()), equalTo(Integer.valueOf(0)));
    }
    finally {
      this.tracker.service = null;
      this.impl.removeLogListener(this.listener);
    }
  }

  @Test(timeout = 8000)
  public void testLogListenersEAlevelTooLow() throws InterruptedException {
    final long tm = System.currentTimeMillis();

    this.impl.addLogListener(this.listener);
    this.tracker.service = this.ea;

    try {
      this.impl.log(null, null, LogService.LOG_DEBUG, "message", null, null);

      while (this.ea.postEvents.size() == 0) {
        Thread.sleep(10);
      }
      while (this.listener.logs.size() == 0) {
        Thread.sleep(10);
      }

      assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(0)));

      assertThat(Integer.valueOf(this.impl.logStore.logs.size()), equalTo(Integer.valueOf(0)));

      assertThat(Integer.valueOf(this.ea.postEvents.size()), equalTo(Integer.valueOf(1)));
      final Event p = this.ea.postEvents.get(0);
      final LogEntry lp = (LogEntry) p.getProperty(LogEntryImpl.EA_LOG_ENTRY);

      assertThat(Integer.valueOf(this.listener.logs.size()), equalTo(Integer.valueOf(1)));
      final LogEntry ll = this.listener.logs.get(0);

      assertThat(lp, equalTo(ll));

      assertThat(ll, notNullValue());
      assertThat(Boolean.valueOf(ll.getTime() >= tm), equalTo(Boolean.TRUE));
      assertThat(Integer.valueOf(ll.getLevel()), equalTo(Integer.valueOf(LogService.LOG_DEBUG)));
      assertThat(ll.getMessage(), equalTo("message"));
      assertThat(ll.getBundle(), nullValue());
      assertThat(ll.getException(), nullValue());
      assertThat(ll.getServiceReference(), nullValue());

      assertThat(Integer.valueOf(this.impl.queue.size()), equalTo(Integer.valueOf(0)));
    }
    finally {
      this.tracker.service = null;
      this.impl.removeLogListener(this.listener);
    }
  }

  @Test(timeout = 8000)
  public void testBundleChanged() throws InterruptedException {
    final long tm = System.currentTimeMillis();
    final MockBundle bundle = new MockBundle();
    final BundleEvent be = new BundleEvent(BundleEvent.INSTALLED, bundle);

    this.impl.bundleChanged(be);

    while (this.libc.log.size() == 0) {
      Thread.sleep(10);
    }
    while (this.impl.logStore.logs.size() == 0) {
      Thread.sleep(10);
    }

    assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(1)));

    assertThat(Integer.valueOf(this.impl.logStore.logs.size()), equalTo(Integer.valueOf(1)));
    final LogEntry le = this.impl.logStore.logs.get(0);
    assertThat(le, notNullValue());
    assertThat(Boolean.valueOf(le.getTime() >= tm), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(le.getLevel()), equalTo(Integer.valueOf(LogService.LOG_INFO)));
    assertThat(le.getMessage(), equalTo(LogUtils.bundleEventToString(be)));
    assertThat(le.getBundle(), equalTo((Bundle) bundle));
    assertThat(le.getException(), nullValue());
    assertThat(le.getServiceReference(), nullValue());

    assertThat(Integer.valueOf(this.impl.queue.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testFrameworkEventError() throws InterruptedException {
    final long tm = System.currentTimeMillis();
    final Throwable ex = new Throwable("ex");
    final MockBundle bundle = new MockBundle();
    final FrameworkEvent be = new FrameworkEvent(FrameworkEvent.ERROR, bundle, ex);

    this.impl.frameworkEvent(be);

    while (this.libc.log.size() == 0) {
      Thread.sleep(10);
    }
    while (this.impl.logStore.logs.size() == 0) {
      Thread.sleep(10);
    }

    assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(1)));

    assertThat(Integer.valueOf(this.impl.logStore.logs.size()), equalTo(Integer.valueOf(1)));
    final LogEntry le = this.impl.logStore.logs.get(0);
    assertThat(le, notNullValue());
    assertThat(Boolean.valueOf(le.getTime() >= tm), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(le.getLevel()), equalTo(Integer.valueOf(LogService.LOG_ERROR)));
    assertThat(le.getMessage(), equalTo(LogUtils.frameworkEventToString(be)));
    assertThat(le.getBundle(), equalTo((Bundle) bundle));
    assertThat(le.getException(), equalTo(ex));
    assertThat(le.getServiceReference(), nullValue());

    assertThat(Integer.valueOf(this.impl.queue.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testFrameworkEventNoError() throws InterruptedException {
    final long tm = System.currentTimeMillis();
    final Throwable ex = new Throwable("ex");
    final MockBundle bundle = new MockBundle();
    final FrameworkEvent fe = new FrameworkEvent(FrameworkEvent.STARTLEVEL_CHANGED, bundle, ex);

    this.impl.frameworkEvent(fe);

    while (this.libc.log.size() == 0) {
      Thread.sleep(10);
    }
    while (this.impl.logStore.logs.size() == 0) {
      Thread.sleep(10);
    }

    assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(1)));

    assertThat(Integer.valueOf(this.impl.logStore.logs.size()), equalTo(Integer.valueOf(1)));
    final LogEntry le = this.impl.logStore.logs.get(0);
    assertThat(le, notNullValue());
    assertThat(Boolean.valueOf(le.getTime() >= tm), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(le.getLevel()), equalTo(Integer.valueOf(LogService.LOG_INFO)));
    assertThat(le.getMessage(), equalTo(LogUtils.frameworkEventToString(fe)));
    assertThat(le.getBundle(), equalTo((Bundle) bundle));
    assertThat(le.getException(), nullValue());
    assertThat(le.getServiceReference(), nullValue());

    assertThat(Integer.valueOf(this.impl.queue.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testServiceChangedNormal() throws InterruptedException {
    final long tm = System.currentTimeMillis();
    final MockBundle bundle = new MockBundle();
    final MockServiceReference<?> sr = new MockServiceReference<>();
    sr.bundle = bundle;

    final ServiceEvent se = new ServiceEvent(ServiceEvent.REGISTERED, sr);

    this.impl.serviceChanged(se);

    while (this.libc.log.size() == 0) {
      Thread.sleep(10);
    }
    while (this.impl.logStore.logs.size() == 0) {
      Thread.sleep(10);
    }

    assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(1)));

    assertThat(Integer.valueOf(this.impl.logStore.logs.size()), equalTo(Integer.valueOf(1)));
    final LogEntry le = this.impl.logStore.logs.get(0);
    assertThat(le, notNullValue());
    assertThat(Boolean.valueOf(le.getTime() >= tm), equalTo(Boolean.TRUE));
    assertThat(Integer.valueOf(le.getLevel()), equalTo(Integer.valueOf(LogService.LOG_INFO)));
    assertThat(le.getMessage(), equalTo(LogUtils.serviceEventToString(se)));
    assertThat(le.getBundle(), equalTo((Bundle) bundle));
    assertThat(le.getException(), nullValue());
    assertThat(le.getServiceReference(), equalTo((ServiceReference) sr));

    assertThat(Integer.valueOf(this.impl.queue.size()), equalTo(Integer.valueOf(0)));
  }

  @Test(timeout = 8000)
  public void testServiceChangedModified() throws InterruptedException {
    final long tm = System.currentTimeMillis();

    final MockBundle bundle = new MockBundle();
    final MockServiceReference<?> sr = new MockServiceReference<>();
    sr.bundle = bundle;

    final ServiceEvent se = new ServiceEvent(ServiceEvent.MODIFIED, sr);

    this.impl.addLogListener(this.listener);

    try {
      this.impl.serviceChanged(se);

      while (this.listener.logs.size() == 0) {
        Thread.sleep(10);
      }

      assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(0)));

      assertThat(Integer.valueOf(this.impl.logStore.logs.size()), equalTo(Integer.valueOf(0)));

      assertThat(Integer.valueOf(this.listener.logs.size()), equalTo(Integer.valueOf(1)));
      final LogEntry le = this.listener.logs.get(0);
      assertThat(le, notNullValue());
      assertThat(Boolean.valueOf(le.getTime() >= tm), equalTo(Boolean.TRUE));
      assertThat(Integer.valueOf(le.getLevel()), equalTo(Integer.valueOf(LogService.LOG_DEBUG)));
      assertThat(le.getMessage(), equalTo(LogUtils.serviceEventToString(se)));
      assertThat(le.getBundle(), equalTo((Bundle) bundle));
      assertThat(le.getException(), nullValue());
      assertThat(le.getServiceReference(), equalTo((ServiceReference) sr));

      assertThat(Integer.valueOf(this.impl.queue.size()), equalTo(Integer.valueOf(0)));
    }
    finally {
      this.impl.removeLogListener(this.listener);
    }
  }

  @Test(timeout = 8000)
  public void testProcessNameFromEnvironment() {
    this.testInitialEnv();

    this.impl.processNameFromEnvironment();
    assertThat(this.impl.logServiceName, equalTo(SysLog.ENV_LOG_SERVICE_NAME_DEFAULT));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_NAME), equalTo(SysLog.ENV_LOG_SERVICE_NAME_DEFAULT));

    System.setProperty(SysLog.ENV_LOG_SERVICE_NAME, " name ");
    this.impl.processNameFromEnvironment();
    assertThat(this.impl.logServiceName, equalTo("name"));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_NAME), equalTo("name"));
  }

  @Test(timeout = 8000)
  public void testProcessLowestLevelFromEnvironment() {
    this.testInitialEnv();

    this.impl.processLowestLevelFromEnvironment();
    assertThat(Integer.valueOf(this.impl.logServiceLowestLevel),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL),
        equalTo(LogUtils.levelToTopicLevel(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL_DEFAULT)));

    System.setProperty(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL, " error ");
    this.impl.processLowestLevelFromEnvironment();
    assertThat(Integer.valueOf(this.impl.logServiceLowestLevel), equalTo(Integer.valueOf(LogService.LOG_ERROR)));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL),
        equalTo(LogUtils.levelToTopicLevel(LogService.LOG_ERROR)));
  }

  @Test(timeout = 8000)
  public void testProcessQueueDrainWaitMsecFromEnvironment() {
    this.testInitialEnv();

    this.impl.processQueueDrainWaitMsecFromEnvironment();
    assertThat(Integer.valueOf(this.impl.logServiceQueueDrainWaitMsec),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC),
        equalTo(Integer.toString(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC_DEFAULT)));

    System.setProperty(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC, " 12 ");
    this.impl.processQueueDrainWaitMsecFromEnvironment();
    assertThat(Integer.valueOf(this.impl.logServiceQueueDrainWaitMsec), equalTo(Integer.valueOf(12)));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC), equalTo(Integer.toString(12)));
  }

  @Test(timeout = 8000)
  public void testProcessDepthFromEnvironment() {
    this.testInitialEnv();

    this.impl.processDepthFromEnvironment();
    assertThat(Integer.valueOf(this.impl.logReaderDepth),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_READER_DEPTH_DEFAULT)));
    assertThat(Integer.valueOf(this.impl.logStore.depth),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_READER_DEPTH_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_READER_DEPTH),
        equalTo(Integer.toString(SysLog.ENV_LOG_READER_DEPTH_DEFAULT)));

    System.setProperty(SysLog.ENV_LOG_READER_DEPTH, " 12 ");
    this.impl.processDepthFromEnvironment();
    assertThat(Integer.valueOf(this.impl.logReaderDepth), equalTo(Integer.valueOf(12)));
    assertThat(Integer.valueOf(this.impl.logStore.depth), equalTo(Integer.valueOf(12)));
    assertThat(System.getProperty(SysLog.ENV_LOG_READER_DEPTH), equalTo(Integer.toString(12)));
  }

  @Test(timeout = 8000)
  public void testInitialEnv() {
    assertThat(this.impl.logServiceName, equalTo(SysLog.ENV_LOG_SERVICE_NAME_DEFAULT));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_NAME), nullValue());

    assertThat(Integer.valueOf(this.impl.logServiceLowestLevel),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL), nullValue());

    assertThat(Integer.valueOf(this.impl.logServiceQueueDrainWaitMsec),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC), nullValue());

    assertThat(Integer.valueOf(this.impl.logReaderDepth),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_READER_DEPTH_DEFAULT)));
    assertThat(Integer.valueOf(this.impl.logStore.depth),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_READER_DEPTH_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_READER_DEPTH), nullValue());
  }

  @Test(timeout = 8000)
  public void testUpdatedEmpty() {
    this.testInitialEnv();

    this.impl.updated(null);

    this.testInitialEnv();

    final Dictionary<String, ?> properties = new Hashtable<>();
    this.impl.updated(properties);

    this.testInitialEnv();
  }

  @Test(timeout = 8000)
  public void testUpdatedName() {
    this.testInitialEnv();

    /* no value */

    Dictionary<String, Object> properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_SERVICE_NAME + "-dummy", new Object());
    this.impl.updated(properties);

    assertThat(this.impl.logServiceName, equalTo(SysLog.ENV_LOG_SERVICE_NAME_DEFAULT));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_NAME), nullValue());

    /* invalid type */

    properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_SERVICE_NAME, new Object());
    this.impl.updated(properties);

    assertThat(this.impl.logServiceName, equalTo(SysLog.ENV_LOG_SERVICE_NAME_DEFAULT));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_NAME), nullValue());

    /* no change */

    properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_SERVICE_NAME, SysLog.ENV_LOG_SERVICE_NAME_DEFAULT);
    this.impl.updated(properties);

    assertThat(this.impl.logServiceName, equalTo(SysLog.ENV_LOG_SERVICE_NAME_DEFAULT));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_NAME), nullValue());

    /* change */

    this.impl.addLogListener(this.listener);
    this.listener.logSleep = 500;

    properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_SERVICE_NAME, "new name");

    this.impl.signalEmpty.compareAndSet(false, true);
    this.impl.updated(properties);
    this.impl.waitForQueueToDrain();

    assertThat(this.impl.logServiceName, equalTo("new name"));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_NAME), equalTo("new name"));

    assertThat(Integer.valueOf(this.libc.log.size()), equalTo(Integer.valueOf(3)));
    String s = this.libc.log.get(0);
    assertThat(s, equalTo("6:*INFO* LogService name changed from '' to 'new name'"));
    s = this.libc.log.get(1);
    assertThat(s, equalTo(MockSysLogLibc.LOG_CLOSE));
    s = this.libc.log.get(2);
    assertThat(s, equalTo(MockSysLogLibc.LOG_OPEN + " new name 3 8"));
  }

  @Test(timeout = 8000)
  public void testUpdatedLowestLevel() {
    this.testInitialEnv();

    /* no value */

    Dictionary<String, Object> properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL + "-dummy", new Object());
    this.impl.updated(properties);

    assertThat(Integer.valueOf(this.impl.logServiceLowestLevel),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL), nullValue());

    /* invalid type */

    properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL, new Object());
    this.impl.updated(properties);

    assertThat(Integer.valueOf(this.impl.logServiceLowestLevel),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL), nullValue());

    /* no change */

    properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL, "" + SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL_DEFAULT);
    this.impl.updated(properties);

    assertThat(Integer.valueOf(this.impl.logServiceLowestLevel),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL), nullValue());

    /* change */

    properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL, "42");

    this.impl.signalEmpty.compareAndSet(false, true);
    this.impl.updated(properties);
    this.impl.waitForQueueToDrain();

    assertThat(Integer.valueOf(this.impl.logServiceLowestLevel), equalTo(Integer.valueOf(LogService.LOG_DEBUG)));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_LOWEST_LEVEL),
        equalTo(LogUtils.levelToTopicLevel(LogService.LOG_DEBUG)));

    assertThat(Integer.valueOf(this.impl.logStore.logs.size()), equalTo(Integer.valueOf(1)));
  }

  @Test(timeout = 8000)
  public void testUpdatedQueueDrainWaitMsec() {
    this.testInitialEnv();

    /* no value */

    Dictionary<String, Object> properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC + "-dummy", new Object());
    this.impl.updated(properties);

    assertThat(Integer.valueOf(this.impl.logServiceQueueDrainWaitMsec),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC), nullValue());

    /* invalid type */

    properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC, new Object());
    this.impl.updated(properties);

    assertThat(Integer.valueOf(this.impl.logServiceQueueDrainWaitMsec),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC), nullValue());

    /* no change */

    properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC,
        "" + SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC_DEFAULT);
    this.impl.updated(properties);

    assertThat(Integer.valueOf(this.impl.logServiceQueueDrainWaitMsec),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC), nullValue());

    /* change */

    properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC, "42");

    this.impl.signalEmpty.compareAndSet(false, true);
    this.impl.updated(properties);
    this.impl.waitForQueueToDrain();

    assertThat(Integer.valueOf(this.impl.logServiceQueueDrainWaitMsec), equalTo(Integer.valueOf(42)));
    assertThat(System.getProperty(SysLog.ENV_LOG_SERVICE_QUEUE_DRAIN_WAIT_MSEC), equalTo("42"));
  }

  @Test(timeout = 8000)
  public void testUpdatedDepth() {
    this.testInitialEnv();

    /* no value */

    Dictionary<String, Object> properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_READER_DEPTH + "-dummy", new Object());
    this.impl.updated(properties);

    assertThat(Integer.valueOf(this.impl.logReaderDepth),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_READER_DEPTH_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_READER_DEPTH), nullValue());

    /* invalid type */

    properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_READER_DEPTH, new Object());
    this.impl.updated(properties);

    assertThat(Integer.valueOf(this.impl.logReaderDepth),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_READER_DEPTH_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_READER_DEPTH), nullValue());

    /* no change */

    properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_READER_DEPTH, "" + SysLog.ENV_LOG_READER_DEPTH_DEFAULT);
    this.impl.updated(properties);

    assertThat(Integer.valueOf(this.impl.logReaderDepth),
        equalTo(Integer.valueOf(SysLog.ENV_LOG_READER_DEPTH_DEFAULT)));
    assertThat(System.getProperty(SysLog.ENV_LOG_READER_DEPTH), nullValue());

    /* change */

    properties = new Hashtable<>();
    properties.put(SysLog.ENV_LOG_READER_DEPTH, "42");

    this.impl.signalEmpty.compareAndSet(false, true);
    this.impl.updated(properties);
    this.impl.waitForQueueToDrain();

    assertThat(Integer.valueOf(this.impl.logReaderDepth), equalTo(Integer.valueOf(42)));
    assertThat(Integer.valueOf(this.impl.logStore.depth), equalTo(Integer.valueOf(42)));
    assertThat(System.getProperty(SysLog.ENV_LOG_READER_DEPTH), equalTo("42"));

    assertThat(Integer.valueOf(this.impl.logStore.logs.size()), equalTo(Integer.valueOf(1)));
  }

  @Test(timeout = 8000)
  public void testWaitForQueueToDrain() {
    final boolean signalEmptyBackup = this.impl.signalEmpty.get();
    this.impl.signalEmpty.set(true);
    try {
      this.impl.waitForQueueToDrain();
    }
    finally {
      this.impl.signalEmpty.set(signalEmptyBackup);
    }
  }

  @Test(timeout = 8000)
  public void testRunForcedStop() {
    this.impl.forceStop.set(true);
    this.impl.interrupt();

  }
}